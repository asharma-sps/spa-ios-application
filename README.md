# Basic Information

SPA iOS Application
By Amit Sharma

# Abstract

This repository contains the official SixPackAbs.com iOS Xcode Project and its related assets. It replaces an older version of the app which existing in the now-archived “sps-ios” repository. The project was migrated into this new repository to ease the transition to Swift 3.0. For additional details about the migration, please read the README file in the archived project.

# Architecture

The app utilizes a MVC-MVVM hybrid architecture.

There are two possible root view controller, either HomescreenVC (if there IS NOT authenticated user) or RootTabBarController (if there IS an authenticated). You toggle between the two by using the "setRootViewController(withUser optionalUser: User?)" method in the AppDelegate.

Authenticated users keys are stored in the keychain. Upon launch, the appropriate root view controller will be selected depending on if there is a authenticated user in the keychain. If there is, the user is injected into the root tab bar controller and propogated througout the app from there. If you ever need to trace something down in the app, the RootTabBarController is good starting point.

Crtical API responses are manually cached as binary files (not using NSURL cache). They are purged on user log out. There is a cache policy of attempt-to-load-then-fallback-on-cache.
