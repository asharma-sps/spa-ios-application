//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2016 SixPackAbs. All rights reserved.
//  *************************************************
//


import XCTest
@testable import spa_ios

class AuthenticatedUserPersistanceManagerTest: XCTestCase {
    
    func testAuthenticatedUserPersistanceManager() {
        
        // prep
        AuthenticatedUserPersistanceManager.clear()
        XCTAssertNil(AuthenticatedUserPersistanceManager.load(), "Failed to clear AuthenticatedUserPersistanceManager")
        
        // save user
        let user = Mocker.mockUser()
        AuthenticatedUserPersistanceManager.set(user: user)
        
        // TEST - user loading
        guard let loadedUser = AuthenticatedUserPersistanceManager.load() else {
            XCTFail("Failed to load the user from the authentication manager")
            return
        }
        
        // TEST - loaded user data integrity
        XCTAssert(loadedUser.firstName == user.firstName, "Authentication manager provided an incorrect first name")
        XCTAssert(loadedUser.lastName == user.lastName, "Authentication manager provided an incorrect last name")
        XCTAssert(loadedUser.email == user.email, "Authentication manager provided an incorrect email")
        XCTAssert(loadedUser.token == user.token, "Authentication manager provided and incorrect token")
        XCTAssert(loadedUser.id == user.id, "Authentication manager provided and incorrect user id")
        XCTAssert(loadedUser.authenticationMethod == user.authenticationMethod, "Authentication manager provided and incorrect authentication method")

        // TEST - clearing user
        AuthenticatedUserPersistanceManager.clear()
        XCTAssertNil(AuthenticatedUserPersistanceManager.load(), "Failed to clear authentication manager")
    }
}
