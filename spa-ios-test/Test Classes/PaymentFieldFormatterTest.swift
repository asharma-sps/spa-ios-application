//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2016 SixPackAbs. All rights reserved.
//  *************************************************
//

import XCTest
@testable import spa_ios

class PaymentFieldFormatterTest: XCTestCase {
    
    let formatter = PaymentFieldFormatter()
    let field = PaymentCardTextField()
    
    func testFormatStringAsCreditCard() {
        assert(formatter.formatStringAsCreditCardNumber(string: "4111111111111111") == "4111 1111 1111 1111") // visa, discover, mastercard
        assert(formatter.formatStringAsCreditCardNumber(string: "373737337337373") == "3737 373373 37373") // amex
        assert(formatter.formatStringAsCreditCardNumber(string: "1111111111111111") == nil) // invalid
    }
}
