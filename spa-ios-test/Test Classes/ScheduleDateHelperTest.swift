//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2016 SixPackAbs. All rights reserved.
//  *************************************************
//

import XCTest
@testable import spa_ios

class ScheduleDateHelperTest: XCTestCase {
    
    let formatter = DateFormatter()
    
    func testDetermineStartDatesForWeekInRange() {
        
        formatter.dateFormat = "MM/dd/yyyy"
        
        // basic check
        let dates = ScheduleDateHelper.determineStartDatesForWeeksInRange(dateInStartWeek: formatter.date(from: "09/02/1992")!, dateInEndWeek: formatter.date(from: "09/18/1992")!)
        let resultStrings = stringsForDates(dates: dates)
        
        assert(resultStrings[0] == "08/30/1992")
        assert(resultStrings[1] == "09/06/1992")
        assert(resultStrings[2] == "09/13/1992")
        assert(resultStrings.count == 3)
        
        // year wrap
        let datesOverYearWrap = ScheduleDateHelper.determineStartDatesForWeeksInRange(dateInStartWeek: formatter.date(from: "12/30/2017")!, dateInEndWeek: formatter.date(from: "01/10/2018")!)
        let resultStringsOverYearWrap = stringsForDates(dates: datesOverYearWrap)
        
        assert(resultStringsOverYearWrap[0] == "12/24/2017")
        assert(resultStringsOverYearWrap[2] == "01/07/2018")
        assert(resultStrings.count == 3)
    }
    
    func stringsForDates(dates: [Date]) -> [String] {
        var resultStrings = [String]()
        for x in dates {
            resultStrings.append(formatter.string(from: x))
        }
        return resultStrings
    }
}
