//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2016 SixPackAbs. All rights reserved.
//  *************************************************
//

import XCTest
@testable import spa_ios

class UserSessionManagerTest: XCTestCase {
    
//    func testUserSessionManager() {
//        
//        
//        let loginExpectation = expectation(description: "login")
//        UserSessionManager.singleton.login(email: "amitsharma@mac.com", password: "train") { (success, errorMessage) in
//            XCTAssert(success)
//            XCTAssertNil(errorMessage)
//            XCTAssertNotNil(UserSessionManager.singleton.currentUser)
//            let authPersistanceManager = AuthenticatedUserPersistanceManager()
//            XCTAssertNotNil(authPersistanceManager.load(), "Authenticated user credentials was not persisted to the keychain")
//            loginExpectation.fulfill()
//        }
//        
//        waitForExpectations(timeout: 10) { (error) in
//            print("login timeout error occured. error: \(error)")
//        }
//        
//        let logoutExpectation = expectation(description: "logout")
//        UserSessionManager.singleton.logoutCurrentUser { (success, errorMessage) in
//            XCTAssert(success, "(could not log user out... \(errorMessage))")
//            XCTAssertNil(UserSessionManager.singleton.currentUser)
//            XCTAssertTrue(CommonProductPool.products.count == 0, "The product pool wasn't drained at logout")
//            let authPersistanceManager = AuthenticatedUserPersistanceManager()
//            XCTAssertNil(authPersistanceManager.load(), "Authenticated user credentials was not cleared form the keychain")
//            XCTAssertNil(ProductsDataCacheHelper.load(), "Product data cache was not cleared on loggout")
//            XCTAssertNil(ScheduleDataCacheHelper.load(), "Schedule data cache was not cleared on loggout")
//            XCTAssertNil(ProgressDataCacheHelper.load(), "Progress data cache was not cleared on loggout")
//            XCTAssertNil(CheckoutDefaultsHelper.load(), "Checkout defaults we're not cleared on loggout")
//            logoutExpectation.fulfill()
//        }
//        
//        waitForExpectations(timeout: 10) { (error) in
//            print("logout timeout error occured. error: \(error)")
//        }
//    }
}
