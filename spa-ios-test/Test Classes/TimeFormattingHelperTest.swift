//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2016 SixPackAbs. All rights reserved.
//  *************************************************
//

import XCTest
@testable import spa_ios

class TimeFormattingHelperTest: XCTestCase { // Fix Test ASTODO
    
    func testBasicFunctionality() {
        let longTime = TimeFormattingHelper.formattedTime(seconds: 3723, negative: false, alwaysShowMinutes: false)
        XCTAssert(longTime == "1:02:03")
        
        let shorterTime = TimeFormattingHelper.formattedTime(seconds: 189, negative: false, alwaysShowMinutes: false)
        XCTAssert(shorterTime == "03:09")
    }
    
    func testFlagFunctionality() {
        let longTime = TimeFormattingHelper.formattedTime(seconds: 3723, negative: true, alwaysShowMinutes: false)
        XCTAssert(longTime == "-1:02:03")
        
        let veryShortTime = TimeFormattingHelper.formattedTime(seconds: 20, negative: false, alwaysShowMinutes: false)
        XCTAssert(veryShortTime == "20")
        
        let veryShortTimeAlwaysShowMinutes = TimeFormattingHelper.formattedTime(seconds: 20, negative: false, alwaysShowMinutes: true)
        XCTAssert(veryShortTimeAlwaysShowMinutes == "00:20")
    }
    
    func testEdgeCaseInputs() {
        let longTime = TimeFormattingHelper.formattedTime(seconds: 3723, negative: false, alwaysShowMinutes: false)
        XCTAssert(longTime == "1:02:03")
        
        let shorterTime = TimeFormattingHelper.formattedTime(seconds: 189, negative: false, alwaysShowMinutes: false)
        XCTAssert(shorterTime == "03:09")
    }
}
