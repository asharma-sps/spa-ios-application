//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2016 SixPackAbs. All rights reserved.
//  *************************************************
//

import XCTest
@testable import spa_ios

// mock values
private let phone = "5121116666"
private let phoneCountry = "+1"

private let billingFirst = "Bob"
private let billingLast = "Burger"
private let billingStreet = "123 Main Lane"
private let billingCity = "Austin"
private let billingState = "TX"
private let billingCountry = "USA"
private let billingZip = "78613"

private let shippingFirst = "Shea"
private let shippingLast = "Girl"
private let shippingStreet = "456 Market Rd"
private let shippingCity = "Vancouver"
private let shippingState = "British Columbia"
private let shippingCountry = "Canada"
private let shippingZip = "90836"

class CheckoutDefaultsHelperTest: XCTestCase {
    
    func testFunctionalityCorrectness() {
        
        // prep
        CheckoutDefaultsHelper.clear()
        XCTAssertNil(CheckoutDefaultsHelper.load(), "Failed to clear checkout defaults when preparing test.")
        
        // mock objects
        let billing = Address(firstName: billingFirst,
                              lastName: billingLast,
                              street: billingStreet,
                              city: billingCity,
                              state: billingState,
                              country: billingCountry,
                              zipCode: billingCountry)
        
        let shipping = Address(firstName: shippingFirst,
                               lastName: shippingLast,
                               street: shippingStreet,
                               city: shippingCity,
                               state: shippingState,
                               country: shippingCountry,
                               zipCode: shippingZip)
        
        let orderPacket = OrderPacket(productID: 0,
                                      phone: phone,
                                      phoneCountry: phoneCountry,
                                      creditCardString: "not need for test",
                                      cardType: .Amex,
                                      expirationMonth: "not needed for test",
                                      expirationYear: "not needed for test",
                                      cvv: "not needed for test",
                                      billing: billing,
                                      shipping: shipping)
        
        let orderPacketWithoutShipping = OrderPacket(productID: 0,
                                                     phone: phone,
                                                     phoneCountry: phoneCountry,
                                                     creditCardString: "not need for test",
                                                     cardType: .Amex,
                                                     expirationMonth: "not needed for test",
                                                     expirationYear: "not needed for test",
                                                     cvv: "not needed for test",
                                                     billing: billing,
                                                     shipping: nil)
        
        // save and load
        CheckoutDefaultsHelper.set(orderPacket: orderPacket)
        guard let loaded = CheckoutDefaultsHelper.load() else {
            XCTFail("Failed to load the just-set checkout defaults")
            return
        }
        guard let loadedShipping = loaded.shipping else {
            XCTFail("A default shipping address was saved but was not loaded")
            return
        }
        
        // data verification
        XCTAssert(phone == loaded.phone, "Saved and loaded phone do not match")
        XCTAssert(phoneCountry == loaded.phoneCountry, "Saved and loaded phone country do not match")
        XCTAssert(compareAddress(billing, loaded.billing), "Saved and loaded billing address do not match")
        XCTAssert(compareAddress(shipping, loadedShipping), "Saved and loaded shipping addresses do not match")
        
        // verify old shipping addresses are cleared when setting new billing address only
        CheckoutDefaultsHelper.set(orderPacket: orderPacketWithoutShipping)
        guard let loadedWithoutShipping = CheckoutDefaultsHelper.load() else {
            XCTFail("Failed to load defaults after overwritting original data")
            return
        }
        XCTAssertNil(loadedWithoutShipping.shipping, "Failed to clear default checkout for shipping")
        
        // Verify clearing
        CheckoutDefaultsHelper.clear()
        XCTAssertNil(CheckoutDefaultsHelper.load(), "Failed to clear checkout defaults")
    }
    
    private func compareAddress(_ a: Address, _ b: Address) -> Bool {
        guard a.firstName == b.firstName else { return false }
        guard a.lastName == b.lastName else { return false }
        guard a.street == b.street else { return false }
        guard a.city == b.city else { return false }
        guard a.state == b.state else { return false }
        guard a.country == b.country else { return false }
        guard a.zipCode == b.zipCode else { return false }
        return true
    }
}
