//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2016 SixPackAbs. All rights reserved.
//  *************************************************
//

import XCTest
@testable import spa_ios

class EnvironmentTest: XCTestCase {
    
    func testBuildFlagDetection() {
        XCTAssert(Environment.isDebugBuild)
    }
}
