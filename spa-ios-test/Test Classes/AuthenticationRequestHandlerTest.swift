//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2016 SixPackAbs. All rights reserved.
//  *************************************************
//

import XCTest
@testable import spa_ios

class AuthenticationRequestHandlerTest: XCTestCase {
    
    func testNativeLogin() {
        
        let loginExpectation = expectation(description: "login")
     
        AuthenticationRequestHandler.login(email: "amitsharma@mac.com", password: "train") { result in
            switch result {
            case .success(let user):
                XCTAssertNotNil(user, "Failed to authenticate using native login")
                loginExpectation.fulfill()
            case .failure(let errorMessage):
                XCTFail("Failed to authenticate using native login. Error: \(errorMessage)")
                loginExpectation.fulfill()
            }
        }
        
        waitForExpectations(timeout: 10) { error in
            XCTAssertNil(error, "Failed to fufil login expectation")
        }
    }
    
    func testSocialLogin() {
        
        let socialExpectation = expectation(description: "social")
        
        AuthenticationRequestHandler.loginSocial(token: "747927490583819", vendor: .facebook, first: "Marky", last: "Mark", email: "mmarky@gmail.com") { result in
            switch result {
            case .success(let user):
                XCTAssertNotNil(user, "Failed to login social user.")
                socialExpectation.fulfill()
            case .failure(let errorMessage):
                XCTFail("Failed to login social user. \(errorMessage)")
                socialExpectation.fulfill()
            }
        }
        
        waitForExpectations(timeout: 10) { error in
            XCTAssertNil(error, "Login social expectation fufillment error. \(error)")
        }
    }
    
    func testRegisteringUser() {
        
        let register = expectation(description: "register")
        let randomEmail = "\(NSDate().hash)@sixpackshortcuts.com"
        
        AuthenticationRequestHandler.registerUser(first: "Example", last: "Man", password: "password", email: randomEmail, phoneCode: "+1", phoneNumber: "5129998888") { result in
            switch result {
            case .success(let user):
                XCTAssertNotNil(user, "Failed to register user.")
                register.fulfill()
            case .failure(let errorMessage):
                XCTFail("Failed to register user. \(errorMessage)")
                register.fulfill()
            }
        }
        
        waitForExpectations(timeout: 10) { error in
            XCTAssertNil(error, "Register expectation fufillment error. \(error)")
        }
    }
}
