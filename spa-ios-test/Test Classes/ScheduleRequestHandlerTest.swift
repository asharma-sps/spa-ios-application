//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2016 SixPackAbs. All rights reserved.
//  *************************************************
//

import XCTest
@testable import spa_ios

class ScheduleRequestHandlerTest: XCTestCase {
    
    let user = Mocker.mockUser()
    
    func testRequestSchedulableProgramsAndScheduleEntries() {
        let request = expectation(description: "schedule")
        
        ScheduleRequestHandler.requestSchedulableProgramsAndScheduleEntries(user: user) { resultsTupple in
            XCTAssertNotNil(resultsTupple)
            XCTAssertNotNil(resultsTupple?.entries)
            XCTAssertNotNil(resultsTupple?.programs)
            XCTAssertFalse(resultsTupple?.entries.isEmpty ?? true) // test user should have data
            XCTAssertFalse(resultsTupple?.programs.isEmpty ?? true) // test user should have data
            request.fulfill()
        }
        
        waitForExpectations(timeout: 10) { (error) in
            XCTAssertNil(error)
        }
    }
    
    func testRequestSchedulableProgramsForCurrentUser() {
        let request = expectation(description: "program_schedulable")
        
        ScheduleRequestHandler.requestSchedulablePrograms(user: user) { (programs) in
            XCTAssertNotNil(programs)
            XCTAssertFalse(programs?.isEmpty ?? true) // test user should have data
            request.fulfill()
        }
        
        waitForExpectations(timeout: 10) { (error) in
            XCTAssertNil(error)
        }
    }    
}
