//
//  Mocker.swift
//  spa-ios
//
//  Created by Amit Sharma on 1/26/17.
//  Copyright © 2017 Amit Sharma. All rights reserved.
//

import Foundation
@testable import spa_ios

struct Mocker {
    
    static func mockUser() -> User {
        let first = "Dr"
        let last = "Mario"
        let email = "dr.mario@mac.com"
        let token = "$2y$10$nja.Pr0kd2ol6lhswVQS4.Pr0Bco0n2cCxsZb3d938ZcAQcJENAsK"
        let id = "84773"
        let method = AuthenticationMethod.google
        return User(firstName: first, lastName: last, email: email, token: token, id: id, authenticationMethod: method)
    }
}

// test account login
// dr.mario@mac.com
// password
