//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class ExerciseBaseCardVC: UIViewController {
    
    let containerView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupContainerView()
    }
    
    private func setupContainerView() {
        containerView.backgroundColor = UIColor.white
        containerView.layer.cornerRadius = 4.0
        containerView.layer.masksToBounds = true
        containerView.layer.borderWidth = Environment.thinnestLine
        containerView.layer.borderColor = UIColor.brandGray.cgColor
        view.addSubview(containerView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerView.frame = CGRect(15.0, 15.0, view.frame.width - 30.0, view.frame.height - 30.0)
    }
}
