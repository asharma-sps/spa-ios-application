//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

extension UIImage {
    
    func cropToSquare() -> UIImage? {
        let image = self.normalizedOrientation()
        let dimension = min(image.size.width, image.size.height)
        let cropArea = CGRect((image.size.width - dimension) / 2, (image.size.height - dimension) / 2, dimension, dimension)
        guard let imageRef = image.cgImage!.cropping(to: cropArea) else { return nil }
        return UIImage(cgImage: imageRef)
    }
    
    func persistPNGRepresentationToDisk(name: String) -> (success: Bool, path: String?) {
        guard let data = UIImagePNGRepresentation(self) else { return (success: false, path: nil) }
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0].appending("\(name).png")
        guard let url = URL(string: path) else { return (success: false, path: nil) }
        if ((try? data.write(to: url, options: .atomic)) != nil) {
            return (success: true, path: path)
        } else {
            return (success: false, path: nil)
        }
    }
    
    func copyWithSize(size:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
        self.draw(in: CGRect(0, 0, size.width, size.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func normalizedOrientation() -> UIImage { // photo taken in portrait are roated my image context... this cleans it up
        if (self.imageOrientation == UIImageOrientation.up) { return self }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale);
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        self.draw(in: rect)
        let normalizedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext();
        return normalizedImage;
    }
}
