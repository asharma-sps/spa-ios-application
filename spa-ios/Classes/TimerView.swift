//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import AVFoundation

class TimerView: UIView {
    
    fileprivate let timerLabel = UILabel()
    fileprivate let timerCircle = TimerCircleView()
    fileprivate let completionText: String
    fileprivate let totalSeconds: Int
    fileprivate let alwaysShowMinutes: Bool
    fileprivate var currentSeconds: Int = 0
    fileprivate var timer: Timer?
    
    init(seconds: Int, completionText: String) {
        self.completionText = completionText
        self.totalSeconds = seconds
        self.alwaysShowMinutes = (seconds > 60)
        super.init(frame: CGRect.zero)
        setupTimerLabel()
        setupTimerCircle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupTimerLabel() {
        timerLabel.textAlignment = .center
        timerLabel.textColor = UIColor.brandDarkGray
        configureTimerFont(isShowingCountdown: true)
        addSubview(timerLabel)
    }
    
    private func setupTimerCircle() {
        addSubview(timerCircle)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let fullFrame = CGRect(0.0, 0.0, frame.width, frame.height)
        timerLabel.frame = fullFrame
        timerCircle.frame = fullFrame
    }
}

// MARK:- Public
extension TimerView {
    
    func start() {
        startTimeRemainingUpdateTimer()
    }
    
    func stop() {
        cleanupTimer()
    }
    
    func prepareForReuse() {
        timerCircle.configureForUnstartedTimer()
        timerLabel.text = String(totalSeconds)
        configureTimerFont(isShowingCountdown: true)
    }
}

// MARK:- Timer Management
extension TimerView {
    
    fileprivate func startTimeRemainingUpdateTimer() {
        cleanupTimer()
        currentSeconds = totalSeconds
        timerLabel.text = String(totalSeconds)
        timerCircle.start(duration: Double(totalSeconds + 1)) // timer implementation intentionally adds an extra second (so the users see the full break time for a second)
        configureTimerFont(isShowingCountdown: true)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(onTick), userInfo: nil, repeats: true)
    }
    
    @objc private func onTick() {
        currentSeconds -= 1
        if currentSeconds < 0 {
            timer?.invalidate()
            configureTimerFont(isShowingCountdown: false)
            timerLabel.text = completionText
            cleanupTimer()
            AudioServicesPlaySystemSound(1322)
        } else {
            timerLabel.text = String(TimeFormattingHelper.formattedTime(seconds: currentSeconds, negative: false, alwaysShowMinutes: alwaysShowMinutes))
        }
    }
    
    fileprivate func cleanupTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    fileprivate func configureTimerFont(isShowingCountdown: Bool) {
        timerLabel.font = isShowingCountdown ? UIFont.brandMedium(size: 55.0) : UIFont.brandMedium(size: 30)
    }
}
