//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

struct Country {
    let name: String
    let phoneCode: String
}

class CheckoutCountriesHelper {
    
    class func generateCountries() -> [Country] {
        guard let countryPlist = Bundle.main.path(forResource: "CountriesPhoneCodes", ofType: "plist") else { return [] }
        guard let countryDicts = NSArray(contentsOfFile: countryPlist) else { return [] }
        var countries = [Country]()
        for dict in countryDicts {
            guard let checked = dict as? Dictionary<String, String> else { continue }
            guard let phoneCode = checked["phoneCode"] else { continue }
            guard let name = checked["countryName"] else { continue }
            countries.append(Country(name: name, phoneCode: phoneCode))
        }
        return countries
    }
}
