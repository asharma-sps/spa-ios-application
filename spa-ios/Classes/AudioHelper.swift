//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import AVKit
import AVFoundation

protocol VolumeChangeObserver: class {
    func userDidChangeVolume()
}

// MARK:- Volume Change Tracking
class AudioHelper: NSObject {
    
    private static let singleton = AudioHelper()
    private weak var observer: VolumeChangeObserver?
    
    private override init() {
        super.init()
        listenVolumeButton()
    }
    
    static func setVolumeChangeObserver(observer: VolumeChangeObserver?) {
        AudioHelper.singleton.observer = observer
    }
    
    private func listenVolumeButton() {
        let session = AVAudioSession.sharedInstance()
        try? session.setActive(true)
        session.addObserver(self, forKeyPath: "outputVolume", options: NSKeyValueObservingOptions(), context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "outputVolume" {
            observer?.userDidChangeVolume()
        }
    }
}

// MARK:- Audio Session Configuration
extension AudioHelper {
    
    static func configureAudioSessionCategory(){
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
        } catch {
            errorPrint("Failed to set Shared Audio Session Category", error)
        }
    }
}
