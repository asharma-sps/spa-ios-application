//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import Alamofire

// MARK:- Get Excercise Blcoks
class ExerciseRequestHandler {
    
    typealias ExerciseRequestCompletion = (_ success: Bool, _ exerciseBlock: [ExerciseBlock]?) -> () // remove success ASTODO
    
    func requestExercises(user: User, workoutID: Int, completion: @escaping ExerciseRequestCompletion) {
        let request = Router(.getBlocks(workoutID: workoutID), user) //ASTODO
        Alamofire.request(request) //ASTODO
            .validate()
            .responseJSON() { response in
                switch response.result {
                case .success(let value):
                    guard let checkedValue = value as? [[String: AnyObject]], checkedValue.count > 0, let blocks = self.deserializeExerciseBlocks(data: checkedValue) else {
                        print("ERROR: Failed to deserialize blocks.")
                        completion(false, nil)
                        
                        if let unwrappedResponse = response.data {
                            let responseString = String(data: unwrappedResponse, encoding: .utf8) ?? "(no response data found)"
                            // print("\n[ERROR]: \(message)\n\(error)\n\(responseString)\n[END ERROR]\n")
                            print(response)
                            return
                        }
                        
                        
                        
                        return
                    }
                    completion(true, blocks)
                case .failure(let error):
                    errorPrint("Failed to fetch workout block.", error)
                    completion(false, nil)
                }
        }
    }
}

// MARK:- Get Excerise Data (for offline storage)
extension ExerciseRequestHandler {
    
    func requestExercisesData(user: User, workoutID: Int, completion: @escaping (Bool, Data?) -> ()) {
        let request = Router(.getBlocks(workoutID: workoutID), user) //ASTODO
        Alamofire.request(request)
            .validate()
            .responseData() { response in
                switch response.result {
                case .success(let data):
                    completion(true, data)
                case .failure(let error):
                    errorPrint("Failed to get exercise block data", error)
                    completion(false, nil)
                }
        }
    }
    
    func validateExerciseData(data: Data) -> Bool {
        if let _ = generateExerciseBlocks(data: data) { return true }
        return false
    }
    
    func generateExerciseBlocks(data: Data) -> [ExerciseBlock]? {
        let optionalExerciseDict = try? JSONSerialization.jsonObject(with: data, options: [])
        guard let exerciseDicts = optionalExerciseDict as? [[String: AnyObject]], exerciseDicts.count > 0 else { return nil }
        return deserializeExerciseBlocks(data: exerciseDicts)
    }
}

// MARK:- Deserialization
extension ExerciseRequestHandler {
    
    fileprivate func deserializeExerciseBlocks(data: [[String: AnyObject]]) ->  [ExerciseBlock]? {
        var blocks = [ExerciseBlock]()
        for x in data {
            guard let block = deserializeExerciseBlock(data: x) else { return nil }
            blocks.append(block)
        }
        return blocks
    }

    fileprivate func deserializeExerciseBlock(data: [String: AnyObject]) -> ExerciseBlock? {
        guard let id = data["id"] as? Int else { return nil }
        guard let position = data["position"] as? Int else { return nil }
        guard let sets = data["sets"] as? Int else { return nil }
        guard let exerciseData = data["routine"] as? [[String: AnyObject]] else { return nil }
        let exercises = deserializeExercises(data: exerciseData)
        
        return ExerciseBlock(id: String(id), postion: position, sets: sets, exercises: exercises)
    }
    
    fileprivate func deserializeExercises(data: [[String: AnyObject]]) -> [Exercise] {
        
        var exercises = [Exercise]()
        
        for dict in data {
            guard let id = dict["id"] as? Int else { continue }
            guard let name = dict["name"] as? String else { continue }
            guard let reps = dict["reps"] as? Int else { continue }
            guard let restTime = dict["rest_time"] as? Int else { continue }
            guard let weight = dict["weight"] as? Int else { continue }
            guard let description = dict["description"] as? String else { continue }
            guard let typeString = dict["rep_type"] as? String else { continue }
            guard let type = ExerciseType(rawValue: typeString) else { continue }
            guard let videoData = dict["video"] as? [String: AnyObject] else { continue }
            let video = VideoResource(data: videoData)
            
            let excersise = Exercise(id: String(id), name: name, weight: weight, restTime: restTime, reps: reps, type: type, video: video, description: description)
            exercises.append(excersise)
        }
        return exercises
    }
}
