//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

protocol WeekTableViewDelegate {
    func pushViewControllerForEntry(entry: ScheduleEntry)
}

fileprivate let kDayCellReuseID = "DayCell"

class WeekTV: UITableView {
    
    let selectionDelegate: WeekTableViewDelegate
    let pool: ScheduleEntriesPool
    let startDate: Date
    let dateStrings: [String]
    let entries: [[ScheduleEntry]]
    
    init(startDate: Date, pool: ScheduleEntriesPool, delegate: WeekTableViewDelegate) {
        self.selectionDelegate = delegate
        self.pool = pool
        self.startDate = startDate
        self.entries = pool.getEntriesForWeek(startDate: startDate)
        self.dateStrings = ScheduleDateHelper.generateDateStringsForWeekStartingOnDate(startDate: startDate)
        super.init(frame: CGRect.zero, style: .plain)
        self.configureTableView()
    }
    
    fileprivate func configureTableView() {
        register(DayTableViewCell.self, forCellReuseIdentifier: kDayCellReuseID)
        alwaysBounceVertical = false
        separatorStyle = .none
        delegate = self
        dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension WeekTV: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = dequeueReusableCell(withIdentifier: kDayCellReuseID, for: indexPath) as! DayTableViewCell
        cell.setBottomLineHidden(hidden: indexPath.row == 6) // hides seporator on last row
        if entries.count - 1 > indexPath.row && dateStrings.count - 1 > indexPath.row {
            cell.prepareWithSchedules(dateString: dateStrings[indexPath.row], schedules:entries[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return frame.height / 7
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > entries.count - 1 { return }
        guard let entry = entries[indexPath.row].first else { return }
        selectionDelegate.pushViewControllerForEntry(entry: entry)
    }
}
