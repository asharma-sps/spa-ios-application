//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

// Analytic screen names should have corresponding events that are suffed with "_Viewed"

enum AnalyticScreen: String {
    
    // homescreens
    case homescreen = "Homescreen"
    case login = "Login"
    case createAccount = "Create Account"
    
    // product / checkout
    case product = "Products"
    case vslPlayer = "VSL Player"
    case checkout = "Checkout"

    // content
    case contentPlayer = "Content Player"
    case exerciseBlocksOverview = "Exercise Blocks Overview"
    case exerciseDetail = "Exercise Detail"
    case exeriseWalkthrough = "Exercise Walkthrough"
    
    // other
    case schedule = "Schedule"
    case progress = "Progress"
    case settings = "Settings"
    
    var eventName: String {
        return "\(rawValue) Viewed"
    }
}

enum AnalyticEventNames: String {
    case purchaseAttempted = "Purchase_Attempted"
}

enum AnalyticIncrementedProfileAttribute: String {
    case totalExerciseDetailViews = "total_exercise_detail_views"
    case totalExerciseWalkthroughViews = "total_exercise_walkthrough_views"
    case totalExerciseOverviewViews = "total_content_player_views"
    case totalNumberOfTimesProgramScheduled = "total_number_of_times_program_scheduled"
}

enum AnalyticAssignedProfileAttribute: String {
    case currentlyHasProgramScheduled = "currently_has_program_scheduled"
    case progressEntriesCount = "progress_entries_count"
}

struct AnalyticAttribute {
    static let product = "product"
    static let medium = "medium"
    static let success = "success"
    static let price = "price"
    static let productID = "product_ID"
    static let workoutID = "workout_ID"
    static let exerciseID = "exercise_ID"
    static let workoutType = "workout_type"
    static let exerciseBlockID = "exercise_block_ID"
    static let completed = "completed"
    static let percentComplete = "percent_complete"
    static let undefined = "undefined"
}
