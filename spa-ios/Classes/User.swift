//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//


import Foundation

enum AuthenticationMethod: String {
    case native = "native"
    case google = "google"
    case facebook = "facebook"
}

struct User {
    let firstName: String
    let lastName: String
    let email: String
    let token: String
    let id: String
    let authenticationMethod: AuthenticationMethod
}
