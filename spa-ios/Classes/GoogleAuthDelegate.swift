//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import ASGlobalOverlay

class GoogleAuthDelegate: NSObject, GIDSignInDelegate {
    
    static let singleton = GoogleAuthDelegate() // make funcational
    private override init() {}
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        ASGlobalOverlay.showWorkingIndicator(withDescription: nil)
        
        guard error == nil else {
            errorPrint("Failed to get Google OAuth token.", error)
            ErrorReporter.report(error: error, message: "Failed to get Google OAuth token.")
            ASGlobalOverlay.showAlert(withTitle: "Unable to Sign In", message: "Please check your network settings or contact support.", dismissButtonTitle: "OK")
            return
        }
        
        guard let token = user.userID else {
            errorPrint("Failed to extract auth token from Google response", error)
            ErrorReporter.report(error: error, message: "Failed to extract auth token from Google response")
            ASGlobalOverlay.showAlert(withTitle: "Unable to Sign In", message: "Please check your network settings or contact support.", dismissButtonTitle: "OK")
            return
        }
        let first = user.profile.givenName ?? ""
        let last = user.profile.familyName ?? ""
        let email = user.profile.email ?? ""
        // print("first: \(first), last: \(last), email: \(email), token: \(token)") // DEBUG
        
        AuthenticationRequestHandler.loginSocial(token: token, vendor: .google, first: first, last: last, email: email) { result in
            switch result {
            case .success(let user):
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    appDelegate.setRootViewController(withUser: user)
                }
                ASGlobalOverlay.dismissWorkingIndicator()
            case .failure(let errorMessage):
                ASGlobalOverlay.showAlert(withTitle: "Error", message: errorMessage, dismissButtonTitle: "OK")
            }
        }
    }
}
