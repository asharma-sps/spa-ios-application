//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

extension UIViewController {
    
    func addCloseButton() {
        let exit = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.stop, target: self, action: #selector(dismissSelf))
        self.navigationItem.leftBarButtonItem = exit
    }
    
    func dismissSelf() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupBlankBackButton() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}
