//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

protocol ProgressCardScrollerDelegate {
    func selectedProgressCardEntryDidChange(index: Int)
}

// MARK:- Setup
class ProgressCardScrollerPVC: UIPageViewController {
    
    fileprivate var selectionDelegate: ProgressCardScrollerDelegate?
    fileprivate var progressEntries: [ProgressEntry] = []
    fileprivate var progressCardViewControllers: [ProgressCardVC] = []
    fileprivate(set) var currentIndex = 0
    fileprivate var nextIndex = 0
    
    init() {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
    }
}

// MARK:- Public
extension ProgressCardScrollerPVC {
    
    func setProgressEntries(entries: [ProgressEntry]) {
        progressEntries = entries
        progressCardViewControllers = generateProgressCardViewControllers(entries: entries)
        if !progressCardViewControllers.isEmpty { displayCardAtIndex(index: 0) }
    }
    
    func setSelectedProgressEntry(index: Int) {
        if progressCardViewControllers.count - 1 >= index { displayCardAtIndex(index: index) }
    }
    
    func setSelectionDelegate(delegate: ProgressCardScrollerDelegate) {
        self.selectionDelegate = delegate
    }
    
    func addNewProgressEntry(entry: ProgressEntry) {
        let cardVC = ProgressCardVC(entry: entry)
        progressEntries.append(entry)
        progressCardViewControllers.append(cardVC)
    }
    
    func currentlySelectedIndex() -> Int? {
        guard let vc = viewControllers?.first else { return nil }
        guard let currentIndex = indexOfProgressCardViewController(card: vc as! ProgressCardVC) else { return nil }
        return currentIndex
    }
}

// MARK:- Internal
extension ProgressCardScrollerPVC {
    
    fileprivate func displayCardAtIndex(index: Int) {
        self.setViewControllers([progressCardViewControllers[index]], direction: .forward, animated: false, completion: nil)
    }
    
    fileprivate func generateProgressCardViewControllers(entries:[ProgressEntry]) -> [ProgressCardVC] {
        var cards: [ProgressCardVC] = []
        for entry in entries.reversed() { // so it fetches the most recent images first
            let card = ProgressCardVC(entry: entry)
            cards.append(card)
        }
        return cards.reversed() // so it's chronologically ordered
    }
}

// MARK:- UIPageViewController Delegates
extension ProgressCardScrollerPVC: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = indexOfProgressCardViewController(card: viewController as! ProgressCardVC) else { return nil }
        if currentIndex + 1 > progressCardViewControllers.count - 1 { return nil }
        let nextCard = progressCardViewControllers[currentIndex + 1]
        return nextCard
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = indexOfProgressCardViewController(card: viewController as! ProgressCardVC) else { return nil }
        if currentIndex - 1 < 0 || progressCardViewControllers.isEmpty { return nil }
        let priorCard = progressCardViewControllers[currentIndex - 1]
        return priorCard
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            guard let controllers = pageViewController.viewControllers else { return }
            if controllers.isEmpty { return }
            guard let index = indexOfProgressCardViewController(card: controllers[0] as! ProgressCardVC) else { return }
            selectionDelegate?.selectedProgressCardEntryDidChange(index: index)
        }
    }
    
    func indexOfProgressCardViewController(card: ProgressCardVC) -> Int? {
        guard let index = progressCardViewControllers.index(of: card) else { return nil }
        return index
    }
}
