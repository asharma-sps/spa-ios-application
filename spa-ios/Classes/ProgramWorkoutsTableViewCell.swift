//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

private let kPreviewImageViewSize = CGSize(125.0, 75.0)

// MARK:- Setup
class ProgramWorkoutsTableViewCell: UITableViewCell {
    
    static let reuseID = "program_workout_cell_reuse_id"
    let previewImageView = NetworkImageView()
    let workoutLabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        accessoryType = .disclosureIndicator
        selectionStyle = .none
        setupPreviewImageView()
        setupWorkoutLabel()
    }
    
    private func setupPreviewImageView() {
        previewImageView.layer.masksToBounds = true
        previewImageView.layer.cornerRadius = 4.0
        addSubview(previewImageView)
    }
    
    private func setupWorkoutLabel() {
        workoutLabel.font = UIFont.brandMedium(size: 20.0)
        workoutLabel.textColor = UIColor.brandDarkGray
        workoutLabel.numberOfLines = 2
        addSubview(workoutLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK:- Prepare
extension ProgramWorkoutsTableViewCell {

    func prepare(workout: Workout) {
        var titleText = workout.title
        if workout.contentLocked {
            titleText = "🔒" + titleText
        }
        workoutLabel.text = titleText
        backgroundColor = workout.status == .Locked ? UIColor.brandLightGray : UIColor.white
        guard let url = URL(string: workout.cover) else { return }
        previewImageView.setImageFromURL(imageURL: url)
    }
}

// MARK:- Layout
extension ProgramWorkoutsTableViewCell {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let space = CGFloat(20.0)
        previewImageView.frame = CGRect(space, (frame.height - kPreviewImageViewSize.height) / 2, kPreviewImageViewSize.width, kPreviewImageViewSize.height)
        workoutLabel.frame = CGRect(kPreviewImageViewSize.width + space * 2, 0.0, frame.width - kPreviewImageViewSize.width - space * 4, frame.height)
    }
}
