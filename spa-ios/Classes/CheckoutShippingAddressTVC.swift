//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//
import UIKit
import ASGlobalOverlay

protocol CheckoutShippingAddressTableViewControllerDelegate: class {
    var shippingAddress: Address?  { get set }
}

class CheckoutShippingAddressTVC: UITableViewController, CheckoutCountryPickerViewDelegate {
    
    @IBOutlet weak var firstNameField: FormTextField!
    @IBOutlet weak var lastNameField: FormTextField!
    @IBOutlet weak var streetField: FormTextField!
    @IBOutlet weak var cityField: FormTextField!
    @IBOutlet weak var stateField: FormTextField!
    @IBOutlet weak var countryField: FormPickerTextField!
    @IBOutlet weak var zipField: FormTextField!
    @IBOutlet weak var saveButton: UIButton!
    

    var orderedFields = [FormTextField]()
    weak var delegate: CheckoutShippingAddressTableViewControllerDelegate?
    var country: String? {
        didSet {
            countryField.text = country
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupOrderedFields()
        setupCountryInputView()
        loadAddressFromDelegate()
    }
    
    private func configureView() {
        let clearButton = UIBarButtonItem(title: "Clear", style: .plain, target: self, action: #selector(clearShippingAddress))
        navigationItem.rightBarButtonItem = clearButton
    }
    
    private func setupOrderedFields() {
        orderedFields = [firstNameField, lastNameField, streetField, cityField, stateField, countryField, zipField]
        for i in orderedFields {
            i.nextButtonDelegate = self
        }
        zipField.setAccessoryTitle(accessoryTitle: .save)
    }
    
    private func setupCountryInputView() {
        countryField.inputView = CheckoutCountryPV(delegate: self)
    }
    
    private func loadAddressFromDelegate() {
        guard let address = delegate?.shippingAddress else { return }
        firstNameField.text = address.firstName
        lastNameField.text = address.lastName
        streetField.text = address.street
        cityField.text = address.city
        stateField.text = address.state
        countryField.text = address.country
        zipField.text = address.zipCode
    }
}

// MARK:- Update Shipping Address
extension CheckoutShippingAddressTVC {
    
    fileprivate func setDelegateShippingAddressIfPossible() -> Bool {
        
        guard let first = firstNameField.validatedString() else { return false }
        guard let last = lastNameField.validatedString() else { return false }
        guard let street = streetField.validatedString() else { return false }
        guard let city = cityField.validatedString() else { return false }
        guard let state = stateField.validatedString() else { return false }
        guard let country = countryField.validatedString() else { return false }
        guard let zip = zipField.validatedString() else { return false }
        
        delegate?.shippingAddress = Address(firstName: first,
                                                lastName: last,
                                                street: street,
                                                city: city,
                                                state: state,
                                                country: country,
                                                zipCode: zip)
        return true
    }
}

// MARK:- Button Targets
extension CheckoutShippingAddressTVC {
    
    @objc fileprivate func clearShippingAddress() {
        delegate?.shippingAddress = nil
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed() {
        if !setDelegateShippingAddressIfPossible() {
            dismissAnyFirstResponder()
            ASGlobalOverlay.showAlert(withTitle: "Missing Fields", message: "Please make sure all fields are completed", dismissButtonTitle: "OK")
        } else {
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
}

// MARK:- Other
extension CheckoutShippingAddressTVC: FormTextFieldDelegate {
    
    func nextButtonPressed(sender: FormTextField) {
        guard let index = orderedFields.index(of: sender) else { return }
        if index > orderedFields.count { return } // index + 1 cancels out orderedFields.count -1
        if index + 1 == orderedFields.count { saveButtonPressed(); sender.resignFirstResponder() }
        else { orderedFields[index + 1].becomeFirstResponder() }
    }
    
    fileprivate func dismissAnyFirstResponder() {
        for i in orderedFields {
            i.resignFirstResponder()
        }
    }
}

