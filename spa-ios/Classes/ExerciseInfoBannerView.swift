//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

private let kLabelSidePadding = CGFloat(15.0)
private let kTimerButtonSquareDimension = CGFloat(32.0)

protocol ExerciseInfoBannerViewDelegate: class {
    func timerModeButtonToggled(selected: Bool)
}

// MARK:- Setup
class ExerciseInfoBannerView: UIView {
    
    static let suggestedHeight = CGFloat(75.0)
    
    fileprivate let labelContainer = UIView()
    fileprivate let nameLabel = UILabel()
    fileprivate let repetitionsLabel = UILabel()
    fileprivate let dividerLine = UIView()
    fileprivate let exercise: Exercise
    fileprivate var timerButton: UIButton?
    fileprivate var timerButtonSelected = false
    weak var delegate: ExerciseInfoBannerViewDelegate?
    
    init(exercise: Exercise) {
        self.exercise = exercise
        super.init(frame: CGRect.zero)
        setupLabelContainer()
        setupNameLabel()
        setupRepetitionLabel()
        setupDividerLine()
        setupTimerButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupLabelContainer() {
        addSubview(labelContainer)
    }
    
    private func setupNameLabel() {
        nameLabel.text = exercise.name
        nameLabel.font = UIFont.brandMedium(size: 22.0)
        nameLabel.textColor = UIColor.brandDarkGray
        labelContainer.addSubview(nameLabel)
    }
    
    private func setupRepetitionLabel() {
        repetitionsLabel.text = exercise.typeDescription
        repetitionsLabel.font = UIFont.brandRegular(size: 16.0)
        repetitionsLabel.textColor = UIColor.brandDarkGray
        labelContainer.addSubview(repetitionsLabel)
    }
    
    private func setupDividerLine() {
        dividerLine.backgroundColor = UIColor.brandGray
        addSubview(dividerLine)
    }
    
    private func setupTimerButton() {
        guard exercise.type == .seconds else { return }
        timerButton = UIButton(type: .system)
        timerButton?.setImage(UIImage(named: "spa_stop_watch"), for: .normal)
        timerButton?.addTarget(self, action: #selector(timerButtonPressed), for: .touchUpInside)
        timerButton?.layer.masksToBounds = true
        timerButton?.layer.cornerRadius = 4.0
        if let unwrapped = timerButton {
            addSubview(unwrapped)
        }
    }
}

// MARK:- Helpers
extension ExerciseInfoBannerView {

    @objc func timerButtonPressed() {
        timerButtonSelected = !timerButtonSelected
        updateButtonAppearance(selected: timerButtonSelected)
        delegate?.timerModeButtonToggled(selected: timerButtonSelected)
    }
    
    func reset() {
        timerButtonSelected = false
        updateButtonAppearance(selected: false)
    }
    
    private func updateButtonAppearance(selected: Bool) { // can't set button color for state. May as well just do it manually
        let forgroundColor = selected ? UIColor.white : UIColor.brandAccentBlue
        let backgroundColor = selected ? UIColor.brandAccentBlue : UIColor.white
        timerButton?.tintColor = forgroundColor
        timerButton?.backgroundColor = backgroundColor
    }
}

// MARK:- Layout
extension ExerciseInfoBannerView {

    override func layoutSubviews() { //ASTODO clean this up...
        super.layoutSubviews()
        let labelWidth = (timerButton == nil) ? (frame.width - kLabelSidePadding * 2) : (frame.width - kLabelSidePadding * 3 - kTimerButtonSquareDimension)
        labelContainer.frame = CGRect(kLabelSidePadding, 5.0, labelWidth, frame.height - 10.0)
        
        if timerButton != nil {
            let timerButtonX = frame.width - kLabelSidePadding - kTimerButtonSquareDimension
            let timerButtonY = (frame.height - kTimerButtonSquareDimension) / 2
            timerButton?.frame = CGRect(timerButtonX, timerButtonY, kTimerButtonSquareDimension, kTimerButtonSquareDimension)
        }
        
        nameLabel.frame = CGRect(0.0, 0.0, labelContainer.frame.width, labelContainer.frame.height * 0.55)
        repetitionsLabel.frame = CGRect(0.0, nameLabel.frame.height, labelContainer.frame.width, labelContainer.frame.height * 0.35)
        dividerLine.frame = CGRect(0.0, frame.height - Environment.thinnestLine, frame.width, Environment.thinnestLine)
    }
}
