//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

extension URL {
    
    static func resourceStoragePath() -> URL {
        let support = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true)[0]
        let encoded = support.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!
        let url = URL(string: encoded)!
        let resourceDirectory = url.appendingPathComponent("resource_path")
        return resourceDirectory
    }
    
    static func documentDirectoryPath() -> URL {
        let docPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        return URL(string: docPathString)!
    }
    
    static func clearDocumentDirectory() {
        let docDirectory = documentDirectoryPath()
        guard let contents = try? FileManager.default.contentsOfDirectory(atPath: docDirectory.path) else { return }
        for x in contents {
            if x.contains(".localytics") { continue }
            let filePath = docDirectory.appendingPathComponent(x)
            PersistanceHelper.delete(path: filePath)
        }
    }
}
