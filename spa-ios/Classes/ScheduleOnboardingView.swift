//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

fileprivate let kScheduleOnboardingView = "ScheduleOnboardingView"

protocol ScheduleOnboardingViewDelegate {
    func setupScheduleButtonPressed()
}

class ScheduleOnboardingView: UIView {
    
    var delegate: ScheduleOnboardingViewDelegate?
    
    class func generateScheduleOnboardingViewFromNib(delegate: ScheduleOnboardingViewDelegate) -> ScheduleOnboardingView {
        guard let view = Bundle.main.loadNibNamed(kScheduleOnboardingView, owner: self, options: nil)!.first as? ScheduleOnboardingView else {
            return ScheduleOnboardingView()
        }
        view.delegate = delegate
        return view
    }
    
    @IBAction func setupAScheduleButtonPressed(_ sender: AnyObject) {
        delegate?.setupScheduleButtonPressed()
    }
}
