//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

extension UIFont {
    
    class func brandRegular(size: CGFloat = 14) -> UIFont {
        return UIFont(name: "AvenirNext-Regular", size: size)!
    }
    
    class func brandMedium(size: CGFloat = 16) -> UIFont {
        return UIFont(name: "AvenirNext-Medium", size: size)!
    }
    
    class func brandBold(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "AvenirNext-DemiBold", size: size)!
    }
    
    class func brandHeavy(size: CGFloat = 20) -> UIFont {
        return UIFont(name: "Avenir-Heavy", size: size)!
    }
}
