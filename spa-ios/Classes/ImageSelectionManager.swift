//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

enum ImageMode {
    case Library
    case Camera
}

protocol ImageSelectionManagerDelegate {
    func presentImagePickerController(imagePicker: UIImagePickerController)
}

typealias ImageSelectionManagerCompletion = (_ userCancelled: Bool,_ success: Bool,_ image: UIImage?) -> Void

class ImageSelectionManager: NSObject{
    
    let imagePickerController = UIImagePickerController()
    var delegate: ImageSelectionManagerDelegate?
    var completion: ImageSelectionManagerCompletion?
    
    func promptUserToSelectPhoto(delegate: ImageSelectionManagerDelegate, prompt: String?, completion: @escaping ImageSelectionManagerCompletion) {
        let userPrompt = prompt ?? "Select an image source"
        imagePickerController.delegate = self
        self.delegate = delegate
        self.completion = completion
        
        let camera = ASUserOption(title: "Camera") {
            self.imagePickerController.sourceType = .camera
            self.delegate?.presentImagePickerController(imagePicker: self.imagePickerController)
        }
        let library = ASUserOption(title: "Library") {
            self.imagePickerController.sourceType = .photoLibrary
            self.delegate?.presentImagePickerController(imagePicker: self.imagePickerController)
        }
        let cancel = ASUserOption.cancel(withTitle: "Cancel", actionBlock: nil)
        ASGlobalOverlay.showSlideUpMenu(withPrompt: userPrompt, userOptions: [camera as Any, library as Any, cancel as Any])
    }
    
    deinit {
        completion = nil
        delegate = nil
    }
}

// MARK:- UIImagePickerControllerDelegates
extension ImageSelectionManager: UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePickerController.dismiss(animated: true, completion: nil)
        if self.completion == nil { return }
        self.completion!(true, false, nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePickerController.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print(image.imageOrientation.rawValue)
            self.completion!(false, true, image)
        } else {
            self.completion!(false, false, nil)
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        self.completion!(false, true, image)
    }
}
