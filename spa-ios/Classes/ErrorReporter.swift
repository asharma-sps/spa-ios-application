//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import Crashlytics
import Fabric

class ErrorReporter {
    
    static func report(error: Error, message: String? = nil) {
        guard !Environment.isDebugBuild else { return }
        if let unwrappedMessage = message {
            Crashlytics.sharedInstance().recordError(error, withAdditionalUserInfo: ["message": unwrappedMessage])
        } else {
            Crashlytics.sharedInstance().recordError(error)
        }
    }
    
    static func log(_ message: String) {
        guard !Environment.isDebugBuild else {
            print("[CRASHALYTICS]: \(message)")
            return
        }
        CLSLogv(message, getVaList([]))
    }
    
    static func set(state: String) {
        guard !Environment.isDebugBuild else { return }
        Crashlytics.sharedInstance().setObjectValue(state, forKey: "current_state")
    }
}
