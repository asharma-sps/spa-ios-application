//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//


import Foundation
import ASGlobalOverlay

// MARK:- Setup
class HomescreenForgotPasswordTVC: UITableViewController {
    
    @IBOutlet weak var emailTextField: FormTextField!
    @IBOutlet weak var resetPasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView() {
        resetPasswordButton.configure()
        emailTextField.inputAccessoryView = nil
    }
    
    @IBAction func resetPasswordButtonPressed(_ sender: AnyObject) {
        emailTextField.resignFirstResponder()
        guard let email = emailTextField.text else {
            ASGlobalOverlay.showAlert(withTitle: "Error", message: "Please input your email.", dismissButtonTitle: "OK")
            return
        }
        ASGlobalOverlay.showWorkingIndicator(withDescription: nil)
        AuthenticationRequestHandler.sendPasswordResetEmail(email: email) { message in
            let ok = ASUserOption(title: "OK", actionBlock: { _ = self.navigationController?.popViewController(animated: true) })
            ASGlobalOverlay.showAlert(withTitle: "Information", message: message, userOptions: [ok as Any])
        }
    }
}
