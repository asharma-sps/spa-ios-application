//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

private let kStrokeAnimationKey = "strokeEnd"

private enum Context {
    case Background
    case Progress
}

class TimerCircleView: UIView {
    
    fileprivate let backgroundLayer = TimerCircleView.generateCircularShapeLayer(strokeColor: UIColor.brandUltraLightGray)
    fileprivate let foregroundLayer = TimerCircleView.generateCircularShapeLayer(strokeColor: UIColor.brandAccentBlue)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    
    private func setup() {
        layer.addSublayer(backgroundLayer)
        layer.addSublayer(foregroundLayer)
        setNeedsLayout()
    }
}

// MARK:- Public / Control
extension TimerCircleView {
    
    func configureForUnstartedTimer() {
        foregroundLayer.strokeEnd = 0
    }
    
    func start(duration: Double) {
        foregroundLayer.removeAllAnimations()
        let animator = CABasicAnimation(keyPath: kStrokeAnimationKey)
        animator.fromValue = 0
        animator.toValue = 1
        animator.duration = duration
        foregroundLayer.add(animator, forKey: kStrokeAnimationKey)
    }
}

// MARK:- Layout
extension TimerCircleView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundLayer.path = path()
        foregroundLayer.path = path()
    }
    
    private func path() -> CGPath {
        let center = CGPoint(x: layer.bounds.midX, y: layer.bounds.midY)
        let radius = min(layer.bounds.width, layer.bounds.height) / 2.5
        return UIBezierPath(arcCenter: center, radius: radius, startAngle: CGFloat(-M_PI_2), endAngle: CGFloat((M_PI * 2.0) - M_PI_2), clockwise: true).cgPath
    }
}

// MARK:- Layer Generator
private extension TimerCircleView {
    
    class func generateCircularShapeLayer(strokeColor: UIColor) -> CAShapeLayer {
        let layer = CAShapeLayer()
        layer.lineCap = kCALineCapRound
        layer.lineWidth = 10.0
        layer.fillColor = UIColor.clear.cgColor
        layer.strokeColor = strokeColor.cgColor
        return layer
    }
}
