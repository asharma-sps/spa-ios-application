//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

class Session {
    
    let user: User
    let productsPool = ProductsPool()
    
    init(user: User) {
        self.user = user
    }
}
