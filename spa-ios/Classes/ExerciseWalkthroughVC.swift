//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

class ExerciseWalkthroughVC: AnalyticBaseViewController {
    
    let exerciseBlock: ExerciseBlock
    let exerciseDetailViewControllers: [ExerciseBaseCardVC]
    let pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    
    var exitConfirmationNotRequired = false
    var currentSetLocked = false
    var currentSet = 0 {
        didSet {
            navigationItem.title = "Set \(currentSet)"
            if let setCompletionVC = exerciseDetailViewControllers.last as? ExerciseSegmentCompleteCardVC {
                if currentSet == exerciseBlock.sets {
                    setCompletionVC.configureForCompletedWorkout()
                } else {
                    setCompletionVC.configureLabels(currentSet: currentSet, totalSets: exerciseBlock.sets)
                }
            }
        }
    }
    
    init(exerciseBlock: ExerciseBlock, productID: Int) {
        self.exerciseBlock = exerciseBlock
        self.exerciseDetailViewControllers = ExerciseWalkthroughVC.generateExerciseDetailCardViewControllers(exerciseBlock: exerciseBlock, productID: productID)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupPageViewController()
    }
    
    private func configureView() {
        navigationItem.title = "Welcome"
        view.backgroundColor = UIColor.groupTableViewBackground
        addCloseButton()
    }

    private func setupPageViewController() {
        pageController.delegate = self
        pageController.dataSource = self
        addChildViewController(pageController)
        view.addSubview(pageController.view)
        pageController.didMove(toParentViewController: self)
        if exerciseBlock.exercises.count < 1 { return }
        pageController.setViewControllers([exerciseDetailViewControllers[0]], direction: .forward, animated: false, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        pageController.view.frame = CGRect(0.0, 0.0, view.frame.width, view.frame.height)
    }
    
    private class func generateExerciseDetailCardViewControllers(exerciseBlock: ExerciseBlock, productID: Int) -> [ExerciseBaseCardVC] {
        var cardVCs = [ExerciseBaseCardVC]()
        cardVCs.append(ExerciseInstructionsCardVC())
        for exercise in exerciseBlock.exercises {
            cardVCs.append(ExerciseDetailCardVC(exercise: exercise, productID: productID))
            if exercise.restTime > 0 {
                cardVCs.append(ExerciseBreakTimerCardVC(seconds: exercise.restTime))
            }
        }
        cardVCs.append(ExerciseSegmentCompleteCardVC())
        return cardVCs
    }
}

// MARK:- UIPageViewController Delegates
extension ExerciseWalkthroughVC: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = indexOfExerciseDetailCardViewController(vc: viewController as! ExerciseBaseCardVC) else { return nil }
        if currentIndex + 1 > exerciseDetailViewControllers.count - 1 {
            if currentSet >= exerciseBlock.sets {
                exitConfirmationNotRequired = true;
                return nil
            }
            return exerciseDetailViewControllers[1] // restarts and skips the first card (which is a welcome screen)
        }
        let nextCard = exerciseDetailViewControllers[currentIndex + 1]
        return nextCard
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if completed {
            // This logic forces the pageViewController to re-query the datasource delegate for the previous view controller instead of assuming it (based on the prior displayed card)
            guard let controllers = pageViewController.viewControllers else { return }
            if controllers.isEmpty { return }
            DispatchQueue.main.async { // For some reason a crash occurs w/o this even though it's already on the main thread. StackOverflow is confident it's an internal Cocoa issue.
                pageViewController.setViewControllers(controllers, direction: .forward, animated: false, completion: nil)
            }
            
            if controllers[0] as? ExerciseSegmentCompleteCardVC != nil {
                currentSetLocked = false
            }
            
            // checks if current card is [1]th card (which is the card after the ExerciseSegmentCompleteCardVC is show).
            if let currentCard = controllers[0] as? ExerciseDetailCardVC, let index = indexOfExerciseDetailCardViewController(vc: currentCard), index == 1 {
                incrementCurrentSet()
                currentSetLocked = true
            }
        }
    }
    
    private func incrementCurrentSet() {
        if currentSet < exerciseBlock.sets && !currentSetLocked {
            currentSet += 1
        }
    }
    
    func indexOfExerciseDetailCardViewController(vc: ExerciseBaseCardVC) -> Int? {
        guard let index = exerciseDetailViewControllers.index(of: vc) else { return nil }
        return index
    }
}

// MARK:- Exit Confirmation
extension ExerciseWalkthroughVC {
    
    override func dismissSelf() {
        guard !exitConfirmationNotRequired else {
            super.dismissSelf()
            return
        }
        let exit = ASUserOption.destructiveUserOption(withTitle: "Exit") { super.dismissSelf() }
        let cancel = ASUserOption.cancel(withTitle: "Cancel", actionBlock: nil)
        ASGlobalOverlay.showSlideUpMenu(withPrompt: "Are you sure you would like to stop this workout?", userOptions: [exit as Any, cancel as Any])
    }
}

// MARK:- Analytics
extension ExerciseWalkthroughVC {
    
    override func analyticsViewDidAppear() {
        Analytics.screenDidAppear(screen: .exeriseWalkthrough)
        Analytics.incrementProfileAttributeValue(key: .totalExerciseWalkthroughViews)
    }
    
    override func analyticsViewDidDisappear() {
        let percentComplete = String(format: "%.1f", Double(currentSet) / Double(exerciseBlock.sets))
        Analytics.screenDidDissapear(screen: .exeriseWalkthrough, attributes: [AnalyticAttribute.exerciseBlockID:  exerciseBlock.id,
                                                                               AnalyticAttribute.completed:        String(exitConfirmationNotRequired),
                                                                               AnalyticAttribute.percentComplete:  percentComplete])

    }
}
