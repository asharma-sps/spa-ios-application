//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import Alamofire

// Mark:- GET
class ProgressRequestHandler {
    
    typealias ProgressEntriesRequestCompletion = (_ success: Bool, _ entries: [ProgressEntry]) -> ()
    
    func requestProgressEntries(user: User, completion: @escaping ProgressEntriesRequestCompletion) {
        Alamofire.request(Router(.getProgress, user))
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    if let entries = self.deserializeProductEntries(value: value), let data = response.data {
                        ProgressDataCacheHelper.set(data: data)
                        completion(true, entries)
                    } else {
                        completion(false, [])
                    }
                case .failure(let error):
                    errorPrint("Failed to fetch progress entries", error)
                    if let entries = self.unpackCachedData(data: ProgressDataCacheHelper.load()) {
                        completion(true, entries)
                    } else {
                        completion(false, []);
                    }
                }
        }
    }
    
    private func unpackCachedData(data: Data?) -> [ProgressEntry]? {
        guard let unwrappedData = data else { return nil }
        guard let JSON = try? JSONSerialization.jsonObject(with: unwrappedData, options: []) else { return nil }
        return self.deserializeProductEntries(value: JSON)
    }
}

// Mark:- Deserialize
extension ProgressRequestHandler {
    
    fileprivate func deserializeProductEntries(value: Any?) -> [ProgressEntry]? {
        guard let unwrapped = value as? NSArray else { return nil }
        var entries: [ProgressEntry] = []
        for dict in unwrapped {
            if let progress = parseProductEntry(possibleDict: dict) {
                entries.append(progress)
            }
        }
        return entries
    }
    
    private func parseProductEntry(possibleDict: Any) -> ProgressEntry? {
        guard let data = possibleDict as? NSDictionary else { return nil }
        guard let dateString = data["created_at"] as? String else { return nil } // without a date we can't order the progress
        if let date = dateForDateString(dateString: dateString) {
            guard let id = data["id"] as? Int else { return nil }
            let weight = data["weight"] as? String ?? "--"
            let imageURL = data["md"] as? String ?? ""
            return ProgressEntry(id: id, weight: weight, date: date, imageURL: imageURL)
        }
        return nil
    }
    
    private func dateForDateString(dateString: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        if let date = formatter.date(from: dateString) { return date }
        return nil
    }
}

// Mark:- POST
extension ProgressRequestHandler {
    
    typealias ProgressEntriesPostCompletion = (_ success: Bool) -> ()
    
    func postProgressEntry(user: User, image: UIImage, weight: String, completion: @escaping ProgressEntriesPostCompletion) {
        guard let multipartFormData = generateMultipartDataClosure(weight: weight, image: image) else { completion(false); return }
        let request = Router(.postProgress, user)
        Alamofire.upload(multipartFormData: multipartFormData, with: request) { result in
            switch result {
            case .success(let upload,_,_):
                upload.responseJSON { response in
                    switch response.result {
                    case .success(_):
                        completion(true)
                    case .failure(_):
                        completion(false)
                    }
                }
            case .failure(let error):
                errorPrint("Failed to upload progress picture.", error)
                completion(false)
            }
        }
    }
    
    func generateMultipartDataClosure(weight: String, image: UIImage) -> ((MultipartFormData) -> Void)? {
        let imageData = UIImagePNGRepresentation(image)
        guard let weightData = weight.data(using: String.Encoding.utf8, allowLossyConversion: false) else { return nil }
        let multiFormDataClosure: (MultipartFormData) -> Void = { multipartFormData in
            multipartFormData.append(imageData!, withName: "file[]", fileName: "file.png", mimeType: "image/png")
            multipartFormData.append(weightData, withName: "weight")
        }
        return multiFormDataClosure
    }
}

// Mark:- DELETE
extension ProgressRequestHandler {
    
    typealias ProgressDeleteEntryRequestCompletion = (_ success: Bool) -> ()

    func deleteProgressEntry(user: User, id: String, completion: @escaping ProgressDeleteEntryRequestCompletion) {
        let request = Router(.deleteProgress(id: id), user)
        Alamofire.request(request)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    ProgressDataCacheHelper.clear()
                    completion(true)
                case .failure(let error):
                    errorPrint("Failed to delete progress entry.", error)
                    completion(false)
                }
        }
    }
}
