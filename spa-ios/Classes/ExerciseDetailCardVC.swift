//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class ExerciseDetailCardVC: ExerciseBaseCardVC {
    
    let exerciseDetailVC: ExerciseDetailVC
    
    init(exercise: Exercise, productID: Int) {
        exerciseDetailVC = ExerciseDetailVC(exercise: exercise, productID: productID, usedForExerciseCard: true)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupExerciseDetailVC()
    }
    
    private func setupExerciseDetailVC() {
        addChildViewController(exerciseDetailVC)
        containerView.addSubview(exerciseDetailVC.view)
        exerciseDetailVC.didMove(toParentViewController: self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerView.frame = CGRect(15.0, 15.0, view.frame.width - 30.0, view.frame.height - 30.0)
        exerciseDetailVC.view.frame = CGRect(0.0, 0.0, containerView.frame.width, containerView.frame.height)
    }
}
