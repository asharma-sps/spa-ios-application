//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

// MARK:- Setup
class ProgramWorkoutsTVC: UITableViewController {
    
    enum TitleText: String {
        case Videos = "Videos"
        case DemoVideos = "Demo Videos"
    }
    
    let section: Section
    let titleText: TitleText
    let session: Session

    init(section: Section, titleText: TitleText, session: Session) {
        self.section = section
        self.titleText = titleText
        self.session = session
        super.init(style: .grouped)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupTableView()
    }
    
    private func configureView() {
        setupBlankBackButton()
        title = titleText.rawValue
    }
    
    private func setupTableView() {
        tableView.register(ProgramWorkoutsTableViewCell.self, forCellReuseIdentifier: ProgramWorkoutsTableViewCell.reuseID)
    }
}

// MARK:- UITableViewDataSource
extension ProgramWorkoutsTVC {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.section.workouts.count
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProgramWorkoutsTableViewCell.reuseID) as! ProgramWorkoutsTableViewCell
        let workout = section.workouts[indexPath.item]
        cell.prepare(workout: workout)
        return cell
    }
}

// MARK:- UITableVieweDelegate
extension ProgramWorkoutsTVC {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let workout = section.workouts[indexPath.row]
        guard !workout.contentLocked else {
            ASGlobalOverlay.showAlert(withTitle: "Locked", message: "This content is not yet available.", dismissButtonTitle: "OK")
            return
        }
        let player = ProductContentVideoPlayerVC(session: session, workout: workout)
        navigationController?.pushViewController(player, animated: true)
    }
}
