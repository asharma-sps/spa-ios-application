//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import Alamofire

//  MARK:- POST
class CheckoutRequestHandler {
    
    typealias CheckoutRequestHandler = (_ success: Bool, _ orderID: String) -> ()
    
    func processCheckout(user: User, order: OrderPacket, completion: @escaping CheckoutRequestHandler) {
        
        let parameters = generateOrderParamaters(order: order)
        let request = Router(.postCheckout(parameters: parameters), user)
        Alamofire.request(request)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    guard let checkedValue = value as? [String: AnyObject], let orderID = self.extractOrderID(successDict: checkedValue) else {
                        completion(false, "")
                        return
                    }
                    completion(true, orderID)
                case .failure(let error):
                    responseErrorPrint("Failed to process checkout request", error, response.data)
                    completion(false, "")
                }
        }
    }
    
    private func generateOrderParamaters(order: OrderPacket) -> RouterParameters {
        
        var parameters: [String: Any] = ["payment_method": "CreditCard",
                                         "quantity": [1],
                                         "item_id": [order.productID],
                                         "phone": order.phone,
                                         "phone_country": order.phoneCountry,
                                         "credit_card_type": order.cardType.rawValue,
                                         "credit_card_number": order.creditCardString,
                                         "expiration_month": order.expirationMonth,
                                         "expiration_year": order.expirationYear,
                                         "cvv": order.cvv]
        
        if let shipping = order.shipping {
            parameters.updateValue(shipping.firstName, forKey: "first_name")
            parameters.updateValue(shipping.lastName, forKey: "last_name")
            parameters.updateValue(shipping.country, forKey: "country")
            parameters.updateValue(shipping.state, forKey: "state")
            parameters.updateValue(shipping.city, forKey: "city")
            parameters.updateValue(shipping.street, forKey: "address")
            parameters.updateValue(shipping.zipCode, forKey: "zip_code")
            
            parameters.updateValue(order.billing.firstName, forKey: "billing_first_name")
            parameters.updateValue(order.billing.lastName, forKey: "billing_last_name")
            parameters.updateValue(order.billing.country, forKey: "billing_country")
            parameters.updateValue(order.billing.state, forKey: "billing_state")
            parameters.updateValue(order.billing.city, forKey: "billing_city")
            parameters.updateValue(order.billing.street, forKey: "billing_address")
            parameters.updateValue(order.billing.zipCode, forKey: "billing_zip_code")
            
        } else {
            parameters.updateValue(order.billing.firstName, forKey: "first_name")
            parameters.updateValue(order.billing.lastName, forKey: "last_name")
            parameters.updateValue(order.billing.country, forKey: "country")
            parameters.updateValue(order.billing.state, forKey: "state")
            parameters.updateValue(order.billing.city, forKey: "city")
            parameters.updateValue(order.billing.street, forKey: "address")
            parameters.updateValue(order.billing.zipCode, forKey: "zip_code")
        }
        
        return parameters
    }
    
    private func extractOrderID(successDict: [String: AnyObject]) -> String? {
        guard let results = successDict["result"] as? [String: AnyObject] else { return nil }
        guard let tranaction = results["transaccionResponse"] as? [String: AnyObject] else { return nil }
        return tranaction["TransactionID"] as? String
    }
}
