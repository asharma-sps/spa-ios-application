//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import AVKit
import AVFoundation

private let kPlaybackControlBarHeight = CGFloat(45.0)

protocol VideoPlayerViewControllerDelegate: class {
    func presentFullscreenViewController(player: AVPlayer)
}

// MARK:- Declaration and Setup
class VideoPlayerVC: UIViewController {
    
    fileprivate let playerViewController = AVPlayerViewController() // Using this instead of AVPlayerLayer b/c this has a built in loading spinner
    fileprivate let tapDetectingLayerView = UIView()
    fileprivate let playbackControlsView = PlayerControlsView()
    fileprivate let failureWarningView = PlayerFailureWarningView()
    fileprivate var fadeoutTimer: Timer?
    fileprivate let mutedWarningLabel = UILabel()
    fileprivate var mutedWarningLabelTimer: Timer?
    fileprivate var player: AVPlayer?
    
    weak var delegate: VideoPlayerViewControllerDelegate?
    
    init(productID: Int?) {
        failureWarningView.productID = productID ?? -1
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPlayerViewController()
        setupTapDetectingLayerView()
        setupPlayerControlsView()
        setupMutedWarningLabel()
        setupFailureWaringView()
    }

    private func setupPlayerViewController() {
        playerViewController.showsPlaybackControls = false
        addChildViewController(playerViewController)
        view.addSubview(playerViewController.view)
        playerViewController.didMove(toParentViewController: self)
    }
    
    private func setupTapDetectingLayerView() {
        tapDetectingLayerView.backgroundColor = UIColor.clear
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewWasTapped))
        tapDetectingLayerView.addGestureRecognizer(tapRecognizer)
        view.addSubview(tapDetectingLayerView)
    }
    
    private func setupPlayerControlsView() {
        playbackControlsView.delegate = self
        view.addSubview(playbackControlsView)
    }
    
    private func setupMutedWarningLabel() {
        mutedWarningLabel.text = "No audio? Please ensure that your silent switch is off."
        mutedWarningLabel.textColor = UIColor.white
        mutedWarningLabel.textAlignment = .center
        mutedWarningLabel.backgroundColor = UIColor(white: 0.00, alpha: 0.8)
        mutedWarningLabel.layer.masksToBounds = true
        mutedWarningLabel.layer.cornerRadius = 5.0
        mutedWarningLabel.numberOfLines = 2
        mutedWarningLabel.alpha = 0.0
        view.addSubview(mutedWarningLabel)
    }
    
    private func setupFailureWaringView() {
        failureWarningView.isHidden = true
        view.addSubview(failureWarningView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        becomeVolumeChangeObserver()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        playerViewController.player?.pause()
    }
    
    deinit {
        playbackControlsView.clearPlayer()
        guard let unwrapped = player else { return }
        unwrapped.removeObserver(self, forKeyPath: "currentItem.status")
    }
}

// MARK:- Volume Change Observing
extension VideoPlayerVC: VolumeChangeObserver {
    
    fileprivate func becomeVolumeChangeObserver() {
        AudioHelper.setVolumeChangeObserver(observer: self)
    }
    
    func userDidChangeVolume() {
        showMutedAudioWarningForThreeSeconds()
    }
    
    private func showMutedAudioWarningForThreeSeconds() {
        resetTimer()
        mutedWarningLabelTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(dismissShowMutedAudioWarning), userInfo: nil, repeats: false)
        UIView.animate(withDuration: 0.3) { 
            self.mutedWarningLabel.alpha = 1.0
        }
    }
    
    @objc private func dismissShowMutedAudioWarning() {
        UIView.animate(withDuration: 0.3) {
            self.mutedWarningLabel.alpha = 0.0
        }
        resetTimer()
    }
    
    private func resetTimer() {
        mutedWarningLabelTimer?.invalidate()
        mutedWarningLabelTimer = nil
    }
}

// MARK:- Player Management / Status Tracking
extension VideoPlayerVC {
    
    func setPlayer(player: AVPlayer?) {
        if let oldPlayer = self.player {
            oldPlayer.removeObserver(self, forKeyPath: "currentItem.status")
        }
        if let newPlayer = player {
            newPlayer.addObserver(self, forKeyPath: "currentItem.status", options: [], context: nil)
            self.player = newPlayer
            playbackControlsView.setPlayer(player: newPlayer)
            playerViewController.player = newPlayer
            newPlayer.play()
        } else {
            self.player = nil
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let currentItem = player?.currentItem else { return }
        failureWarningView.isHidden = (currentItem.status != .failed)
    }
}

// MARK:- PlayerControlsViewDelegate
extension VideoPlayerVC: PlayerControlsViewDelegate {
    
    internal func fullscreenPressed(player: AVPlayer) {
        delegate?.presentFullscreenViewController(player: player)
    }
    
    internal func playPressed() {
        hidePlayerControlIfPlayingAfterDelay()
    }
}

// MARK:- Control Bar Fading
extension VideoPlayerVC {
    
    @objc fileprivate func viewWasTapped() {
        let barIsVisible = (playbackControlsView.alpha == 1.0)
        setPlaybackControlViewVisibility(visible: !barIsVisible)
    }
    
    fileprivate func setPlaybackControlViewVisibility(visible: Bool) {
        let newAlpha = CGFloat(visible ? 1.0 : 0.0)
        UIView.animate(withDuration: 0.5) {
            self.playbackControlsView.alpha = newAlpha
        }
        if visible {
            hidePlayerControlIfPlayingAfterDelay()
        }
    }
    
    fileprivate func hidePlayerControlIfPlayingAfterDelay() {
        fadeoutTimer?.invalidate()
        fadeoutTimer = nil
        fadeoutTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(hidePlayerControlIfPlaying), userInfo: nil, repeats: false)
    }
    
    @objc fileprivate func hidePlayerControlIfPlaying() {
        fadeoutTimer?.invalidate()
        fadeoutTimer = nil
        if player?.rate == 1.0 {
            setPlaybackControlViewVisibility(visible: false)
        }
    }
}

// MARK:- Layout
extension VideoPlayerVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let playerFrame = CGRect(0.0, 0.0, view.frame.width, view.frame.height)
        playerViewController.view.frame = playerFrame
        tapDetectingLayerView.frame = playerFrame
        failureWarningView.frame = playerFrame
        playbackControlsView.frame = CGRect(0.0, view.frame.height - kPlaybackControlBarHeight, view.frame.width, kPlaybackControlBarHeight)
        
        let mutedLabelSidePadding = CGFloat(20)
        let mutedLabelWidth = view.frame.width - mutedLabelSidePadding * 2
        let mutedLabelHeight = mutedWarningLabel.sizeThatFits(CGSize(mutedLabelWidth, CGFloat.greatestFiniteMagnitude)).height + 20
        mutedWarningLabel.frame = CGRect(mutedLabelSidePadding,
                                         playbackControlsView.frame.origin.y - 10 - mutedLabelHeight,
                                         mutedLabelWidth,
                                         mutedLabelHeight)
    }
    
    class func requiredViewHeight(width: CGFloat) -> CGFloat {
        return width * 0.562 // 16:9
    }
}
