//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//


import Foundation

struct ExerciseBlock {
    let id: String
    let postion: Int
    let sets: Int
    let exercises: [Exercise]
}

struct Exercise {
    let id: String
    let name: String
    let weight: Int
    let restTime: Int
    let reps: Int
    let type: ExerciseType
    let video: VideoResource
    let description: String
    
    var typeDescription: String {
        switch type {
        case .repetition:
            return "\(reps) Reps"
        case .failure:
            return "Repeat Until Failure"
        case .seconds:
            return "Perform For \(reps) Seconds"
        }
    }
}

enum ExerciseType: String {
    case repetition = "reps"
    case seconds = "seconds"
    case failure = "failure"
}
