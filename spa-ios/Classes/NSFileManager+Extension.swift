//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

extension FileManager {
        
    func roughSizeOfDirectory(filePathURL : URL) -> UInt64? {
        
        var totalSize = UInt64(0)
        let prefetchKeys = [URLResourceKey.isRegularFileKey, URLResourceKey.fileAllocatedSizeKey, URLResourceKey.totalFileAllocatedSizeKey ]
        
        let errorHandler: (URL, Error) -> Bool = { url, error in
            errorPrint("An error occurred while trying to enumerate file at url: \(url)", error)
            return true
        }
        
        guard let enumerator = self.enumerator(at: filePathURL, includingPropertiesForKeys: prefetchKeys, options: [], errorHandler: errorHandler) else { return nil }
        
        for item in enumerator {
            let contentItemURL = item as! NSURL
            
            let resourceValueForKey: (String) throws -> NSNumber? = { key in
                var value: AnyObject?
                try contentItemURL.getResourceValue(&value, forKey: URLResourceKey(rawValue: key))
                return value as? NSNumber
            }
            
            guard let isRegularFileOptional = try? resourceValueForKey(URLResourceKey.isRegularFileKey.rawValue), let isRegularFile = isRegularFileOptional, isRegularFile.boolValue else {
                print("Encountered a non-regular (or at least we can't confirm it is regular). Continuing w/o summing....")
                continue
            }

            guard let fileSize = try? resourceValueForKey(URLResourceKey.totalFileAllocatedSizeKey.rawValue) ?? resourceValueForKey(URLResourceKey.fileAllocatedSizeKey.rawValue) else {
                print("Could not determing \"totalFileAllocatedSizeKey\" file size or \"fileAllocatedSizeKey\". Continuing w/o summing...")
                continue
            }
            
            totalSize += fileSize?.uint64Value ?? UInt64(0)
        }
        
        return totalSize
    }
}
