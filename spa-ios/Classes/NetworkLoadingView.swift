//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class BaseLoadingView: UIView {
    
    let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    
    init() {
        super.init(frame: CGRect.zero)
        spinner.color = UIColor.brandDarkGray
        self.addSubview(spinner)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        spinner.frame = CGRect(0.0, 0.0, frame.width, frame.height)
    }
}
