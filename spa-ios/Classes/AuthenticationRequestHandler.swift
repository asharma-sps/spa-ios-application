//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import Alamofire

enum AuthenticationResult<T> { // ASTODO doesn't need to be an enum
    case success(T)
    case failure(String)
}

enum RequestResult {
    case success
    case failure(String)
}

enum Vendor: String {
    case google = "google"
    case facebook = "facebook"
    var authMethod: AuthenticationMethod {
        switch self {
        case .google:
            return .google
        case .facebook:
            return .facebook
        }
    }
}

// Authentication
struct AuthenticationRequestHandler {
    
    static func login(email: String, password: String, completion: @escaping (AuthenticationResult<User>) -> Void) {
        let parameters: RouterParameters = ["email" : email, "password" : password, "os" : "ios", "device" : UIDevice.current.model]
        let request = PublicRouter(.login(parameters: parameters))
        Alamofire.request(request)
            .validate()
            .responseJSON { response in
                self.processRequestResult(response: response, method: .native, completion: completion) { user in
                    Analytics.tagUserSignedIn(user: user)
                }
        }
    }
    
    static func registerUser(first: String, last: String, password: String, email: String, phoneCode: String, phoneNumber: String, completion: @escaping (AuthenticationResult<User>) -> Void) {
        var parameters = ["first_name": first, "last_name": last, "password": password, "email": email, "phone_country": "+\(phoneCode)", "phone": phoneNumber]
        if let source = SourceTracker.lastKnownSource {
            parameters["source"] = source
        }
        let request = PublicRouter(.register(parameters: parameters))
        Alamofire.request(request)
            .validate()
            .responseJSON { response in
                self.processRequestResult(response: response, method: .native, completion: completion) { user in
                    Analytics.tagUserRegistered(user: user)
                }
        }
    }
    
    static func loginSocial(token: String, vendor: Vendor, first: String, last: String, email: String, completion: @escaping (AuthenticationResult<User>) -> Void) {
        var parameters = ["vendor_id": token, "vendor": vendor.rawValue, "first_name": first, "last_name": last, "email": email]
        if let source = SourceTracker.lastKnownSource {
            parameters["source"] = source
        }
        let request = PublicRouter(.login(parameters: parameters))
        Alamofire.request(request)
            .validate()
            .responseJSON { response in
                self.processRequestResult(response: response, method: vendor.authMethod, completion: completion) { user in
                    print("IMPLEMENT TRACKING -- SOCIAL SIGN IN! \(user)") // ASTODO
                }
        }
    }
}

// MARK:- Response Processing
extension AuthenticationRequestHandler {
    
    fileprivate static func processRequestResult(response: DataResponse<Any>, method: AuthenticationMethod, completion: @escaping (AuthenticationResult<User>) -> Void, analyticsCompletion: (User) -> Void) {
        switch response.result {
        case .success(let value):
            guard let checked = value as? [String: AnyObject], let user = self.deserializeUser(data: checked, auth: method) else {
                completion(.failure(ErrorMessages.inconsistancy))
                return
            }
            analyticsCompletion(user)
            completion(.success(user))
        case .failure(let error):
            if error.isConnectionIssue() {
                completion(.failure(ErrorMessages.connection))
            }
            let errorContent = self.extractAuthenticationErrorMessages(data: response.data)
            let message = errorContent.message ?? ErrorMessages.general
            ErrorReporter.report(error: error, message: "LOGIN: message: \(errorContent.message ?? "no_message"), exception: \(errorContent.exception ?? "no_exception"))")
            completion(.failure(message))
        }
    }

    fileprivate static func deserializeUser(data: [String: AnyObject], auth: AuthenticationMethod) -> User? {
        guard let email = data["email"] as? String else { return nil }
        guard let token = data["remember_token"] as? String else { return nil }
        guard let userID = data["id"] as? Int else { return nil }
        let first = data["first_name"] as? String ?? ""
        let last = data ["last_name"] as? String ?? ""
        return User(firstName: first, lastName: last, email: email, token: token, id: String(userID), authenticationMethod: auth)
    }
}

// MARK:- Non-Authenticating
extension AuthenticationRequestHandler {
    
    static func logout(user: User, completion: @escaping (RequestResult) -> Void) {
        let parameters = ["email": user.email, "token": user.token]
        let request = PublicRouter(.logout(parameters: parameters))
        Alamofire.request(request)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    Analytics.tagUserSignedOut()
                    completion(.success)
                case .failure(let error):
                    let message = error.isConnectionIssue() ? ErrorMessages.connection : ErrorMessages.inconsistancy
                    errorPrint("Failed to logout the user", error)
                    completion(.failure(message))
                }
        }
    }
    
    static func sendPasswordResetEmail(email: String, completion: @escaping (String) -> Void) {
        let request = PublicRouter(.resetPassword(resetEmail: email))
        Alamofire.request(request)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let uncheckedDict):
                    guard let dict = uncheckedDict as? [String: AnyObject], let message = dict["message"] as? String else {
                        completion(ErrorMessages.inconsistancy)
                        return
                    }
                    completion(message)
                case .failure(let error):
                    responseErrorPrint("Failed to reset password", error, response.data)
                    completion(ErrorMessages.connection)
                }
        }
    }
}

// MARK:- Error Message Extraction
extension AuthenticationRequestHandler {
    
    // I'm getting a 400 something response for authentication-related response for simple errors, such as "email already used."
    // This helper method extracts the error message from data, which isn't being deserialized by Alamofire, b/c it thinks it's an error.
    // I was going to just skip validating the response, but I've decided this is the best solution.
    fileprivate static func extractAuthenticationErrorMessages(data optionalData: Data?) -> (message: String?, exception: String?) {
        guard let data = optionalData else { return (message: nil, exception: nil) }
        guard let optionalJSON = try? JSONSerialization.jsonObject(with: data, options: []) else { return (message: nil, exception: nil) }
        guard let JSON = optionalJSON as? [String: AnyObject] else { return (message: nil, exception: nil) }
        let message = JSON["message"] as? String
        let exception = JSON["exception"] as? String // for debugging
        return (message: message, exception: exception)
    }
}
