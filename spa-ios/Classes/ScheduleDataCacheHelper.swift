//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

private let kDirectoryName = "schedule_cache"
private let kProgramsDataFileName = "schedule_programs_data"
private let kEntriesDataFileName = "schedule_entries_data"

struct ScheduleDataCacheHelper {
    
    private static let cacheDirectory = URL.documentDirectoryPath().appendingPathComponent(kDirectoryName)
    private static let programsDataFilePath = URL(fileURLWithPath: cacheDirectory.appendingPathComponent(kProgramsDataFileName).path)
    private static let entriesDataFilePath = URL(fileURLWithPath: cacheDirectory.appendingPathComponent(kEntriesDataFileName).path)

    static func set(programsData: Data, entriesData: Data) {
        guard PersistanceHelper.createDirectoryIfNonExistant(path: cacheDirectory) else { return }
        PersistanceHelper.write(data: programsData, path: programsDataFilePath)
        PersistanceHelper.write(data: entriesData, path: entriesDataFilePath)
    }
    
    static func load() -> (programData: Data, entriesData: Data)? {
        guard let programData = FileManager.default.contents(atPath: programsDataFilePath.path) else { return nil }
        guard let entryData = FileManager.default.contents(atPath: entriesDataFilePath.path) else { return nil }
        return (programData: programData, entriesData: entryData)
    }
    
    static func clear() {
        PersistanceHelper.delete(path: cacheDirectory)
    }
}
