//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

private let StoryboardName = "MessageVC"

private enum Topic: String {
    case training = "Training"
    case support = "Support"
}

class MessageVC: UIViewController {
    
    // MARK:- IBOutlets
    @IBOutlet weak var selectedTopicLabel: UILabel!
    @IBOutlet weak var topicSelectionButton: UIButton!
    @IBOutlet weak var textField: UITextView!
    @IBOutlet weak var textFieldBottomConstraint: NSLayoutConstraint!
    
    // MARK:- Properties
    fileprivate var topic = Topic.training
    fileprivate let requestHandler = SettingsRequestHandler()
    var session: Session?
    
    // MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarButtons()
        startObservingForKeyboard()
        setSelectedTopic(topic: .training)
        setupTextField()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        textField.resignFirstResponder()
    }
    
    func setupTextField() {
        textField.text = "\n\n\n\n\nApp Version: \(Environment.version) Build: \(Environment.build)"
    }
    
    // MARK:- Bar Buttons
    private func setupNavigationBarButtons() {
        let cancel = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissSelf))
        let send = UIBarButtonItem(title: "Send", style: .done, target: self, action: #selector(sendPressed))
        self.navigationItem.leftBarButtonItem = cancel
        self.navigationItem.rightBarButtonItem = send
    }
    
    @objc private func sendPressed() {
        guard let user = session?.user else {
            ASGlobalOverlay.showAlert(withTitle: "Error", message: ErrorMessages.inconsistancy, dismissButtonTitle: "OK")
            return
        }
        send(message: textField.text ?? "", user: user)
    }
    
    // MARK:- Topic Selection
    private func promptUserToSelectTopic() {
        ASGlobalOverlay.manageResponderStateDuringNextPopover(textField)
        let train = ASUserOption(title: "Training") { self.setSelectedTopic(topic: .training) }
        let support = ASUserOption(title: "Support") { self.setSelectedTopic(topic: .support) }
        let cancel = ASUserOption.cancel(withTitle: "Cancel", actionBlock: nil)
        ASGlobalOverlay.showSlideUpMenu(withPrompt: "Select a topic", userOptions: [train as Any, support as Any, cancel as Any])
    }
    
    @IBAction func topicSelectionButtonPressed(_ sender: AnyObject) {
        promptUserToSelectTopic()
    }
    
    private func setSelectedTopic(topic: Topic) {
        self.topic = topic
        selectedTopicLabel.text = topic.rawValue
    }
    
    // MARK:- Load From Storyboard
    class func generateMessageViewController() -> MessageVC{
        let storyboard = UIStoryboard(name: StoryboardName, bundle: nil)
        let messageVC = storyboard.instantiateInitialViewController() as? MessageVC!
        return messageVC!
    }
}

// MARK:- View Model
extension MessageVC {
    
    fileprivate func send(message: String, user: User) {
        textField.resignFirstResponder()
        ASGlobalOverlay.showWorkingIndicator(withDescription: nil)
        requestHandler.postSupportInquiry(user: user, message: textField.text, subject: topic.rawValue) { success in
            if success {
                ASGlobalOverlay.showAlert(withTitle: "Message Sent!", message: "A support agent will reply your message shortly.", dismissButtonTitle: "OK")
                self.dismissSelf()
            } else {
                ASGlobalOverlay.showAlert(withTitle: "Error", message: "An error occurred! Do five pushups then try again, or call support.", dismissButtonTitle: "OK")
            }
        }
    }
}

// MARK:- Keyboard Management
extension MessageVC: UITextViewDelegate {
    
    fileprivate func startObservingForKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillAppear(notificaiton: NSNotification) {
        let userInfo:NSDictionary = notificaiton.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        UIView.animate(withDuration: 0.5) {
            self.textFieldBottomConstraint.constant = keyboardFrame.cgRectValue.height + 20.0
        }
    }
    
    func keyboardWillHide() {
        UIView.animate(withDuration: 0.5) {
            self.textFieldBottomConstraint.constant = 20
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
