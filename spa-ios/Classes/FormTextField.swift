//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

protocol FormTextFieldDelegate: class {
    func nextButtonPressed(sender: FormTextField)
}

class FormTextField: UITextField {
    
    enum AccessoryTitle: String {
        case next = "Next"
        case done = "Done"
        case save = "Save"
        case create = "Create Account"
    }
    
    weak var nextButtonDelegate: FormTextFieldDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        font = UIFont.brandRegular(size: 14.0)
        inputAssistantItem.leadingBarButtonGroups = []
        inputAssistantItem.trailingBarButtonGroups = []
        inputAccessoryView = UIButton.generateAccessoryInputButton(title: "--", target: self, selector: #selector(nextButtonPressed))
        returnKeyType = .done
        setAccessoryTitle(accessoryTitle: .next)
        delegate = self
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(bounds.origin.x + 18, bounds.origin.y, bounds.size.width - 36, bounds.size.height);
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds);
    }
    
    @objc private func nextButtonPressed() {
        nextButtonDelegate?.nextButtonPressed(sender: self)
    }
}

extension FormTextField {

    func setAccessoryTitle(accessoryTitle: AccessoryTitle) {
        guard let accessoryInputButton = inputAccessoryView as? UIButton else { return }
        accessoryInputButton.setTitle(accessoryTitle.rawValue, for: .normal)
    }
    
    func validatedString() -> String? {
        guard let value = text, !value.isEmpty && value != " " else { return nil }
        return value
    }
}

extension FormTextField: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        resignFirstResponder()
        return true
    }
}
