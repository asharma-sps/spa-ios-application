//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class ProductsCollectionViewCell: UICollectionViewCell {
    
    static let reuseID = "product_collection_view_cell"
    private let productImageView = NetworkImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureProductImageView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configureProductImageView()
    }
    
    func configureProductImageView(){
        self.addSubview(self.productImageView)
        productImageView.contentMode = .scaleAspectFill
    }
    
    func setupWithProduct(product: Product, rounded: Bool) {
        layer.masksToBounds = rounded
        layer.cornerRadius = rounded ? 5.0 : 0.0
        layer.borderWidth = rounded ? Environment.thinnestLine : 0.0
        layer.borderColor = UIColor.brandUltraLightGray.cgColor
        guard let url = URL(string: product.imageURL) else {
            productImageView.clearImage()
            return
        }
        productImageView.setImageFromURL(imageURL: url)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        productImageView.frame = CGRect(0.0, 0.0, frame.width, frame.height)
    }
}
