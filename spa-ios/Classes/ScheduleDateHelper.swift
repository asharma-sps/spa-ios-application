//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

/*
 *
 * ABOUT THIS CLASS:
 * This class centralizes scheduling-related date calculations to ensure consistancy and performace throughout the scheduling system.
 * A singleton to is used to host a cached instances of the date formatters.
 *
 */

class ScheduleDateHelper {
    
    static let singleton = ScheduleDateHelper()
    
    var calendar: Calendar
    fileprivate let dateFormatter: DateFormatter
    fileprivate let dayOfMonthFormatter: DateFormatter
    fileprivate let dayOfWeekFormatter: DateFormatter
    fileprivate let userDateOfWeekFormatter: DateFormatter

    // MARK:- Setup
    fileprivate init() {
        calendar = Calendar.current
        calendar.firstWeekday = 1
        calendar.minimumDaysInFirstWeek = 7
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        dayOfMonthFormatter = DateFormatter()
        dayOfMonthFormatter.dateFormat = "d"
        
        dayOfWeekFormatter = DateFormatter()
        dayOfWeekFormatter.dateFormat = "E"
        
        userDateOfWeekFormatter = DateFormatter()
        userDateOfWeekFormatter.dateFormat = "MMMM d, yyyy"
    }
    
    // MARK:- Week Range Calculations
    class func determineStartDatesForWeeksInRange(dateInStartWeek: Date, dateInEndWeek: Date) -> [Date] {
        guard let normalizedFirstWeekStartDate = ScheduleDateHelper.firstDayOfWeekWithDate(date: dateInStartWeek) else { return [] }
        guard let normalizedLastWeekStartDate = ScheduleDateHelper.firstDayOfWeekWithDate(date: dateInEndWeek) else { return [] }
        var dates = [normalizedFirstWeekStartDate]
        var currentDate = normalizedFirstWeekStartDate
        
        repeat {
            currentDate = singleton.calendar.date(byAdding: DateComponents(day: 7), to: currentDate)!
            dates.append(currentDate)
            
        } while !singleton.calendar.isDate(currentDate, inSameDayAs: normalizedLastWeekStartDate) && currentDate.compare(normalizedLastWeekStartDate) == .orderedAscending
        
        return dates
    }
    
    
    class func firstDayOfWeekWithDate(date: Date) -> Date? {
        var comps = ScheduleDateHelper.singleton.calendar.dateComponents([.weekOfYear, .yearForWeekOfYear], from: date)
        comps.weekday = 1
        let mondayInWeek = ScheduleDateHelper.singleton.calendar.date(from: comps)!
        return mondayInWeek
    }
    
    // MARK:- Generate Date Strings
    class func generateDateStringsForWeekStartingOnDate(startDate: Date) -> [String] {
        // generate start dates
        var dates = [startDate]
        var currentDate = startDate
        for _ in 0..<7 {
            currentDate = singleton.calendar.date(byAdding: DateComponents(day: 1), to: currentDate)!
            dates.append(currentDate)
        }
        // generate date strings
        var dateStrings = [String]()
        for date in dates {
            dateStrings.append(singleton.dateFormatter.string(from: date))
        }
        return dateStrings
    }
    
    // MARK:- Decompose Date Components
    class func componentsOfDateString(dateString: String) -> (dayOfWeek: String, day: String)? {
        guard let date = singleton.dateFormatter.date(from: dateString) else {
            print("ERROR: Schedule receieved a malformed date string: \(dateString)")
            return nil
        }
        let dayOfWeek = singleton.dayOfWeekFormatter.string(from: date).uppercased()
        let day = singleton.dayOfMonthFormatter.string(from: date)
        return (dayOfWeek: dayOfWeek, day: day)
    }
    
    // MARK:- User Date String
    class func userDateString(date: Date) -> String {
        let dateString = singleton.userDateOfWeekFormatter.string(from: date)
        guard let comma = dateString.range(of: ",") else { return "" }
        return dateString.substring(to: comma.lowerBound) + daySuffix(date: date) + dateString.substring(from: comma.upperBound)
    }
    
    fileprivate class func daySuffix(date: Date) -> String {
        let calendar = NSCalendar.current
        let dayOfMonth = calendar.component(.day, from: date)
        switch dayOfMonth {
        case 1, 21, 31: return "st"
        case 2, 22: return "nd"
        case 3, 23: return "rd"
        default: return "th"
        }
    }
}
