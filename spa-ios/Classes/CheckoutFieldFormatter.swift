//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class PaymentFieldFormatter {
    
    private let nonNumbers = NSCharacterSet.decimalDigits.inverted
    
    // MARK:- Public Formatter
    func formatStringAsCreditCardNumber(string: String) -> String? {
        guard let sanitizedString = sanitizeCreditCardStringIfPossible(string: string) else { return nil }
        if sanitizedString.characters.count == 0 { return nil }
        guard let firstCharacter = sanitizedString.characters.first else { return nil }
        let firstCharacterString = String(firstCharacter)
        
        if firstCharacterString == "4" || firstCharacterString == "5" || firstCharacterString == "6" {
            return formatStringAsStandardCreditCard(text: sanitizedString)
        }
        else if firstCharacterString == "3" {
            return formatStringAsAmexCreditCard(text: sanitizedString)
        }
        return nil
    }
    
    // MARK:- Internal Formatters
    private func formatStringAsStandardCreditCard(text: String) -> String? {
        let count = text.characters.count
        let space = " "
        if count > 16 { return nil }
        if count > 12 { return text.sub(0, 4) + space + text.sub(4, 8) + space + text.sub(8, 12) + space + text.subToEnd(from: 12) }
        if count > 8 { return text.sub(0, 4) + space + text.sub(4, 8) + space + text.subToEnd(from: 8) }
        if count > 4 { return text.sub(0, 4) + space + text.subToEnd(from: 4) }
        return text
    }
    
    private func formatStringAsAmexCreditCard(text: String) -> String? {
        let count = text.characters.count
        let space = " "
        if count > 15 { return nil }
        if count > 10 { return text.sub(0, 4) + space + text.sub(4, 10) + space + text.subToEnd(from: 10) }
        if count > 4 { return text.sub(0, 4) + space + text.subToEnd(from: 4) }
        return text
    }
    
    // MARK:- Sanitization
    fileprivate func sanitizeCreditCardStringIfPossible(string: String) -> String? {
        let sanitizedString = string.replacingOccurrences(of: " ", with: "")
        if sanitizedString.rangeOfCharacter(from: nonNumbers) != nil { return nil }
        if sanitizedString.characters.count > 16 { return nil }
        return sanitizedString
    }
    
    // MARK:- Soft Validation
    func stringIsPossiblyACreditCard(string: String) -> Bool {
        if sanitizeCreditCardStringIfPossible(string: string) == nil { return false }
        return true
    }
}

// MARK:- Substring Helpers
private extension String {
    
    func sub(_ start: Int, _ end: Int) -> String {
        let rangeStartIndex = index(startIndex, offsetBy: start)
        let rangeEndIndex = index(startIndex, offsetBy: end)
        return substring(with: rangeStartIndex..<rangeEndIndex)
    }
    
    func subToEnd(from: Int) -> String {
        let rangeEndIndex = index(startIndex, offsetBy: from)
        return substring(with: rangeEndIndex..<endIndex)
    }
}

class PaymentCardTextField: FormTextField {
    
    private let formatter = PaymentFieldFormatter()
    var sanitizedPaymentCardValue: String? {
            return formatter.sanitizeCreditCardStringIfPossible(string: text ?? "")
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(paste) {
            guard let clipboardText = UIPasteboard.general.string else { return false }
            return formatter.stringIsPossiblyACreditCard(string: clipboardText)
        }
        return false
    }
}
