//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

private let kStoryboardName = "CheckoutTVC"

// MARK:- Outlets & Properties
class CheckoutTVC: AnalyticBaseTableViewController, CheckoutShippingAddressTableViewControllerDelegate, CheckoutCountryCodePickerViewDelegate, CheckoutCountryPickerViewDelegate, CheckoutExpirationDatePickerViewDelegate {
    
    // MARK:- Field IBOutlets
    @IBOutlet weak var firstNameField: FormTextField!
    @IBOutlet weak var lastNameField: FormTextField!
    @IBOutlet weak var cardNumberField: PaymentCardTextField!
    @IBOutlet weak var expirationDateField: FormPickerTextField!
    @IBOutlet weak var cvvField: FormTextField!
    @IBOutlet weak var countryCodeField: FormPickerTextField!
    @IBOutlet weak var phoneNumberField: FormTextField!
    @IBOutlet weak var billingStreetAddressField: FormTextField!
    @IBOutlet weak var billingCityField: FormTextField!
    @IBOutlet weak var billingStateField: FormTextField!
    @IBOutlet weak var billingCountryField: FormPickerTextField!
    @IBOutlet weak var billingZipCodeField: FormTextField!
    
    // MARK:- Shipping IBOutlets
    @IBOutlet weak var shippingAddressCell: UITableViewCell!
    
    // MARK:- Order Confirmation IBOutlets
    @IBOutlet weak var productTypeLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var completeOrderButton: UIButton!
    
    // MARK:- Properties
    var session: Session?
    var program: Product?
    let requestHandler = CheckoutRequestHandler()
    var orderedFields = [FormTextField]()
    let paymentFieldFormatter = PaymentFieldFormatter()
    
    // MARK:- CheckoutShippingAddressTableViewControllerDelegate
    var shippingAddress: Address? {
        didSet {
            shippingAddressCell.detailTextLabel?.text = shippingAddress?.description ?? "Add a different shipping address (optional)"
        }
    }
    
    // MARK:- CheckoutCountryCodePickerViewDelegate
    var phoneCountryCode: String? {
        didSet {
            if let unwrapped = phoneCountryCode {
                countryCodeField.text = "+\(unwrapped)"
            } else {
                countryCodeField.text = nil
            }
        }
    }
    
    // MARK:- CheckoutCountryPickerViewDelegate
    var country: String? {
        didSet {
            billingCountryField.text = country
        }
    }
    
    // MARK:- CheckoutExpirationDatePickerViewDelegate
    var expirationDate: (month: String, year: String)? {
        didSet {
            if let unwrapped = expirationDate {
                expirationDateField.text = "\(unwrapped.month) / \(unwrapped.year)"
            } else {
                expirationDateField.text = nil
            }
        }
    }
    
    @objc func cardNumberTextFieldValueDidChange(_ sender: AnyObject) {
        guard let formattedPaymentCardString = paymentFieldFormatter.formatStringAsCreditCardNumber(string: cardNumberField.text ?? "") else { return }
        cardNumberField.text = formattedPaymentCardString
    }
}
// MARK:- Setup
extension CheckoutTVC {
    
    class func generateCheckoutTableViewController(product: Product, session: Session) -> CheckoutTVC {
        let storyboard = UIStoryboard(name: kStoryboardName, bundle: nil)
        let tvc = storyboard.instantiateInitialViewController() as? CheckoutTVC
        tvc?.session = session
        tvc?.program = product
        return tvc!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupOrderedFields()
        setupTextFieldInputViews()
        loadDefaultTextFieldValues()
    }
    
    private func configureView() {
        setupBlankBackButton()
        completeOrderButton.configure()
        configureProductInformationLabels(program: program)
    }
    
    private func setupOrderedFields() {
        orderedFields = [firstNameField, lastNameField, cardNumberField, expirationDateField, cvvField, countryCodeField, phoneNumberField, billingStreetAddressField, billingCityField, billingStateField, billingCountryField, billingZipCodeField]
        for field in orderedFields {
            field.nextButtonDelegate = self
        }
        if let lastField = orderedFields.last {
            lastField.setAccessoryTitle(accessoryTitle: .done)
        }
    }
    
    private func configureProductInformationLabels(program: Product?) {
        guard let unwrapped = program else { return }
        productNameLabel.text = unwrapped.name
        productTypeLabel.text = unwrapped.type.rawValue.uppercased()
        priceLabel.text = unwrapped.totalCostDescription
    }
    
    private func setupTextFieldInputViews() {
        expirationDateField.inputView = CheckoutExpirationDatePV(delegate: self)
        countryCodeField.inputView = CheckoutCountryCodePV(delegate: self)
        billingCountryField.inputView = CheckoutCountryPV(delegate: self)
        cardNumberField.addTarget(self, action: #selector(cardNumberTextFieldValueDidChange), for: .editingChanged)
    }
    
    private func loadDefaultTextFieldValues() {
        guard let defaults = CheckoutDefaultsHelper.load() else { return }
        phoneNumberField.text = defaults.phone
        phoneCountryCode = defaults.phoneCountry
        firstNameField.text = defaults.billing.firstName
        lastNameField.text = defaults.billing.lastName
        billingStreetAddressField.text = defaults.billing.street
        billingCityField.text = defaults.billing.city
        billingStateField.text = defaults.billing.state
        billingCountryField.text = defaults.billing.country
        billingZipCodeField.text = defaults.billing.zipCode
        shippingAddress = defaults.shipping
    }
}

// MARK:- Payment Processing
extension CheckoutTVC {
    
    @IBAction func completeOrderButtonPressed(_ sender: AnyObject) {
        processPayment()
    }
    
    private func processPayment() {
        
        guard let user = session?.user else {
            ASGlobalOverlay.showAlert(withTitle: "Error", message: ErrorMessages.inconsistancy, dismissButtonTitle: "OK")
            return
        }
        
        guard let order = generateOrderPacket() else {
            ASGlobalOverlay.showAlert(withTitle: "Error", message: "Please ensure that all fields are completed correctly.", dismissButtonTitle: "OK")
            return
        }
        
        ASGlobalOverlay.showWorkingIndicator(withDescription: "purchasing...")
        requestHandler.processCheckout(user: user, order: order) { (success, orderID) in
            print("starting stall...")
            let deadlineTime = DispatchTime.now() + .seconds(2) // TEMPORARY!!! There is a server side race-condition that will take time to resolve. ASTODO. DEPLOY.
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                print("contining operations...")
                if success {
                    if let rootProgramsVC = self.navigationController?.viewControllers.first as? ProductsVC {
                        rootProgramsVC.retryLoadingData()
                        _ = self.navigationController?.popToRootViewController(animated: true)
                    }
                    ASGlobalOverlay.showAlert(withTitle: "Order Completed", message: "An receipt will be emailed to you shortly.\n\n\(orderID)", dismissButtonTitle: "OK")
                } else {
                    ASGlobalOverlay.showAlert(withTitle: "Error", message: "We we're unable to process your order. Please check your card information and network settings then try again.", dismissButtonTitle: "OK")
                }
                guard let product = self.program else { return }
                self.tagPurchaseAttempt(success: success, product: product)
            }
        }
    }

    
    private func generateOrderPacket() -> OrderPacket? {
        
        // Ints
        guard let productID = program?.id else { return nil }
        
        // Strings
        guard let firstName = firstNameField.validatedString() else { return nil }
        guard let lastName = lastNameField.validatedString() else { return nil }
        guard let cardNumber = cardNumberField.sanitizedPaymentCardValue else { return nil }
        guard let expirationMonth = expirationDate?.month, !expirationMonth.isEmpty else { return nil }
        guard let expirationYear = expirationDate?.year, !expirationYear.isEmpty else { return nil }
        guard let cvv = cvvField.validatedString() else { return nil }
        guard let phoneNumber = phoneNumberField.validatedString() else { return nil }
        guard let phoneCountryCode = phoneCountryCode else { return nil }
        guard let billingStreet = billingStreetAddressField.validatedString() else { return nil }
        guard let billingCity = billingCityField.validatedString() else { return nil }
        guard let billingState = billingStateField.validatedString() else { return nil }
        guard let billingCountry = billingCountryField.validatedString() else { return nil }
        guard let billingZipCode = billingZipCodeField.validatedString() else { return nil }
        
        // Enums
        guard let firstCardNumber = cardNumber.characters.first else { return nil }
        guard let cardType = OrderPacket.CardType.cardTypeForNumber(firstDigit: String(firstCardNumber)) else { return nil } // ASTODO propagate error to users...
        
        let billingAddress = Address(firstName: firstName,
                                         lastName: lastName,
                                         street: billingStreet,
                                         city: billingCity,
                                         state: billingState,
                                         country: billingCountry,
                                         zipCode: billingZipCode)
        
        let orderPacket = OrderPacket(productID: productID,
                                      phone: phoneNumber,
                                      phoneCountry: phoneCountryCode,
                                      creditCardString: cardNumber,
                                      cardType: cardType,
                                      expirationMonth: expirationMonth,
                                      expirationYear: expirationYear,
                                      cvv: cvv,
                                      billing: billingAddress,
                                      shipping: shippingAddress)

        CheckoutDefaultsHelper.set(orderPacket: orderPacket)
        return orderPacket
    }
}

// MARK:- Other
extension CheckoutTVC: FormTextFieldDelegate {
    
    // MARK:- FormTextFieldDelegate
    func nextButtonPressed(sender: FormTextField) {
        guard let index = orderedFields.index(of: sender) else { return }
        if index > orderedFields.count { return } // index + 1 cancels out orderedFields.count -1
        if index + 1 == orderedFields.count { sender.resignFirstResponder() }
        else { orderedFields[index + 1].becomeFirstResponder() }
    }
    
    // Keyboard dismiss on scroll
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        for textField in orderedFields {
            textField.resignFirstResponder()
        }
    }
    
    // Shipping address TVC delegate
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let CheckoutShippingAddressTVC = segue.destination as? CheckoutShippingAddressTVC else { return }
        CheckoutShippingAddressTVC.delegate = self
    }
}

// MARK:- Analytics
extension CheckoutTVC {
    
    override func analyticsViewDidAppear() {
        Analytics.screenDidAppear(screen: .checkout)
    }
    
    override func analyticsViewDidDisappear() {
        guard let product = program else { return }
        Analytics.screenDidDissapear(screen: .checkout, attributes: [AnalyticAttribute.product:    product.name,
                                                                     AnalyticAttribute.medium:     product.medium.rawValue])
    }
    
    fileprivate func tagPurchaseAttempt(success: Bool, product: Product) {
        Analytics.tag(event: .purchaseAttempted, parameters: [AnalyticAttribute.success: String(success),
                                                              AnalyticAttribute.product: product.name,
                                                              AnalyticAttribute.medium: product.medium.rawValue,
                                                              AnalyticAttribute.price: String(product.priceInDollars)])
    }
}
