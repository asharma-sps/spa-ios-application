//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

protocol CheckoutCountryCodePickerViewDelegate: class {
    var phoneCountryCode: String? { get set }
}

class CheckoutCountryCodePV: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    override var numberOfComponents: Int { get { return 1 } }
    let countries = CheckoutCountriesHelper.generateCountries()
    weak var selectionDelegate: CheckoutCountryCodePickerViewDelegate?
    
    init(delegate: CheckoutCountryCodePickerViewDelegate) {
        self.selectionDelegate = delegate
        super.init(frame: CGRect.zero)
        self.delegate = self
        self.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func numberOfRows(inComponent component: Int) -> Int {
        return countries.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 { return "Select a Country Code" }
        let country = countries[row - 1]
        return "+\(country.phoneCode) \(country.name)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectionDelegate?.phoneCountryCode = row == 0 ? nil : countries[row - 1].phoneCode

    }
}
