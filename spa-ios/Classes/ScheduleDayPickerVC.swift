//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

fileprivate let kStoryboardName = "ScheduleDayPickerVC"

class ScheduleDayPickerVC: UIViewController {
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var dayOfWeekPickerView: DaysOfWeekSelectorView!
    var pattern: ScheduleWeekPattern?
    var program: WorkoutProgram?
    var session: Session?
    
    // MARK:- Load From Storyboard
    class func generateScheduleDayPicker(session: Session, pattern: ScheduleWeekPattern, program: WorkoutProgram) -> ScheduleDayPickerVC{
        let storyboard = UIStoryboard(name: kStoryboardName, bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as! ScheduleDayPickerVC
        vc.session = session
        vc.pattern = pattern
        vc.program = program
        return vc
    }
    
    // MARK:- Setup
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    fileprivate func setupView() {
        dayOfWeekPickerView.pattern = pattern
        title = program?.name
        continueButton.backgroundColor = UIColor.brandAccentBlue
        setupBlankBackButton()
    }
    
    // MARK:- IBAction
    @IBAction func continueButtonPressed(_ sender: AnyObject) {
        guard let index = dayOfWeekPickerView.selectedDayIndex else {
            ASGlobalOverlay.showAlert(withTitle: "Missing Selection", message: "You must select a starting day to proceed.", dismissButtonTitle: "OK")
            return
        }
        guard let unwrappedPattern = pattern, let unwrappedProgram = program, let unwrappedSession = session else { return }
        let vc = ScheduleWeekPickerVC.generateScheduleDayPicker(session: unwrappedSession, pattern: unwrappedPattern, startingWeekday: index, program: unwrappedProgram)
        navigationController?.pushViewController(vc, animated: true)
    }
}
