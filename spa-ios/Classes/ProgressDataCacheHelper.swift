//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

private let kDirectoryName = "progress_cache"
private let kDataFileName = "progress_data"

struct ProgressDataCacheHelper {
    
    private static let cacheDirectory = URL.documentDirectoryPath().appendingPathComponent(kDirectoryName)
    private static let dataFilePath = URL(fileURLWithPath: cacheDirectory.appendingPathComponent(kDataFileName).path)
    
    static func set(data: Data) {
        guard PersistanceHelper.createDirectoryIfNonExistant(path: cacheDirectory) else { return }
        PersistanceHelper.write(data: data, path: dataFilePath)
    }
    
    static func load() -> Data? {
        return FileManager.default.contents(atPath: dataFilePath.path)
    }
    
    static func clear() {
        PersistanceHelper.delete(path: cacheDirectory)
    }
}
