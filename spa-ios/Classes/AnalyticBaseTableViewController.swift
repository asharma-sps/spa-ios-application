//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

// Required mostly for CheckoutTVC (which is a static Storyboard)

class AnalyticBaseTableViewController: UITableViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        analyticsViewDidAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        analyticsViewDidDisappear()
    }
}

extension AnalyticBaseTableViewController {
    
    func analyticsViewDidAppear() {
        warningPrint("analyticViewDidAppear() was not overriden by AnalyticBaseTableViewController subclass")
    }
    
    func analyticsViewDidDisappear() {
        warningPrint("analyticsViewDidDisappear() was not overriden by AnalyticBaseTableViewController subclass")
    }
}
