//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import Alamofire

struct UserResourceManager {
    
    static func cleanupUserResources() {
        AuthenticatedUserPersistanceManager.clear()
        CheckoutDefaultsHelper.clear()
        StoredContentResourceHelper.deleteAllStoredContent()
        ProductsDataCacheHelper.clear()
        ScheduleDataCacheHelper.clear()
        ProgressDataCacheHelper.clear()
        GIDSignIn.sharedInstance().signOut()
        FacebookAuthHelper.signOutFacebookUser()
    }
}
