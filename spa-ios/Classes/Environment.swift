//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

struct Environment {
    
    static let isTablet = determineIfTabletDevice()
    static let isDebugBuild = wasBuiltWithDebugFlag()
    static let version = getAppVersionNumber()
    static let build = getAppBuildNumber()
    static let summary = "Mode: \(isDebugBuild ? "DEBUG" : "RELEASE"), Version: \(version), Build: \(build)"
    static let thinnestLine: CGFloat = (UIScreen.main.scale == 1.0) ? 1.0 : 0.5
    
    private static func determineIfTabletDevice() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    private static func wasBuiltWithDebugFlag() -> Bool {
        #if DEBUG
            return true
        #else
            return false
        #endif
    }
    
    private static func getAppVersionNumber() -> String {
        guard let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
            return "no_version_number"
        }
        return version
    }
    
    private static func getAppBuildNumber() -> String {
        guard let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String else {
            return "no_build_number"
        }
        return build
    }
}
