//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import KeychainAccess

struct AuthenticatedUserPersistanceManager {
    
    static private let kAuthenticatedUserTokenKey = "spa_authenticated_user_token"
    static private let kAuthenticatedUserFirstKey = "spa_authenticated_user_first"
    static private let kAuthenticatedUserLastKey = "spa_authenticated_user_last"
    static private let kAuthenticatedUserIDKey = "spa_authenticated_user_id"
    static private let kAuthenticatedUserEmailKey = "spa_authenticated_user_email"
    static private let kAuthenticatedUserAuthMethod = "spa_authenticated_user_auth_method"
    static private let keychain = Keychain(service: "com.sixpackabs.spa-ios")
    
    static func set(user: User) {
        keychain[kAuthenticatedUserFirstKey] = user.firstName
        keychain[kAuthenticatedUserLastKey] = user.lastName
        keychain[kAuthenticatedUserTokenKey] = user.token
        keychain[kAuthenticatedUserIDKey] = user.id
        keychain[kAuthenticatedUserEmailKey] = user.email
        keychain[kAuthenticatedUserAuthMethod] = user.authenticationMethod.rawValue
    }
    
    static func load() -> User? {
        guard let first = keychain[kAuthenticatedUserFirstKey],
            let last = keychain[kAuthenticatedUserLastKey],
            let token = keychain[kAuthenticatedUserTokenKey],
            let id = keychain[kAuthenticatedUserIDKey],
            let email = keychain[kAuthenticatedUserEmailKey],
            let authString = keychain[kAuthenticatedUserAuthMethod],
            let auth = AuthenticationMethod(rawValue: authString)
            else { return nil }
        return User(firstName: first, lastName: last, email: email, token: token, id: id, authenticationMethod: auth)
    }
    
    static func clear() {
        keychain[kAuthenticatedUserFirstKey] = nil
        keychain[kAuthenticatedUserLastKey] = nil
        keychain[kAuthenticatedUserTokenKey] = nil
        keychain[kAuthenticatedUserIDKey] = nil
        keychain[kAuthenticatedUserEmailKey] = nil
        keychain[kAuthenticatedUserAuthMethod] = nil
    }
}
