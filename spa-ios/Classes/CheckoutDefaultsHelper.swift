//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

// MARK:- User Defaults Keys
private let kDefaultPhoneKey = "spa_default_phone_key"
private let kDefaultPhoneCodeKey = "spa_default_phone_code"
private let kDefaultBillingFirstNameKey = "spa_default_billing_first_name"
private let kDefaultBillingLastNameKey = "spa_default_billing_last_name"
private let kDefaultBillingStreetKey = "spa_default_billing_street"
private let kDefaultBillingCityKey = "spa_default_billing_city"
private let kDefaultBillingStateKey = "spa_default_billing_state"
private let kDefaultBillingCountryKey = "spa_default_billing_country"
private let kDefaultBillingZipCodeKey = "spa_default_billing_zip"
private let kDefaultShippingFirstNameKey = "spa_default_shipping_first_name"
private let kDefaultShippingLastNameKey = "spa_default_shipping_last_name"
private let kDefaultShippingStreetKey = "spa_default_shipping_street"
private let kDefaultShippingCityKey = "spa_default_shipping_city"
private let kDefaultShippingStateKey = "spa_default_shipping_state"
private let kDefaultShippingCountryKey = "spa_default_shipping_country"
private let kDefaultShippingZipCodeKey = "spa_default_shipping_zip"

// MARK:- User Defaults Management
struct CheckoutDefaultsHelper {
    
    static func set(orderPacket: OrderPacket?) {
        CheckoutDefaultsHelper.clear()
        guard let order = orderPacket else { return }
        
        let defaults = UserDefaults.standard
        defaults.setValue(order.phone, forKey: kDefaultPhoneKey)
        defaults.setValue(order.phoneCountry, forKey: kDefaultPhoneCodeKey)
        defaults.setValue(order.billing.firstName, forKey: kDefaultBillingFirstNameKey)
        defaults.setValue(order.billing.lastName, forKey: kDefaultBillingLastNameKey)
        defaults.setValue(order.billing.street, forKey: kDefaultBillingStreetKey)
        defaults.setValue(order.billing.city, forKey: kDefaultBillingCityKey)
        defaults.setValue(order.billing.state, forKey: kDefaultBillingStateKey)
        defaults.setValue(order.billing.country, forKey: kDefaultBillingCountryKey)
        defaults.setValue(order.billing.zipCode, forKey: kDefaultBillingZipCodeKey)
        if let shipping = order.shipping {
            defaults.setValue(shipping.firstName, forKey: kDefaultShippingFirstNameKey)
            defaults.setValue(shipping.lastName, forKey: kDefaultShippingLastNameKey)
            defaults.setValue(shipping.street, forKey: kDefaultShippingStreetKey)
            defaults.setValue(shipping.city, forKey: kDefaultShippingCityKey)
            defaults.setValue(shipping.state, forKey: kDefaultShippingStateKey)
            defaults.setValue(shipping.country, forKey: kDefaultShippingCountryKey)
            defaults.setValue(shipping.zipCode, forKey: kDefaultShippingZipCodeKey)
        }
    }
    
    static func load() -> (billing: Address, shipping: Address?, phone: String, phoneCountry: String)? {
        guard let phoneCountryCode = loadValueForKey(kDefaultPhoneCodeKey) else { return nil }
        guard let phone = loadValueForKey(kDefaultPhoneKey) else { return nil }
        guard let billingFirst = loadValueForKey(kDefaultBillingFirstNameKey) else { return nil }
        guard let billingLast = loadValueForKey(kDefaultBillingLastNameKey) else { return nil }
        guard let billingStreet = loadValueForKey(kDefaultBillingStreetKey) else { return nil }
        guard let billingCity = loadValueForKey(kDefaultBillingCityKey) else { return nil }
        guard let billingState = loadValueForKey(kDefaultBillingStateKey) else { return nil }
        guard let billingCountry = loadValueForKey(kDefaultBillingCountryKey) else { return nil }
        guard let billingZip = loadValueForKey(kDefaultBillingZipCodeKey) else { return nil }
        let billingAddress = Address(firstName: billingFirst, lastName: billingLast, street: billingStreet, city: billingCity, state: billingState, country: billingCountry, zipCode: billingZip)
        var shippingAddress: Address?
        shipping: if true {
            guard let shippingFirst = loadValueForKey(kDefaultShippingFirstNameKey) else { break shipping }
            guard let shippingLast = loadValueForKey(kDefaultShippingLastNameKey) else { break shipping }
            guard let shippingStreet = loadValueForKey(kDefaultShippingStreetKey) else { break shipping}
            guard let shippingCity = loadValueForKey(kDefaultShippingCityKey) else { break shipping }
            guard let shippingState = loadValueForKey(kDefaultShippingStateKey) else { break shipping }
            guard let shippingCountry = loadValueForKey(kDefaultShippingCountryKey) else { break shipping }
            guard let shippingZip = loadValueForKey(kDefaultShippingZipCodeKey) else { break shipping }
            shippingAddress = Address(firstName: shippingFirst, lastName: shippingLast, street: shippingStreet, city: shippingCity, state: shippingState, country: shippingCountry, zipCode: shippingZip)
        }
        return (billing: billingAddress, shipping: shippingAddress, phone: phone, phoneCountry: phoneCountryCode)
    }
    
    static func clear() {
        let keys = [kDefaultPhoneKey,
                    kDefaultPhoneCodeKey,
                    kDefaultBillingFirstNameKey,
                    kDefaultBillingLastNameKey,
                    kDefaultBillingStreetKey,
                    kDefaultBillingCityKey,
                    kDefaultBillingStateKey,
                    kDefaultBillingZipCodeKey,
                    kDefaultShippingFirstNameKey,
                    kDefaultShippingLastNameKey,
                    kDefaultShippingStreetKey,
                    kDefaultShippingCityKey,
                    kDefaultShippingStateKey,
                    kDefaultShippingCountryKey,
                    kDefaultShippingZipCodeKey]
        
        let defaults = UserDefaults.standard
        for key in keys {
            defaults.setValue(nil, forKey: key)
        }
    }
    
    private static func loadValueForKey(_ key: String) -> String? {
        return UserDefaults.standard.value(forKey: key) as? String
    }
}
