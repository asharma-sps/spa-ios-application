//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

private let StoryboardName = "SettingsTVC"

// MARK:- Setup
class SettingsTVC: AnalyticBaseTableViewController, ImageSelectionManagerDelegate {

    // MARK:- Component IBOutlets
    @IBOutlet weak var videoQualityLabel: UILabel!
    @IBOutlet weak var videoQualitySwitch: UISwitch!
    @IBOutlet weak var heightLabel: UILabel!
    
    // MARK:- Cell IBOutlets
    @IBOutlet weak var heightCell: UITableViewCell!
    @IBOutlet weak var aboutUsCell: UITableViewCell!
    @IBOutlet weak var manageSavedProgramsCell: UITableViewCell!
    @IBOutlet weak var contactUsCell: UITableViewCell!
    @IBOutlet weak var logoutCell: UITableViewCell!
    
    private let imageSelectionManager = ImageSelectionManager()
    var session: Session?

    override func viewDidLoad() {
        super.viewDidLoad()
        precondition(session != nil)
        setupBlankBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        deselectAnySelectedCell()
        refreshSettingsLabels()
        guard let user = session?.user else {
            ASGlobalOverlay.showAlert(withTitle: "Error", message: ErrorMessages.inconsistancy, dismissButtonTitle: "OK")
            return
        }
        SettingsRequestHandler.updateSettingsFromServer(user: user) { (success) in
            if success { self.refreshSettingsLabels() }
        }
    }
    
    func refreshSettingsLabels() {
        videoQualityLabel.text = SettingsRequestHandler.userVideoQualityIsHD() ? "High Definition" : "Standard Definition"
        videoQualitySwitch.isOn = SettingsRequestHandler.userVideoQualityIsHD()
        heightLabel.text = SettingsRequestHandler.userHeight()?.description ?? "--"
    }
    
    class func generateSettingsTableViewController() -> SettingsTVC{
        let storyboard = UIStoryboard(name: StoryboardName, bundle: nil)
        let settingsTVC = storyboard.instantiateInitialViewController() as? SettingsTVC!
        return settingsTVC!
    }
    
    // MARK:- ImageSelectionManagementDelegate
    func presentImagePickerController(imagePicker: UIImagePickerController) {
        self.present(imagePicker, animated: true, completion:nil)
    }
}

// MARK:- Cell Selection Management
extension SettingsTVC {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        deselectAnySelectedCell()
        let cell = tableView.cellForRow(at: indexPath)
        if cell == heightCell { pushHeightSettingsViewController() }
        else if cell == aboutUsCell { pushViewController(vc: AboutVC()) }
        else if cell == manageSavedProgramsCell { pushStoredContentManagement() }
        else if cell == contactUsCell { pushContactViewController() }
        else if cell == logoutCell { pushLogoutViewController() }
    }
    
    private func pushHeightSettingsViewController() {
        guard let unwrapped = session else {
            ASGlobalOverlay.showAlert(withTitle: "Error", message: ErrorMessages.inconsistancy, dismissButtonTitle: "OK")
            return
        }
        pushViewController(vc: HeightSettingsVC(session: unwrapped))
    }
    
    private func pushViewController(vc: UIViewController) {
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func pushStoredContentManagement() {
        guard let unwrappedSession = session else { return }
        pushViewController(vc: StoredContentManagerVC(session: unwrappedSession))
    }
    
    private func pushLogoutViewController() {
        guard let unwrapped = session else {
            print("PRECONDITION UNSATISFIED-- No session injected into SettingsTVC")
            return
        }
        pushViewController(vc: LogoutVC(session: unwrapped))
    }
    
    private func pushContactViewController() {
        let contact = ContactTVC.generateContactTableViewController()
        contact.session = session
        pushViewController(vc: contact)
    }
}

// MARK:- IBActions
extension SettingsTVC {
    
    @IBAction func videoQualitySwitchToggled(_ sender: AnyObject) {
        ASGlobalOverlay.showWorkingIndicator(withDescription: nil)
        videoQualityLabel.text = videoQualitySwitch.isOn ? "High Definition" : "Standard Definition"
        SettingsRequestHandler.setUserVideoQualityIsHD(user: session!.user, value: videoQualitySwitch.isOn) { (success) in
            if success {
                ASGlobalOverlay.dismissWorkingIndicator()
            } else {
                ASGlobalOverlay.showAlert(withTitle: "Error", message: "We were unable to update your settings. Please check your network settings and try again.", dismissButtonTitle: "OK")
                self.videoQualitySwitch.setOn(!self.videoQualitySwitch.isOn, animated: true)
                self.refreshSettingsLabels()
            }
        }
    }
}

// MARK:- Table View Helper Functionality
extension SettingsTVC {
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.setSelected(false, animated: false)
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == logoutCellSection() {
            guard let user = session?.user else { return nil }
            var text = "\(user.firstName) \(user.lastName)\n\(user.email)\nVersion: \(Environment.version) Build: \(Environment.build)"
            if user.authenticationMethod != .native {
                text.append("\nAuthenticated with \(user.authenticationMethod.rawValue.capitalized)")
            }
            if Environment.isDebugBuild {
                text.append("\nDEBUG BUILD --- QA PLEASE FAIL.")
            }
            return text
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if section == logoutCellSection() {
            guard let footerView = view as? UITableViewHeaderFooterView else { return }
            footerView.textLabel?.textAlignment = .center
        }
    }
    
    private func logoutCellSection() -> Int {
        return tableView.numberOfSections - 1
    }
}

// MARK:- Analytics
extension SettingsTVC {
    
    override func analyticsViewDidAppear() {
        Analytics.screenDidAppear(screen: .settings)
    }
    
    override func analyticsViewDidDisappear() {
        Analytics.screenDidDissapear(screen: .settings)
    }
}
