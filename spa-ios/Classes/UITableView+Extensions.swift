//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

extension UITableView {
    
    func deselectAnySelectedCell() {
        guard let selectedIndexPath = indexPathForSelectedRow else { return }
        guard let cell = cellForRow(at: selectedIndexPath) else { return }
        cell.setSelected(false, animated: true)
    }
}
