//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class ExerciseSegmentCompletedView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtextLabel: UILabel!
    
    class func generateExerciseSegmentCompletedView() -> ExerciseSegmentCompletedView {
        guard let vc = Bundle.main.loadNibNamed("ExerciseSegmentCompletedView", owner: nil, options: nil)?[0] as? ExerciseSegmentCompletedView else {
            return ExerciseSegmentCompletedView()
        }
        return vc
    }
}
