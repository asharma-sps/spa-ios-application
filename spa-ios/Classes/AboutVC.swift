//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class AboutVC: UIViewController {
    
    let textView = UITextView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "About Us"
        setupTextView()
    }
    
    private func setupTextView() {
        textView.isScrollEnabled = false
        textView.font = UIFont.brandMedium()
        textView.isEditable = false
        textView.dataDetectorTypes = .link
        textView.textContainerInset = UIEdgeInsetsMake(20, 20, 20, 20)
        textView.text = aboutUsCopy()
        view.addSubview(textView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textView.isScrollEnabled = true
    }
    
    override func viewDidLayoutSubviews() {
        textView.frame = CGRect(0.0, 0.0, view.frame.width, view.frame.height)
    }
    
    func aboutUsCopy() -> String {
        return "Over 8 years ago, I was overweight and out of shape. Finally, one day I made the decision that I was going to learn how to get a ripped body and abs - no matter what.\n\nWhen I first started, I made every mistake in the book. But through trial and error, I finally learned the exercise and nutrition strategies that get you into great shape.\n\nOnce I learned this stuff, I got into shape surprisingly fast. It made a huge difference in my life - it became much easier for me to attract women, I got much more respect at work, dramatically improved my social life. And I just felt way better about myself and had much more confidence every time I looked in the mirror.\n\nAnd six years ago, I decided that I wanted to help others change their life through fitness the same way I’d changed mine. So I got my Personal Trainer Certification, and started training people full-time.\n\nEventually I started specializing in ab training, and I became known as a “Six Pack Abs Coach.” I trained hundreds of guys to get abs using my system, and kept refining it and improving it over time.\n\nEventually, a few of my clients, who had great success with my system, convinced me to put it online so that everyone could have access to it. We called it “Six Pack Shortcuts,” because it’s the proven way to get six pack abs, faster and easier.\n\nSix Pack Shortcuts uses a specialized type of training that harness what is known today as the Afterburn Effect. This allows your body not only to burn fat during your workouts, but up to 48 hours after your training is done. This was what separated Six Pack Shortcuts from any other workout available and what made it so effective in helping countless guys just like you get the body they deserved.\n\nNow six years later, with the help of our ever improving science and technology, I’ve discovered there’s a faster and even more effective way to getting rid of you belly fat and finally getting six pack abs. Now even though supporting your metabolism and maximizing your calorie burn with the Afterburn Effect is very effective and powerful…With further research, I soon realized that burning more calories than you intake is only the first step in getting rid of excess fat on your body. What I found was even more important is being able take control the one thing your body depends on for fat burning and preventing fat storage - Your Fat burning hormones.\n\nAnd that’s why I’ve updated my original Six Pack Shortcuts Program to my new Six Pack Shortcuts 2.0. Not will Six Pack Shortcuts 2 maximize your calorie burn with the Afterburn Effect, in addition it’s upgraded with my hormones support training system.\n\nThis is how you’ll be able to get rid of your belly fat even faster and easier than before…In just 3 simple steps.\n\nListen, I know you want to get rid of your belly fat and get into killer shape…And I’d like to be there by your side and help you get there. I want you to get the body you want for yourself…but remember you don’t have to do this by yourself. I encourage you to get started with me TODAY.\n\nYou can learn more about Six Pack Shortcuts 2 from the Hormone Training video I posted on my site: http://www.sixpackshortcuts.com/.\n\n- Clark Shao"
    }
}
