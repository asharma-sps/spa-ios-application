//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

fileprivate let kButtonDiameter = CGFloat(38.0)

// MARK:- Setup
class DaysOfWeekSelectorView: UIView {
    
    var pattern: ScheduleWeekPattern?
    var selectedDayIndex: Int?
    let buttons = [UIButton(type: .system),
                   UIButton(type: .system),
                   UIButton(type: .system),
                   UIButton(type: .system),
                   UIButton(type: .system),
                   UIButton(type: .system),
                   UIButton(type: .system)] // 7 days per week
    
    init(pattern: ScheduleWeekPattern) {
        super.init(frame: CGRect.zero)
        commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonSetup()
    }
    
    func commonSetup() {
        backgroundColor = UIColor.clear
        let days = ["S", "M", "T", "W", "Th", "F", "S"]
        var i = 0
        
        for button in buttons {
            button.setTitle(days[i], for: .normal)
            i += 1
            setButtonColor(button: button, selected: false)
            button.layer.masksToBounds = true
            button.layer.cornerRadius = kButtonDiameter / 2
            button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
            addSubview(button)
        }
    }
}

// MARK:- Selection Management
extension DaysOfWeekSelectorView {

    @objc func buttonPressed(button: UIButton) {
        guard let index = buttons.index(of: button) else { return }
        selectedDayIndex = index
        colorButtonsForPatternAtIndex(index: index)
    }
    
    func colorButtonsForPatternAtIndex(index: Int) {
        guard let pat = pattern else {return}
        var currentIndex = index
        var i = 0
        for _ in buttons {
            setButtonColor(button: buttons[currentIndex], selected: pat.valueForIndex(index: i))
            currentIndex += 1
            if currentIndex == 7 { currentIndex = 0 }
            i += 1
        }
    }
    
    func setButtonColor(button: UIButton, selected: Bool) {
        button.backgroundColor = selected ? UIColor.brandAccentBlue : UIColor.white
        button.setTitleColor(selected ? UIColor.white : UIColor.brandGray, for: .normal)
    }
}

// MARK:- Layout
extension DaysOfWeekSelectorView {
    
    override func layoutSubviews() {
        let spaceBetweenButtons = (frame.width - CGFloat(kButtonDiameter * 7)) / 6
        var nextOriginX = CGFloat(0.0)
        for button in buttons {
            button.frame = CGRect(nextOriginX, 0.0, kButtonDiameter, kButtonDiameter)
            nextOriginX += kButtonDiameter + spaceBetweenButtons
        }
    }
}
