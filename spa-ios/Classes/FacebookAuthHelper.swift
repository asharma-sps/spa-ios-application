//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import FacebookCore
import FacebookLogin

enum FacebookAuthResult {
    case success
    case failure
    case cancelled
}

class FacebookAuthHelper {
    
    private static let singleton = FacebookAuthHelper()
    private let manager = LoginManager()
    private init() {}
    
    static func showUserFacebookAuthDialog(viewController: UIViewController, completion: @escaping (FacebookAuthResult) -> Void) {
        singleton.manager.logOut()
        singleton.manager.logIn([.publicProfile], viewController: viewController) { loginResult in
            switch loginResult {
            case .failed(let error):
                errorPrint("Failed to authenticate Facebook user", error)
                completion(.failure)
                return
            case .cancelled:
                completion(.cancelled)
            case .success(let grantedPermissions, _, _):
                print(grantedPermissions)
                print(grantedPermissions.count)
                completion(.success)
            }
        }
    }
    
    static func authenticateUsingCurrentFacebookUser(completion: @escaping (User?) -> Void) {
        let infoRequest = GraphRequest(graphPath: "me", parameters: ["fields" : "first_name,last_name,email"])
        infoRequest.start { (httpResponse, graphResponse) in
            switch graphResponse {
            case .success(let response):
                let wrappedInfo = response.dictionaryValue
                let wrappedUserID = wrappedInfo?["id"] as? String
                let wrappedEmail = wrappedInfo?["email"] as? String
                guard let info = wrappedInfo, let userID = wrappedUserID, let email = wrappedEmail else {
                    completion(nil)
                    return
                }
                let first = info["first_name"] as? String ?? "(no first name)"
                let last = info["last_name"] as? String ?? "(no last name)"
                
                AuthenticationRequestHandler.loginSocial(token: userID, vendor: .facebook, first: first, last: last, email: email, completion: { result in
                    switch result {
                    case .success(let user):
                        completion(user)
                    case .failure:
                        completion(nil)
                    }
                })
                
            case .failed(let error):
                errorPrint("Failed to download Facebook user data", error) //ASTODO, handle conflicting emails.
                completion(nil)
            }
        }
    }
    
    static func signOutFacebookUser() {
        singleton.manager.logOut()
    }
}
