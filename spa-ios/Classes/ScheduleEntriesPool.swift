//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

class ScheduleEntriesPool {

    fileprivate var pool = Dictionary<String, [ScheduleEntry]>()
    
    func addEntries(entries: [ScheduleEntry]) {
        for entry in entries {
            // print("entry date:\(entry.date) -- date string: \(entry.dateString)") // DEBUG
            if pool[entry.dateString] != nil {
                pool[entry.dateString]!.append(entry)
            } else {
                pool[entry.dateString] = [entry]
            }
        }
    }
    
    func getEntries(dateString: String) -> [ScheduleEntry] {
        guard let entries = pool[dateString] else { return [] }
        return entries
    }
    
    func getEntriesForWeek(startDate: Date) -> [[ScheduleEntry]] {
        let dateStrings = ScheduleDateHelper.generateDateStringsForWeekStartingOnDate(startDate: startDate)
        var entriesByWeekday = [[ScheduleEntry]]()
        for dateString in dateStrings {
            let entries = getEntries(dateString: dateString)
            entriesByWeekday.append(entries)
        }
        return entriesByWeekday
    }
}
