//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import AVFoundation

private let kPlayPauseButtonWidth = CGFloat(45.0)
private let kTimingLabelWidth = CGFloat(50.0)
private let kFullscreenButtonWidth = CGFloat(45.0)

protocol PlayerControlsViewDelegate: class {
    func fullscreenPressed(player: AVPlayer)
    func playPressed()
}

class PlayerControlsView: UIView {
    
    weak var delegate: PlayerControlsViewDelegate?
    fileprivate let playPauseButton = UIButton(type: .system)
    fileprivate let timeElapsedLabel = PlayerControlsView.generateTimingLabel()
    fileprivate let trackingView = PlayerTrackingView()
    fileprivate let timeRemainingLabel = PlayerControlsView.generateTimingLabel()
    fileprivate let fullscreenButton = UIButton(type: .system)
    fileprivate var playWhenScrubbingEnds = false
    fileprivate var tracker: Any?
    fileprivate weak var player: AVPlayer?
    
    init() {
        super.init(frame: CGRect.zero)
        configureView()
        setupPlayPauseButton()
        setupTimeElapsedLabel()
        setupProgressBar()
        setupTimeRemainingLabel()
        setupFullscreenButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        backgroundColor = UIColor.black.withAlphaComponent(0.85)
    }
    
    private func setupPlayPauseButton() {
        playPauseButton.tintColor = UIColor.white
        playPauseButton.contentMode = .center
        playPauseButton.addTarget(self, action: #selector(togglePlayPausePressed), for: .touchUpInside)
        addSubview(playPauseButton)
    }
    
    private func setupTimeElapsedLabel() {
        timeElapsedLabel.text = "--:--"
        addSubview(timeElapsedLabel)
    }
    
    private func setupProgressBar() {
        trackingView.delegate = self
        addSubview(trackingView)
    }
    
    private func setupTimeRemainingLabel() {
        timeRemainingLabel.text = "--:--"
        addSubview(timeRemainingLabel)
    }
    
    private func setupFullscreenButton() {
        fullscreenButton.setImage(UIImage(named: "spa_player_fullscreen"), for: .normal)
        fullscreenButton.tintColor = UIColor.white
        fullscreenButton.contentMode = .center
        fullscreenButton.addTarget(self, action: #selector(fullscreenButtonPressed), for: .touchUpInside)
        addSubview(fullscreenButton)
    }
    
    private class func generateTimingLabel() -> UILabel {
        let label = UILabel()
        label.font = UIFont.brandRegular(size: 12.0)
        label.textColor = UIColor.white
        label.textAlignment = .center
        return label
    }
}

// MARK:- ProgressBarDelegate
extension PlayerControlsView: PlayerTrackingViewDelegate {

    func trackingPercentageCompleteDidChange(percent: Double) {
        guard let player = player else { return }
        guard let duration = player.currentItem?.duration.seconds else { return }
        let newCurrentTime = CMTime(seconds: percent * duration, preferredTimescale: 600)
        player.seek(to: newCurrentTime)
    }
    
    func scrubbingStateDidChanged(isScrubbing: Bool) {
        if isScrubbing {
            playWhenScrubbingEnds = (player?.rate == 1.0)
            player?.pause()
        } else {
            if playWhenScrubbingEnds {
                player?.play()
            }
        }
    }
}

// MARK:- Layout
extension PlayerControlsView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let progressBarWidth = frame.width - (kTimingLabelWidth * 2 + kFullscreenButtonWidth + kPlayPauseButtonWidth)
        playPauseButton.frame = CGRect(0.0, 0.0, kPlayPauseButtonWidth, frame.height)
        timeElapsedLabel.frame = CGRect(playPauseButton.frame.maxX, 0.0, kTimingLabelWidth, frame.height)
        trackingView.frame = CGRect(timeElapsedLabel.frame.maxX, 0.0, progressBarWidth, frame.height)
        timeRemainingLabel.frame = CGRect(trackingView.frame.maxX, 0.0, kTimingLabelWidth, frame.height)
        fullscreenButton.frame = CGRect(timeRemainingLabel.frame.maxX, 0.0, kFullscreenButtonWidth, frame.height)
    }
}

// MARK:- Player Management
extension PlayerControlsView {
    
    func setPlayer(player: AVPlayer) {
        player.addObserver(self, forKeyPath: "rate", options: [], context: nil)
        let interval = CMTime(seconds: 1, preferredTimescale: 600)
        tracker = player.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) { currentTime in
            guard let duration = self.player?.currentItem?.duration else { return }
            self.updateTimingLabels(current: currentTime, total: duration)
            self.updateProgressBar(current: currentTime, total: duration)
        }
        self.player = player
    }
    
    func clearPlayer() {
        guard let player = player else { return }
        player.removeObserver(self, forKeyPath: "rate")
        if let tracker = tracker {
            player.removeTimeObserver(tracker)
        }
        self.player = nil
    }

    private func updateTimingLabels(current: CMTime, total: CMTime) {
        guard total.seconds.isNormal, current.seconds.isNormal else {
            timeElapsedLabel.text = "--:--"
            timeRemainingLabel.text = "--:--"
            return
        }
        
        let elapsed = Int(current.seconds)
        let remaining = Int(total.seconds) - Int(current.seconds)
        timeElapsedLabel.text = TimeFormattingHelper.formattedTime(seconds: elapsed, negative: false, alwaysShowMinutes: true)
        timeRemainingLabel.text = TimeFormattingHelper.formattedTime(seconds: remaining, negative: true, alwaysShowMinutes: true)
    }
    
    private func updateProgressBar(current: CMTime, total: CMTime) {
        let percent = current.seconds / total.seconds
        trackingView.percent = percent
    }
}

// MARK:- Button State Management
extension PlayerControlsView {
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        updatePlayPauseButtonIcon()
    }
    
    fileprivate func updatePlayPauseButtonIcon() {
        guard let player = self.player else { return }
        let isPaused = (player.rate == 0.0)
        let icon = isPaused ?  UIImage(named: "spa_player_play") : UIImage(named: "spa_player_pause")
        playPauseButton.setImage(icon, for: .normal)
    }
}

// MARK:- Button Targets
extension PlayerControlsView {
    
    // AVPlayerTimeControlStatus.paused == 0.0, hardcoding b/c iOS 9 compatibility (was a static string, now an enum)
    
    @objc func togglePlayPausePressed() {
        guard let player = self.player else { return }
        let isPaused = (player.rate == 0.0)
        if isPaused { delegate?.playPressed() }
        player.rate = isPaused ? 1.0 : 0.0
        updatePlayPauseButtonIcon()
    }
    
    @objc func fullscreenButtonPressed() {
        guard let player = self.player else { return }
        self.delegate?.fullscreenPressed(player: player)
    }
}
