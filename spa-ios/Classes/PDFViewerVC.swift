//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//


import UIKit
import WebKit

class PDFViewerVC: NetworkViewController {
    
    private let webView = WKWebView(frame: CGRect.zero)
    private let url: URL
    
    init(url: URL, title: String?) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
        self.title = title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupWebView()
        loadPDF()
    }
    
    private func loadPDF() {
        baseViewControllerState = .loadingView
        let localPath = ProgramPDFHelper.pathForGuideFile(name: url.lastPathComponent)
        if FileManager.default.fileExists(atPath: localPath.path) {
            setPDF(localPath: localPath)
        } else {
            downloadGuide(savePath: localPath, resourceURL: url)
        }

    }
    
    private func downloadGuide(savePath: URL, resourceURL: URL) {
        ProgramPDFHelper.downloadGuide(savePath: savePath, resourceURL: resourceURL) { success in
            if success {
                self.setPDF(localPath: savePath)
            } else {
                self.baseViewControllerState = .failureView
            }
        }
    }
    
    private func setupWebView() {
        mainView.addSubview(webView)
        webView.backgroundColor = UIColor.groupTableViewBackground
    }
    
    private func setPDF(localPath: URL) {
        webView.loadFileURL(localPath, allowingReadAccessTo: localPath)
        baseViewControllerState = .mainView
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webView.frame = CGRect(0.0, 0.0, mainView.frame.width, mainView.frame.height)
    }
    
    override func retryLoadingData() {
        loadPDF()
    }
}


