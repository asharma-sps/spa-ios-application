//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

extension UIColor {
    
    // blues
    static let brandAccentBlue = UIColor(red: 0.161, green: 0.502, blue: 0.902, alpha: 1.00)
    static let brandLightBlue = UIColor(red: 45/255, green: 114/255, blue: 241/255, alpha: 1.0)
    static let brandDarkBlue = UIColor(red: 11/255, green: 46/255, blue: 85/255, alpha: 1.0)

    // grays
    static let brandUltraLightGray = UIColor(white: 0.85, alpha: 1.0)
    static let brandLightGray = UIColor(white: 0.65, alpha: 1.0)
    static let brandGray = UIColor(white: 0.5, alpha: 1.0)
    static let brandDarkGray = UIColor(white: 0.35, alpha: 1.0)
    static let brandUltraDarkGray = UIColor(red: 0.059, green: 0.059, blue: 0.067, alpha: 1.00)
}
