//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

struct PersistanceHelper {
    
    @discardableResult static func write(data: Data, path: URL) -> Bool {
        do {
            try data.write(to: path)
        } catch {
            errorPrint("Failed to save data to path. Path: \(path)", error)
            return false
        }
        return true
    }
    
    @discardableResult static func delete(path: URL) -> Bool {
        if !FileManager.default.fileExists(atPath: path.path) {
            print("Could not delete item at path because the path item not exist. \(path)")
            return true
        }
        do {
            try FileManager.default.removeItem(at: URL(fileURLWithPath: path.path))
        } catch {
            errorPrint("Failed to delete item at path. Path: \(path)", error)
            return false
        }
        return true
    }
    
    @discardableResult static func createDirectoryIfNonExistant(path: URL) -> Bool {
        if !FileManager.default.fileExists(atPath: path.path) {
            do {
                let filePath = URL(fileURLWithPath: path.absoluteString, isDirectory: true)
                try FileManager.default.createDirectory(at: filePath, withIntermediateDirectories: true, attributes: nil)
            } catch {
                errorPrint("Failed to create path. Path: \(path)", error)
                return false
            }
        }
        return true
    }
}
