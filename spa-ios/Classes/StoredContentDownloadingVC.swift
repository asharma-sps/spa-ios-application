//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

private let kDownloadButtonHeight = CGFloat(50.0)
private let kProgressViewHeight = StoredContentProgressView.suggestedHeight
private let kViewSpacing = CGFloat(25.0)

class StoredContentDownloadingVC: UIViewController {
    
    private let instructionsLabel = UILabel()
    private let progressView = StoredContentProgressView()
    private let downloadButton = UIButton.generateButton(title: "Start Download")
    private let product: Product
    private let downloader: StoredContentDownloader
    
    private var isDownloading = false {
        didSet {
            setDownloadButtonState(enabled: !isDownloading)
            progressView.isHidden = !isDownloading
        }
    }
    
    init(product: Product, session: Session) {
        self.product = product
        self.downloader = StoredContentDownloader(session: session, productID: product.id)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupInstructionLabel()
        setupProgressView()
        setupDownloadButton()
    }
    
    private func configureView() {
        title = product.name
        view.backgroundColor = UIColor.groupTableViewBackground
        addCloseButton()
    }
    
    private func setupInstructionLabel() {
        instructionsLabel.font = UIFont.brandRegular()
        instructionsLabel.textColor = UIColor.brandGray
        instructionsLabel.numberOfLines = 0
        instructionsLabel.text = "Downloading a program allows you to view its exercise clips without an internet connect.\n\nTo conserve memory, downloading a program excludes full-length walkthrough videos.\n\nIMPORTANT:\nIt is recomended that you connect to WiFi network before you begin.\n\n"
        view.addSubview(instructionsLabel)
    }
    
    private func setupProgressView() {
        progressView.isHidden = true
        view.addSubview(progressView)
    }
    
    private func setupDownloadButton() {
        downloadButton.addTarget(self, action: #selector(startDownload), for: .touchUpInside)
        setDownloadButtonState(enabled: true)
        view.addSubview(downloadButton)
    }
    
    override func dismissSelf() {
        if !isDownloading { super.dismissSelf(); return }
        
        let cancel = ASUserOption.destructiveUserOption(withTitle: "Cancel Download") {
            self.downloader.cancelWhenPossible = true
            ASGlobalOverlay.showWorkingIndicator(withDescription: "Canceling...")
        }
        let keepDownloading = ASUserOption.cancel(withTitle: "Continue Downloading", actionBlock: nil)
        ASGlobalOverlay.showAlert(withTitle: "Cancel Download?", message: "Are you sure you want to stop downloading \(product.name)?", userOptions: [keepDownloading as Any, cancel as Any])
    }
    
    @objc private func startDownload() {
        isDownloading = true
        let progress: (StoredContentDownloader.State) -> () = { state in
            DispatchQueue.main.async {
                switch state {
                case .preparing:
                    self.progressView.update(text: "Preparing...", spinning: true, progress: 0.0)
                case .downloadingVideos(let total, let current):
                    self.progressView.update(text: "Downloading exercise \(current) of \(total)", spinning: true, progress: Float(current) / Float(total))
                }
            }
        }
        
        downloader.saveContentForOfflineUse(product: product, progress: progress) { success in
            DispatchQueue.main.async {
                if success {
                    super.dismissSelf()
                    ASGlobalOverlay.showAlert(withTitle: "Download Complete", message: "You can now view \(self.product.name) exercises offline.", dismissButtonTitle: "OK")
                } else if self.downloader.cancelWhenPossible {
                    ASGlobalOverlay.dismissWorkingIndicator()
                    super.dismissSelf()
                } else {
                    ASGlobalOverlay.showAlert(withTitle: "Error", message: "We are unable to complete your download. Please check your network settings and try again.", dismissButtonTitle: "OK")
                    self.isDownloading = false
                }
            }
        }
    }
    
    private func setDownloadButtonState(enabled: Bool) {
        downloadButton.backgroundColor = enabled ? UIColor.brandAccentBlue : UIColor.brandLightGray
        downloadButton.setTitleColor(enabled ?  UIColor.white : UIColor.brandUltraLightGray , for: .normal)
        downloadButton.setTitle(enabled ? "Start Download" : "Downloading...", for: .normal)
        downloadButton.isUserInteractionEnabled = enabled
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let contentWidth = view.frame.width - kViewSpacing * 2
        let instructionLabelMaxHeight = view.frame.height - kDownloadButtonHeight - kProgressViewHeight - kViewSpacing * 4
        let instructionLabelAutoHeight = instructionsLabel.sizeThatFits(CGSize(contentWidth, CGFloat.greatestFiniteMagnitude)).height
        instructionsLabel.frame = CGRect(kViewSpacing, kViewSpacing, contentWidth, min(instructionLabelAutoHeight, instructionLabelMaxHeight))
        downloadButton.frame = CGRect(kViewSpacing, view.frame.height - kViewSpacing - kDownloadButtonHeight, contentWidth, kDownloadButtonHeight)
        progressView.frame = CGRect(kViewSpacing, downloadButton.frame.origin.y - kViewSpacing - kProgressViewHeight, contentWidth, kProgressViewHeight)
    }
}
