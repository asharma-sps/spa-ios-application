//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import Alamofire

// MARK:- GET
class ProductsRequestHandler {
    
    typealias ProductRequestCompletion = (Bool, [Product], Data?) -> ()
    
    func requestProducts(user: User, completion: @escaping ProductRequestCompletion) {
        let request = Router(.getProducts, user)
        Alamofire.request(request)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    guard let checked = value as? [[String: AnyObject]], let data = response.data else {
                        completion(false, [], nil)
                        return
                    }
                    let products = self.deserializeProduct(data: checked)
                    completion(true, products, data)
                case .failure(let error):
                    errorPrint("Failed to fetch products", error)
                    responseErrorPrint("Failed to fetch products", error, response.data)
                    completion(false, [], nil)
                }
        }
    }
    
    func generateProductsWithData(data: Data) -> [Product]? {
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else { return nil }
        guard let checkedJSON = json as? [[String : AnyObject]] else { return nil }
        return deserializeProduct(data: checkedJSON)
    }
}

// MARK:- Deserializers
extension ProductsRequestHandler {
    
    fileprivate func deserializeProduct(data: [[String: AnyObject]]) -> [Product] {
        var products = [Product]()
        for dict in data {

            // basics
            guard let id = dict["id"] as? Int else { continue }
            guard let name = dict["name"] as? String else { continue }
            guard let priceInDollars = dict["price"] as? Int else { continue }
            guard let shippingPrice = dict["shipping_price"] as? Float else { continue }
            guard let shippable = dict["shippable"] as? Bool else { continue }
            guard let imageURL = dict["banner"]?["md"] as? String else { continue }
            
            // PDF resource
            let resourceDicts = dict["resources"] as? [[String: AnyObject]] ?? [] // API allows this to be nil
            var resources: [Resource] = []
            for item in resourceDicts {
                guard let title = item["title"] as? String,
                    let path = item["path"] as? String,
                    let url = URL(string: path)
                    else { continue }
                resources.append(Resource(title: title, resource: url))
            }
            
            // enums
            guard let accessString = dict["access"] as? String, let accessType = Product.AccessType(rawValue: accessString) else { continue }
            guard let mediumString = dict["type"] as? String, let mediumType = Product.MediumType(rawValue: mediumString) else { continue }
            guard let productString = dict["subtype"] as? String, let productType = Product.ProductType(rawValue: productString) else { continue }
            guard let purchasedStateString = dict["category"] as? String, let purchasedStateType = Product.PurchasedState(rawValue: purchasedStateString) else { continue }

            // sections
            guard let sectionsDict = dict["sections"] as? [[String: AnyObject]] else { continue }
            let sections = deserializeSections(data: sectionsDict, productID: id)
            
            // create and append
            let product = Product(id: id,
                                      name: name,
                                      imageURL: imageURL,
                                      medium: mediumType,
                                      type: productType,
                                      access: accessType,
                                      purchasedState: purchasedStateType,
                                      resources: resources.isEmpty ? nil : resources,
                                      sections: sections,
                                      priceInDollars: priceInDollars,
                                      shippable: shippable,
                                      shippingPrice: shippingPrice)
            products.append(product)
        }
        return products
    }

    private func deserializeSections(data: [[String: AnyObject]], productID: Int) -> [Section] {
        var sections = [Section]()
        for dict in data {

            // basics
            guard let id = dict["id"] as? Int else { continue }
            guard let position = dict["position"] as? Int else { continue }
            guard let name = dict["name"] as? String else { continue }
            let locked = dict["content_filtered"] as? Bool ?? false // key may not always be present

            // enums
            guard let productString = dict["type"] as? String, let productType = Section.ProductType(rawValue: productString) else { continue }

            // workouts
            guard let workoutData = dict["contents"] as? [[String: AnyObject]] else { continue }
            let workouts = deserializeWorkouts(data: workoutData, productID: productID)
            
            // create and append
            let section = Section(id: id, position: position, name: name, type: productType, workouts: workouts, contentLocked: locked)
            sections.append(section)
        }
        return sections
    }
    
    private func deserializeWorkouts(data: [[String: AnyObject]], productID: Int) -> [Workout] {
        
        var workouts = [Workout]()
        for dict in data {
            
            // basics
            guard let id = dict["id"] as? Int else { continue }
            guard let title = dict["title"] as? String else { continue }
            guard let subtitle = dict["subtitle"] as? String else { continue }
            guard let description = dict["description"] as? String else { continue }
            guard let cover = dict["cover"]?["md"] as? String else { continue }
            let locked = dict["content_filtered"] as? Bool ?? false // key may not always be present
            let summary = dict["summary"] as? String ?? "" // key may not always be present
            
            let resources = dict["resources"] as? [[String: String]] ?? [[String: String]]()// key may not always be present
            var PDFResources = [Resource]()
            for resourceDict in resources {
                print(resourceDict)
                guard let title = resourceDict["title"],
                    let resourcePath =  resourceDict["path"],
                    let resourceURL = URL(string: resourcePath) else {
                    continue
                }
                let resource = Resource(title: title, resource: resourceURL)
                PDFResources.append(resource)
            }

            // enums / structs
            guard let typeString = dict["type"] as? String else { continue }
            let type = typeString == "workout" ? WorkoutStatus.Workout : WorkoutStatus.Other
            guard let videoDict = dict["video"] as? [String: AnyObject] else { continue }
            let video = VideoResource(data: videoDict)
            
            // create and append
            let workout = Workout(id: id,
                                  type: typeString,
                                  title: title,
                                  subtitle: subtitle,
                                  description: description,
                                  summary: summary,
                                  video: video,
                                  cover: cover,
                                  status: type,
                                  productID: productID,
                                  contentLocked: locked,
                                  PDFResources: PDFResources)
            
            workouts.append(workout)
        }
        return workouts
    }
}
