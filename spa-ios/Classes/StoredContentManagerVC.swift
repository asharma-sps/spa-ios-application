//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

class StoredContentManagerVC: NetworkViewController {
    
    let tableView = UITableView(frame: CGRect.zero, style: .grouped)
    var products = [Product]()
    let session: Session

    init(session: Session) {
        self.session = session
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupTableView()
        setupProducts()
    }
    
    private func configureView() {
        title = "Download Manager"
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(StoredContentManagerVCTableViewCell.self, forCellReuseIdentifier: StoredContentManagerVCTableViewCell.reuseID)
        mainView.addSubview(tableView)
    }
    
    private func setupProducts() {
        var productsTemp = [Product]()
        for product in session.productsPool.products {
            let isOwned = (product.purchasedState == .paid || product.purchasedState == .free)
            let isProduct = product.type == .program
            let isNotExcluded = !(product.id == 28)
            if isOwned && isProduct && isNotExcluded  {
                productsTemp.append(product)
            }
        }
        products = productsTemp
        baseViewControllerState = .mainView
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
}

extension StoredContentManagerVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: StoredContentManagerVCTableViewCell.reuseID) ?? StoredContentManagerVCTableViewCell(style: .subtitle, reuseIdentifier: StoredContentManagerVCTableViewCell.reuseID)
        
        let product = products[indexPath.row]
        cell.textLabel?.text = product.name
        cell.detailTextLabel?.text = cellDetailText(product: product) // not efficient, but also not worth optimizing.
        return cell
    }
    
    private func cellDetailText(product: Product) -> String {
        let resourceHelper = StoredContentResourceHelper(productID: product.id)
        if !resourceHelper.isProductDownloaded() {
            return "Tap to download"
        }
        return resourceHelper.fileSizeDescription() ?? "Downloaded"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectAnySelectedCell()
        let resourceHelper = StoredContentResourceHelper(productID: products[indexPath.row].id)
        if resourceHelper.isProductDownloaded() {
            let delete = ASUserOption.destructiveUserOption(withTitle: "Delete") {
                resourceHelper.deleteProductStoredContent()
                self.tableView.reloadData()
            }
            let cancel = ASUserOption.cancel(withTitle: "Cancel", actionBlock: nil)
            ASGlobalOverlay.showAlert(withTitle: "Delete Download", message: "Would you like to delete \(products[indexPath.row].name)? You can re-download it in the future.", userOptions: [delete as Any, cancel as Any])
        } else {
            let vc = StoredContentDownloadingVC(product: products[indexPath.row], session: session)
            let nc = UINavigationController(rootViewController: vc)
            present(nc, animated: true, completion: nil)
        }
    }
}

extension StoredContentManagerVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(0.0, 0.0, mainView.frame.width, mainView.frame.height)
    }
}

class StoredContentManagerVCTableViewCell: UITableViewCell {
    
    static let reuseID = "saved_programs_cell"
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        textLabel?.font = UIFont.brandMedium()
        detailTextLabel?.font = UIFont.brandRegular()
        detailTextLabel?.textColor = UIColor.lightGray
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
