//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

fileprivate let kWeekdayLabelHeight = CGFloat(12)
fileprivate let kDayLabelHeight = CGFloat(20)
fileprivate let kDateLabelAreaWidth = CGFloat(50)

class DayTableViewCell: UITableViewCell {
    
    fileprivate let weekdayLabel = UILabel()
    fileprivate let dayLabel = UILabel()
    fileprivate let descriptionLabel = UILabel()
    fileprivate let bottomSeporatorLine = UIView()
    fileprivate let dateAreaSeporatorLine = UIView()

    // MARK:- Setup Sequence
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupWeekdayLabel()
        setupDayLabel()
        setupDescriptionLabel()
        setupSeporatorLine()
        setupDateAreaSeporatorLine()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupWeekdayLabel() {
        weekdayLabel.font = UIFont.brandRegular(size: 10.0)
        weekdayLabel.textColor = UIColor.brandDarkGray
        weekdayLabel.textAlignment = .center
        weekdayLabel.text = "-"
        addSubview(weekdayLabel)
    }
    
    fileprivate func setupDayLabel() {
        dayLabel.font = UIFont.brandMedium(size: 18.0)
        dayLabel.textColor = UIColor.brandDarkGray
        dayLabel.textAlignment = .center
        dayLabel.text = "-"
        addSubview(dayLabel)
    }
    
    fileprivate func setupDescriptionLabel() {
        descriptionLabel.font = UIFont.brandMedium(size: 20.0)
        descriptionLabel.numberOfLines = 2
        addSubview(descriptionLabel)
    }
    
    fileprivate func setupSeporatorLine() {
        bottomSeporatorLine.backgroundColor = UIColor(white: 0.9, alpha: 1.0)
        addSubview(bottomSeporatorLine)
    }
    
    func setupDateAreaSeporatorLine() {
        dateAreaSeporatorLine.backgroundColor = UIColor(white: 0.9, alpha: 1.0)
        addSubview(dateAreaSeporatorLine)
    }
    
    // MARK:- Public
    func prepareWithSchedules(dateString: String, schedules: [ScheduleEntry]) {
        guard let (dayOfWeek, day) = ScheduleDateHelper.componentsOfDateString(dateString: dateString) else { return }
        weekdayLabel.text = dayOfWeek
        dayLabel.text = day
        
        guard let program = schedules.first?.programName, var workout = schedules.first?.workoutTitle else {
            descriptionLabel.attributedText = nil
            return
        }
        
        if schedules.count > 1 {
            let andMore = " and \(schedules.count - 1) other."
            workout.append(andMore)
        }
        
        let titleString = NSMutableAttributedString(string: program + ":\n", attributes: [NSForegroundColorAttributeName:UIColor.brandDarkGray, NSFontAttributeName: UIFont.brandBold(size: 15.0)])
        titleString.append(NSAttributedString(string: workout, attributes: [NSForegroundColorAttributeName: UIColor.brandGray, NSFontAttributeName: UIFont.brandMedium(size: 14.0)]))
        descriptionLabel.attributedText = titleString
    }
    
    func setBottomLineHidden(hidden: Bool) {
        bottomSeporatorLine.isHidden = hidden
    }
    
    // MARK:- Layout
    override func layoutSubviews() {
        super.layoutSubviews()
        let topBottomPadding = (frame.height - kWeekdayLabelHeight - kDayLabelHeight) / 2
        weekdayLabel.frame = CGRect(0.0, topBottomPadding, kDateLabelAreaWidth, kWeekdayLabelHeight)
        dayLabel.frame = CGRect(0.0, topBottomPadding + kWeekdayLabelHeight, kDateLabelAreaWidth, kDayLabelHeight)
        descriptionLabel.frame = CGRect(kDateLabelAreaWidth + 15.0, 0.0, frame.width - kDateLabelAreaWidth - 15.0, frame.height)
        bottomSeporatorLine.frame = CGRect(0.0, frame.height - Environment.thinnestLine, frame.width, Environment.thinnestLine)
        dateAreaSeporatorLine.frame = CGRect(kDateLabelAreaWidth, 0, Environment.thinnestLine, frame.height)
    }
}
