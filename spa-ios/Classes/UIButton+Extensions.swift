//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

extension UIButton {
    
    private class func internalGenerateButton(title: String) -> UIButton {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.brandAccentBlue
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = UIFont.brandMedium(size: 18)
        return button
    }
    
    class func generateAccessoryInputButton(title: String, target: AnyObject, selector: Selector) -> UIButton {
        let button = internalGenerateButton(title: title)
        button.frame = CGRect(0, 0, 0, 45)
        button.addTarget(target, action: selector, for: .touchUpInside)
        return button
    }
    
    class func generateButton(title: String) -> UIButton {
        let button = internalGenerateButton(title: title)
        button.layer.cornerRadius = 4.0
        button.layer.masksToBounds = true
        return button
    }
    
    func configure() {
        backgroundColor = UIColor.brandAccentBlue
        setTitleColor(UIColor.white, for: .normal)
        titleLabel?.font = UIFont.brandMedium(size: 18)
        layer.cornerRadius = 4.0
        layer.masksToBounds = true
    }
}
