//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class ExerciseBreakTimerCardVC: ExerciseBaseCardVC {
    
    var timerShouldResetOnApperance = true
    let timerView: TimerView
    let instructionLabel = UILabel()
    
    init(seconds: Int) {
        self.timerView = TimerView(seconds: seconds, completionText: "Breaks Up!")
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTimerView()
        setupInstructionLabel()
    }
    
    private func setupTimerView() {
        containerView.addSubview(timerView)
    }
    
    private func setupInstructionLabel() {
        instructionLabel.font = UIFont.brandMedium(size: 20.0)
        instructionLabel.textAlignment = .center
        instructionLabel.textColor = UIColor.brandDarkGray
        instructionLabel.numberOfLines = 3
        instructionLabel.text = "Take a quick break, then swipe left to continue."
        containerView.addSubview(instructionLabel)
    }
}

// MARK:- View Lifecycle Control
extension ExerciseBreakTimerCardVC {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if timerShouldResetOnApperance {
            timerView.prepareForReuse()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        ErrorReporter.log("delayedViewDidApper will be added to main queue [outside, method]")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ErrorReporter.log("delayedViewDidAppear will be invoked [outside, block]")
            self.delayedViewDidAppear()
        }
    }
    
    // viewDidAppear: is invoked in a runloop cycle before the view is actaully drawn/finailized, preventing animations from starting.
    // The below workaround simply stalls breifly allowing the view to be drawn first.
    // I'm not happy about this solution, but seeing that viewDidAppear is the final delegate call, I have limited options.
    // Despite the fragile design, it appears to work reliably.
    private func delayedViewDidAppear() {
        ErrorReporter.log("delayedViewDidAppear was invoked [inside] timerview:")
        if timerShouldResetOnApperance {
            ErrorReporter.log("made it inside the if statement")
            timerView.prepareForReuse()
            ErrorReporter.log("made it past prepare")
            timerView.start()
            ErrorReporter.log("made past start")
            timerShouldResetOnApperance = false
        }
        ErrorReporter.log("delayedViewDidAppear was completed [inside] [end]")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timerView.stop()
        timerShouldResetOnApperance = true
    }
}

// MARK:- Layout
extension ExerciseBreakTimerCardVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let circleDiameter = containerView.frame.width - 40.0
        let circleOriginY = (containerView.frame.height * 0.4) - (circleDiameter / 2)
        timerView.frame = CGRect(20, circleOriginY, circleDiameter, circleDiameter)
        instructionLabel.frame = CGRect(15.0, containerView.frame.height - 90.0, containerView.frame.width - 30.0, 90.0)
    }
}
