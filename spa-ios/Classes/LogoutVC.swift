//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

// MARK:- Setup
class LogoutVC: UIViewController {
    
    let session: Session
    let instructionsLabel = UILabel()
    let logoutButton = UIButton.generateButton(title: "Logout")
    
    init(session: Session) {
        self.session = session
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupInstructionsLabel()
        setupLogoutButton()
    }
    
    private func configureView() {
        title = "Logout"
        view.backgroundColor = UIColor.groupTableViewBackground
    }
    
    private func setupInstructionsLabel() {
        instructionsLabel.text = "WARNING:\n\nLogging out will delete any programs you have saved for offline-use, but you can re-downloaded them later."
        instructionsLabel.font = UIFont.brandRegular()
        instructionsLabel.numberOfLines = 0
        instructionsLabel.textAlignment = .center
        instructionsLabel.textColor = UIColor.brandGray
        view.addSubview(instructionsLabel)
    }
    
    private func setupLogoutButton() {
        logoutButton.backgroundColor = UIColor(red: 0.808, green: 0.000, blue: 0.000, alpha: 1.00)
        logoutButton.setTitleColor(UIColor.white, for: .normal)
        logoutButton.addTarget(self, action: #selector(logoutButtonPressed), for: .touchUpInside)
        view.addSubview(logoutButton)
    }
}

// MARK:- Layout
extension LogoutVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let space = CGFloat(20.0)
        let buttonHeight = CGFloat(45.0)
        let instructionLabelWidth = view.frame.width - space * 2
        let instructionLabelHeight = CGFloat(250)
        instructionsLabel.frame = CGRect(space, space, instructionLabelWidth, instructionLabelHeight)
        logoutButton.frame = CGRect(space, view.frame.maxY - space - buttonHeight, view.frame.width - space * 2, buttonHeight)
    }
}

// MARK:- Networking
extension LogoutVC {

    @objc fileprivate func logoutButtonPressed() {
        showLogoutDialog()
    }
    
    private func showLogoutDialog() {
        let logout = ASUserOption.destructiveUserOption(withTitle: "Log Out") {
            self.logout()
        }
        let cancel = ASUserOption.cancel(withTitle: "Cancel", actionBlock: nil)
        ASGlobalOverlay.showAlert(withTitle: "Log Out", message: "Are you sure you want to log out?", userOptions: [cancel as Any, logout as Any])
    }
    
    private func logout() {
        ASGlobalOverlay.showWorkingIndicator(withDescription: nil)
        AuthenticationRequestHandler.logout(user: session.user) { _ in // We log the user out locally reguardless of the request success
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                appDelegate.setRootViewController(withUser: nil)
            }
            SourceTracker.lastKnownSource = nil
            ASGlobalOverlay.dismissWorkingIndicator()
        }
    }
}
