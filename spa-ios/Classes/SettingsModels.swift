//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

struct Height {
    
    enum Unit: String {
        case CM = "CM"
        case IN = "IN"
    }
    
    let unit: Unit
    let value: Int
    var description: String {
        get {
            switch unit {
            case .CM:
                return String(value) + " cm"
            case .IN:
                let inches = value % 12
                let feet  = value / 12
                return String(feet) + " Ft " + String(inches) + " inch"
            }
        }
    }
    var dataString: String {
        return unit.rawValue + String(value)
    }
    
    static func height(string: String) -> Height? {
        if !isValidHeightString(string: string) { return nil }
        guard let unit = Unit(rawValue: string.sub(0, 2)) else { return nil }
        guard let value = Int(string.subToEnd(from: 2)) else { return nil }
        return Height(unit: unit, value: value)
    }
    
    static private func isValidHeightString(string: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^(CM|IN)\\d\\d\\d?$", options: []) // two or three unit values are roughly acceptable, for either unit.
            let match = regex.firstMatch(in: string, options: .anchored, range: NSMakeRange(0, string.characters.count))
            if match == nil { return false }
            return true
        } catch {
            print("ERROR: Could not initialize the height model regular expression")
        }
        return false
    }
}

// MARK:- Substring Helpers
private extension String {
    
    func sub(_ start: Int, _ end: Int) -> String {
        let rangeStartIndex = index(startIndex, offsetBy: start)
        let rangeEndIndex = index(startIndex, offsetBy: end)
        return substring(with: rangeStartIndex..<rangeEndIndex)
    }
    
    func subToEnd(from: Int) -> String {
        let rangeEndIndex = index(startIndex, offsetBy: from)
        return substring(with: rangeEndIndex..<endIndex)
    }
}
