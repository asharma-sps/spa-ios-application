//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

struct Resource {
    let title: String
    let resource: URL
}

struct Product {
    
    enum MediumType: String {
        case digital = "digital"
        case physical = "physical"
    }
    
    enum ProductType: String {
        case supplement = "supplement"
        case program = "program"
        case nutritional = "nutritional"
        case compilation = "compilation"
        case application = "application"
    }
    
    enum AccessType: String {
        case paid = "paid"
        case free = "free"
        case special = "special"
    }
    
    enum PurchasedState: String { // corelates to the "category" key in the JSON
        case paid = "paid"
        case available = "available"
        case supplement = "supplement"
        case free = "free"
    }
    
    let id: Int
    let name: String
    let imageURL: String
    let medium: MediumType
    let type: ProductType
    let access: AccessType
    let purchasedState: PurchasedState
    let resources: [Resource]?
    var sections: [Section]
    let priceInDollars: Int
    let shippable: Bool
    let shippingPrice: Float?
    var totalCost: Float { return (shippingPrice ?? 0.0) + Float(priceInDollars) }
    var totalCostDescription: String { return String(format: "$%.2f", totalCost) }
}

struct Section {
    
    enum ProductType: String {
        case supplement = "supplement"
        case Program = "program"
        case VSL = "vsl" // "video sales letter", basically a promo video
        case Demo = "demo"
    }
    
    let id: Int
    let position: Int
    let name: String
    let type: ProductType
    let workouts: [Workout]
    let contentLocked: Bool
}

enum WorkoutStatus {
    case Workout
    case Other
    case Locked
    case Purchased
}

struct Workout { // A "Workout" may represent any type of video page, including cooking tutorials and promotional videos.
    let id: Int
    let type: String
    let title: String
    let subtitle: String
    let description: String
    let summary: String
    let video: VideoResource
    let cover: String
    let status: WorkoutStatus
    let productID: Int
    let contentLocked: Bool
 //   let shoppingListPDF: URL?
    let PDFResources: [Resource]
}

struct VideoResource {
    let sd: String
    let hd: String
    var preferedResolution: String {
        return SettingsRequestHandler.userVideoQualityIsHD() ? hd : sd
    }
    var urlCachedFrom: String {
        return hd
    }
    
    init(data: [String : AnyObject]) {
        sd = (data["sd"] as? String)?.replacingOccurrences(of: " ", with: "%20") ?? ""
        hd = (data["hd"] as? String)?.replacingOccurrences(of: "", with: "%20") ?? ""
    }
}
