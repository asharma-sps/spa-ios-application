//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class RootTabBarController: UITabBarController {
    
    let session: Session
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask { return UIInterfaceOrientationMask.portrait }
    
    init(user: User) {
        self.session = Session(user: user)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureAppearance()
        initializeViewControllers()
    }
    
    private func configureAppearance(){
        tabBar.isTranslucent = false
        tabBar.tintColor = UIColor.white
        tabBar.barTintColor = UIColor.brandUltraDarkGray
    }
    
    private func initializeViewControllers() {
        
        let tabBarInset = UIEdgeInsets(top: 6.0, left: 0.0, bottom: -6.0, right: 0.0)
        
        let programsVC = ProductsVC(session: session)
        let programsNC = ManagedNavigationController(rootViewController: programsVC)
        programsNC.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "spa_tab_programs_lined"), selectedImage: UIImage(named: "spa_tab_programs_filled"))
        programsNC.tabBarItem.imageInsets = tabBarInset
        
        let scheduleVC = ScheduleVC(session: session)
        let scheduleNC = ManagedNavigationController(rootViewController: scheduleVC)
        scheduleNC.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "spa_tab_calendar_lined"), selectedImage: UIImage(named: "spa_tab_calendar_filled"))
        scheduleNC.tabBarItem.imageInsets = tabBarInset
        
        let progressVC = ProgressVC(session: session)
        let progressNC = ManagedNavigationController(rootViewController: progressVC)
        progressNC.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "spa_tab_progress_lined"), selectedImage: UIImage(named: "spa_tab_progress_filled"))
        progressNC.tabBarItem.imageInsets = tabBarInset

        let settingsVC = SettingsTVC.generateSettingsTableViewController()
        settingsVC.session = session
        let settingsNC = ManagedNavigationController(rootViewController: settingsVC)
        settingsNC.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "spa_tab_settings_lined"), selectedImage: UIImage(named: "spa_tab_settings_filled"))
        settingsNC.tabBarItem.imageInsets = tabBarInset
        
        self.setViewControllers([programsNC, scheduleNC, progressNC, settingsNC], animated: false)
    }
}

