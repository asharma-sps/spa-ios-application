//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2016 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

private let kSecondsInDay = TimeInterval(60 * 60 * 24)

struct SourceTracker { // ASTODO test me
    
    private static var privateLastKnownSource: String?
    private static var lastKnownSourceLastUpdate: Date?

    static var lastKnownSource: String? {
        get {
            guard let updateTime = lastKnownSourceLastUpdate else { return nil }
            guard updateTime.timeIntervalSinceNow < kSecondsInDay else { return nil }
            return privateLastKnownSource
        }
        set {
            privateLastKnownSource = newValue
            lastKnownSourceLastUpdate = (newValue != nil) ? Date() : nil
        }
    }
}
