//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

struct TimeFormattingHelper {
    
    static func formattedTime(seconds: Int, negative: Bool, alwaysShowMinutes: Bool) -> String {
        let sign = negative ? "-" : ""
        let t = timeComponents(forSeconds: seconds)
        
        if t.hours > 0 {
            return "\(sign)\(t.hours):\(t.minutes.tpd):\(t.seconds.tpd)"
        } else if t.minutes > 0 || alwaysShowMinutes {
            return "\(sign)\(t.minutes.tpd):\(t.seconds.tpd)"
        } else {
            return "\(seconds.tpd)"
        }
    }
    
    private static func timeComponents(forSeconds seconds: Int) -> (hours: Int, minutes: Int, seconds: Int) {
        let totalSeconds = abs(seconds)
        let hours = totalSeconds / 3600
        let minutes = (totalSeconds % 3600) / 60
        let seconds = totalSeconds % 60
        return (hours: hours, minutes: minutes, seconds: seconds)
    }
}

private extension Int {
    
    var tpd: String { // "two padding digits"
        return String(format: "%02d", self)
    }
}
