//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

protocol ProductsObserver: class {
    func productsDidReload()
}

class ProductsPool {
    
    let request = ProductsRequestHandler()
    private let observers: NSHashTable = NSHashTable<AnyObject>(options: [.weakMemory])
    private(set) var products: [Product] = [] {
        didSet {
            for observer in observers.allObjects {
                guard let checked = observer as? ProductsObserver else { continue }
                checked.productsDidReload()
            }
        }
    }
    
    func addObserver(_ observer: ProductsObserver) {
        observers.add(observer)
    }
    
    func reloadProducts(user: User) {
        request.requestProducts(user: user) { (success, products, optionalData) in
            if success, products.count > 0, let data = optionalData {
                self.products = products
                ProductsDataCacheHelper.set(data: data)
            } else {
                guard let storedProducts = self.loadSavedProducts() else {
                    self.products = []
                    return
                }
                self.products = storedProducts
            }
        }
    }
    
    private func loadSavedProducts() -> [Product]? {
        guard let data = ProductsDataCacheHelper.load() else { return nil }
        return request.generateProductsWithData(data: data)
    }
}
