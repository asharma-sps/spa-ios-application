//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

extension Error {
    
    func isConnectionIssue() -> Bool {
        let code = (self as NSError).code
        return code == -1009
    }
}
