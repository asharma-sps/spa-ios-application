//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

private let kGeneralPadding = CGFloat(20.0)
private let kButtonHeight = CGFloat(60.0)

class SchedulePickerConfirmationVC: UIViewController {
    
    fileprivate let saveButton = UIButton.generateButton(title: "Confirm and Save")
    fileprivate let descriptionLabel = UILabel()
    fileprivate let startDate: Date
    fileprivate let program: WorkoutProgram
    fileprivate let session: Session
    
    init(session: Session, startDate: Date, program: WorkoutProgram) {
        self.session = session
        self.startDate = startDate
        self.program = program
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupDescriptionLabel()
        setupSaveButton()
    }
    
    private func configureView() {
        view.backgroundColor = UIColor.groupTableViewBackground
        title = "Confirmation"
    }
    
    private func setupDescriptionLabel() {
        let oulineCount = program.outlines.count
        let startDateText = ScheduleDateHelper.userDateString(date: startDate)
        guard let endDate = ScheduleDateHelper.singleton.calendar.date(byAdding: DateComponents(day: oulineCount - 1), to: startDate) else { return } // -1 b/c start date is inclusive
        let endDateText = ScheduleDateHelper.userDateString(date: endDate)
        let labelText = "PROGRAM:\n\(program.name)\n\nSTART DATE:\n\(startDateText)\n\nEND DATE:\n\(endDateText)"
        
        descriptionLabel.text = labelText
        descriptionLabel.textAlignment = .center
        descriptionLabel.font = UIFont.brandMedium()
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textColor = UIColor.brandGray
        view.addSubview(descriptionLabel)
    }
    
    private func setupSaveButton() {
        saveButton.addTarget(self, action: #selector(saveSchedule), for: .touchUpInside)
        view.addSubview(saveButton)
    }
}

// MARK:- Layout
extension SchedulePickerConfirmationVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let width = view.frame.width - kGeneralPadding * 2
        let descriptionSize = descriptionLabel.sizeThatFits(CGSize(width, CGFloat.greatestFiniteMagnitude))
        descriptionLabel.frame = CGRect(kGeneralPadding, 140.0, width, descriptionSize.height)
        saveButton.frame = CGRect(kGeneralPadding, view.frame.height - kButtonHeight - kGeneralPadding, width, kButtonHeight)
    }
}

// MARK:- Save
extension SchedulePickerConfirmationVC {
    
    @objc fileprivate func saveSchedule() {
        ASGlobalOverlay.showWorkingIndicator(withDescription: "Saving")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        var currentDate = startDate
        var entries = [[String: Any]]()
        for outline in program.outlines {
            if let id = outline.id {
                entries.append(["content_id" : id as AnyObject, "date" : dateFormatter.string(from: currentDate) as AnyObject])
            }
            currentDate = ScheduleDateHelper.singleton.calendar.date(byAdding: DateComponents(day: 1), to: currentDate)! // ASTODO safety
        }
        
        ScheduleRequestHandler.setScheduleEntries(user: session.user, entriesDicts: entries) { (success) in
            if success {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kScheduleUpdatedNotificationName), object: nil) // can I improve this? ASTODO
                self.dismissSelf()
                Analytics.incrementProfileAttributeValue(key: .totalNumberOfTimesProgramScheduled)
                ASGlobalOverlay.dismissWorkingIndicator()
            } else {
                ASGlobalOverlay.showAlert(withTitle: "Error", message: ErrorMessages.general, dismissButtonTitle: "OK")
            }
        }
    }
}
