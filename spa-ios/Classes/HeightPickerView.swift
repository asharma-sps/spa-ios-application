//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

private let kMinFeet = 4
private let kMaxFeet = 7
private let kMinCentimeters = 120
private let kMaxCentimeters = 240

protocol HeightPickerViewDelegate: class {
    func heightSelectionDidChange(height: Height)
}

class HeightPickerView: UIPickerView {
    
    weak var selectionDelegate: HeightPickerViewDelegate?
    var currentlySelectedHeight: Height {
        if unit == .IN {
            let inches = (selectedRow(inComponent: 0) + kMinFeet) * 12 + selectedRow(inComponent: 1)
            return Height(unit: .IN, value: inches)
        } else {
            return Height(unit: .CM, value: selectedRow(inComponent: 0) + kMinCentimeters)
        }
    }
    var unit: Height.Unit = Height.Unit.IN {
        didSet {
            reloadAllComponents()
        }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        delegate = self
        dataSource = self
    }
    
    func selectHeight(height: Height) {
        if unit == .IN {
            selectRow(height.value / 12 - kMinFeet, inComponent: 0, animated: false)
            selectRow(height.value % 12, inComponent: 1, animated: false)
        } else {
            selectRow(height.value - kMinCentimeters, inComponent: 0, animated: false)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension HeightPickerView: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return unit == .IN ? 2 : 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            if unit == .IN { return kMaxFeet - kMinFeet + 1}
            else { return kMaxCentimeters - kMinCentimeters + 1}
        }
        return 12 // inches
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch unit {
        case .IN:
            if component == 0 { return String(row + kMinFeet) }
            return String(row) // inches
        case .CM:
            return String(row + kMinCentimeters)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        updateSelectionDelegate()
    }
    
    private func updateSelectionDelegate() {
        selectionDelegate?.heightSelectionDidChange(height: currentlySelectedHeight)
    }
}
