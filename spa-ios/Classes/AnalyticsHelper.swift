//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import Localytics

/*
 * ------------------------------------------------
 * ------ ANALYTICS DOCUMENTATION AND NOTES -------
 * ------------------------------------------------
 *
 * Analytics are setup to "tag" several key actions within the app:
 *
 * SCREEN TAGS:
 *      - Reports a Localytics "Screen Tag," which is used for determing user flow.
 *      - Screen tags can't be used in funnels, and can't contain parameters
 *      - Screen tags are distinct from Event tags (see below).
 *
 * EVENTS TAGS:
 *      - Reports a Localytics "Event Tag," which can be used for funnels, targeted messaging, and general analysis.
 *      - Can include attributes.
 *
 * TIMED EVENT TAGS:
 *      - Reports a Localytics "Event Tag" which contains a duration attribute (and possible other attributes as well)
 *      - This is in-house feature/concept. It doesn't directly correlate to a Localytics feature. 
 *      - In other words, there is no Localytic-side distinction between a normal "Event" tag and a "Timed Event" tag.
 *
 * SIXPACKABS MOBILING ANALYTIC GUIDELINES:
 *      - When we create a Screen tag, we also create a "Timed Event" so we can utilize the data as an event.
 *      - Once again, a "Timed Event" is just a regulard event with an attribute for duration.
 *      - All screen names, event names, and timed event names should be defined as sting-backed enums (see AnalyticConstants.swift)
 */

// MARK:- Initialization
struct Analytics {
    
    fileprivate static var eventStartTimes = [String: Date]()
    
    static func initializeAnalytics() {
        
        // We must manually intergrate Localytics b/c auto-intergrating causes issues with social sign-on services
        // https://docs.localytics.com/dev/ios.html#alternative-initialization-ios
        
        if Environment.isDebugBuild {
            Localytics.integrate("3f6f5c14cfcc998557cebe8-14974bb4-bda3-11e6-db6b-00dba685b405")
            analyticDebugPrint(action: "Localytics was initialized and auto-intergrated for DEBUG Build")
        }
        else {
         //   Localytics.integrate("ad92d01f161e5cd6a9c41f9-81ae96e6-bd9e-11e6-4ce2-00f573af1d63")
       //     analyticDebugPrint(action: "Localytics was initialized and auto-intergrated for RELEASE Build")
        }
    }
}

// MARK:- User Auth Management
extension Analytics {
    
    // Tagging a user sign-in/sign-out/registration log a tag and sets the current user appropriately.
    // Setting the "current user" specifies what user info should be attached to all other tag.
    
    static func tagUserSignedIn(user: User) {
        Localytics.tagCustomerLogged(in: localyticsCustomer(forUser: user), methodName: user.authenticationMethod.rawValue, attributes: nil)
        analyticDebugPrint(action: "signed in", details: "\(user.firstName) (\(user.id)")
    }
    
    static func tagUserRegistered(user: User) {
        Localytics.tagCustomerRegistered(localyticsCustomer(forUser: user), methodName: user.authenticationMethod.rawValue, attributes: nil)
        analyticDebugPrint(action: "registered", details: "\(user.firstName) (\(user.id)")
    }
    
    static func tagUserSignedOut() {
        Localytics.tagCustomerLoggedOut(nil)
        clearCurrentUser()
        analyticDebugPrint(action: "signed out")
    }
    
    static func setCurrentUser(user: User) {
        Localytics.setCustomerId(user.id)
        Localytics.setCustomerFirstName(user.firstName)
        Localytics.setCustomerLastName(user.lastName)
        Localytics.setCustomerFullName("\(user.firstName) \(user.lastName)")
        Localytics.setCustomerEmail(user.email)
        analyticDebugPrint(action: "set current user", details: "\(user.firstName) (\(user.id))")
    }
    
    static func clearCurrentUser() {
        Localytics.setCustomerId(nil)
        Localytics.setCustomerFirstName(nil)
        Localytics.setCustomerLastName(nil)
        Localytics.setCustomerFullName(nil)
        Localytics.setCustomerEmail(nil)
        analyticDebugPrint(action: "user cleared")
    }
}

// MARK:- Profile Attributes
extension Analytics {
    
    static func setProfileAttribute(key: AnalyticAssignedProfileAttribute, value: Any) {
        Localytics.setValue(value as Any, forProfileAttribute: key.rawValue)
        analyticDebugPrint(action: "attribute set", details: "user: \(Localytics.customerId()) value: \(value) key: \(key)")
    }
    
    static func incrementProfileAttributeValue(key: AnalyticIncrementedProfileAttribute) {
        Localytics.incrementValue(by: 1, forProfileAttribute: key.rawValue)
        analyticDebugPrint(action: "attribute ++", details: "\(Localytics.customerId()) key: \(key)")
    }
}

// MARK:- Tagging
extension Analytics {
    
    static func tag(event: AnalyticEventNames, parameters: [String: String]?) {
        Localytics.tagEvent(event.rawValue, attributes: parameters)
        analyticDebugPrint(action: "tagged", details: "\(event.rawValue) \(parameters))")
    }
}

// MARK:- Screen Flow Tracking
extension Analytics {
    
    static func screenDidAppear(screen: AnalyticScreen) {
        tagScreenViewedForFlowTracking(screen: screen)
        startTimingScreenViewingEvent(screen: screen)
    }
    
    static func screenDidDissapear(screen: AnalyticScreen, attributes: [String:String]? = nil) {
        endTimingScreenViewingEventAndTagEvent(screen: screen, attributes: attributes)
    }
}

// MARK:- Screen Tracking Helpers
private extension Analytics {
    
    static func tagScreenViewedForFlowTracking(screen: AnalyticScreen) {
        Localytics.tagScreen(screen.rawValue)
        analyticDebugPrint(action: "screen tagged", details: screen.rawValue)
    }
    
    static func startTimingScreenViewingEvent(screen: AnalyticScreen) {
        eventStartTimes[screen.rawValue] = Date()
        analyticDebugPrint(action: "started timing", details: screen.eventName)
    }
    
    static func endTimingScreenViewingEventAndTagEvent(screen: AnalyticScreen, attributes: [String : String]?) {
        guard let startTime = popStartTime(screen: screen) else {
            Localytics.tagEvent(screen.eventName, attributes: attributes)
            analyticDebugPrint(action: "warning", details: "Could NOT find start time for event: \(screen.rawValue). Tagging event w/o duration property")
            return
        }
        let duration = Int(fabs(startTime.timeIntervalSinceNow))
        var finalAttributes = attributes ?? [String: String]()
        finalAttributes["duration"] = String(duration)
        Localytics.tagEvent(screen.eventName, attributes: finalAttributes)
        analyticDebugPrint(action: "ending timing", details: "\(screen.eventName) \(finalAttributes)")
    }
    
    static func popStartTime(screen: AnalyticScreen) -> Date? {
        guard let startTime = eventStartTimes[screen.rawValue] else { return nil }
        eventStartTimes.removeValue(forKey: screen.rawValue)
        return startTime
    }
}

// MARK:- Localytics Alternative Session Management
extension Analytics {
    
    static func didFinishLaunching(app: UIApplication) {
        guard app.applicationState != .background else { return }
        Localytics.openSession()
    }
    
    static func didBecomeActive() {
        Localytics.openSession()
        Localytics.upload()
    }
    
    static func didEnterBackground() {
        Localytics.dismissCurrentInAppMessage()
        Localytics.closeSession()
        Localytics.upload()
    }
    
    static func willEnterForeground() {
        Localytics.openSession()
        Localytics.upload()
    }
}

// MARK:- Private Helpers
private extension Analytics {
    
    static func localyticsCustomer(forUser user: User) -> LLCustomer? {
        let customer = LLCustomer(){ builder in
            builder.customerId = user.id
            builder.firstName = user.firstName
            builder.lastName = user.lastName
            builder.fullName = "\(user.firstName) \(user.lastName)"
            builder.emailAddress = user.email
        }
        return customer
    }
    
    static func analyticDebugPrint(action: String, details: String = "") {
        // print("[ANALYTICS] \(action.uppercased())\t\t\t\t\t\t\(details)")
    }
}
