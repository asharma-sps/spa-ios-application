//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

protocol CheckoutExpirationDatePickerViewDelegate: class {
    var expirationDate: (month: String, year: String)? { get set }
}

class CheckoutExpirationDatePV: UIPickerView {
    
    let expirationYears = CheckoutExpirationDatePV.generateExpirationYears()
    weak var selectionDelegate: CheckoutExpirationDatePickerViewDelegate?
    
    init(delegate: CheckoutExpirationDatePickerViewDelegate) {
        super.init(frame: CGRect.zero)
        self.delegate = self
        self.dataSource = self
        self.selectionDelegate = delegate
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private class func generateExpirationYears() -> [String] {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yy"
        let yearString = dateFormatter.string(from: Date())
        var yearInt = Int(yearString) ?? 17
        var expirationDateStrings = [String(yearInt)]
        for _ in 0..<10 {
            yearInt += 1
            expirationDateStrings.append(String(yearInt))
        }
        return expirationDateStrings
    }
}

extension CheckoutExpirationDatePV: UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 { return 13 } // 12 months + title
        return expirationYears.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            if row == 0 { return "Month" }
            return String(row)
        } else {
            if row == 0 { return "Year" }
            return expirationYears[row - 1]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if selectedRow(inComponent: 0) == 0 || selectedRow(inComponent: 1) == 0 {
            selectionDelegate?.expirationDate = nil
        } else {
            let month = String(selectedRow(inComponent: 0))
            let year = expirationYears[selectedRow(inComponent: 1) - 1]
            selectionDelegate?.expirationDate = (month: month, year: year)
        }
    }
}
