//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import Alamofire

typealias SettingsCompletion = (Bool) -> ()
private let supportedSettingsKeys = ["height", "video_quality"]

// MARK:- GET
class SettingsRequestHandler {
    
    class func updateSettingsFromServer(user: User, completion: @escaping SettingsCompletion) {
        let request = Router(.getSettings, user)
       Alamofire.request(request)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    guard let resultsArray = value as? [[String: AnyObject]] else {
                        print("ERROR: Settings fetch returned an unexpected type. Expected: NSArray. Received: \(type(of: value))")
                        completion(false)
                        return
                    }
                    unpackAndPersistSettingsData(settings: resultsArray)
                    completion(true)
                case .failure(let error):
                    errorPrint("Failed to fetch settings", error)
                    completion(false)
                }
        }
    }
    
    private class func unpackAndPersistSettingsData(settings: [[String: AnyObject]]) {
        for dict in settings {
            guard let key = dict["name"] as? String, supportedSettingsKeys.contains(key) else { continue }
            UserDefaults.standard.set(dict["value"], forKey: key)
        }
    }
}

// MARK:- POST
extension SettingsRequestHandler {
    
    fileprivate class func updateSetting(user: User, value: String, key: String, completion: @escaping SettingsCompletion) {
        let request = Router(.postSetting(parameters: ["value" : value, "name" : key ]), user)
        Alamofire.request(request)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    UserDefaults.standard.set(value, forKey: key)
                    UserDefaults.standard.synchronize()
                    completion(true)
                case .failure(let error):
                    errorPrint("Failed to print error", error)
                    completion(false)
                }
        }
    }
}

// MARK:- POST
extension SettingsRequestHandler {
    
    func postSupportInquiry(user: User, message: String, subject: String, completion: @escaping SettingsCompletion) {
        let parameters = ["subject" : subject, "message" : message, "email" : user.email]
        let request = Router(.postSupportMessage(parameters: parameters), user)
        Alamofire.request(request)
            .validate()
            .responseJSON() { response in
                switch response.result {
                case .success:
                    completion(true)
                case .failure(let error):
                    responseErrorPrint("Failed to post support inqury.", error, response.data)
                    completion(false)
                }
        }
    }
}

// MARK:- Accessors
extension SettingsRequestHandler {
    
    class func userHeight() -> Height? {
        guard let height = UserDefaults.standard.object(forKey: "height") as? String else { return nil }
        return Height.height(string: height)
    }
    
    class func userVideoQualityIsHD() -> Bool {
        guard let value = UserDefaults.standard.object(forKey: "video_quality") as? String else { return true } // defaults to HD
        return value == "HD"
    }
}

// MARK:- Setters
extension SettingsRequestHandler {
    
    class func setUserHeight(user: User, height: Height, completion: @escaping SettingsCompletion) {
        updateSetting(user: user, value: height.dataString, key: "height", completion: completion)
    }
    
    class func setUserVideoQualityIsHD(user: User, value: Bool, completion: @escaping SettingsCompletion) {
        let stringValue = value ? "HD" : "SD"
        updateSetting(user: user, value: stringValue, key: "video_quality", completion: completion)
    }
}
