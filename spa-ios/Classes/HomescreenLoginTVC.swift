//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//


import UIKit
import ASGlobalOverlay

private let StoryboardName = "HomescreenLoginTVC"

// MARK:- Setup / Outlets
class HomescreenLoginTVC: AnalyticBaseTableViewController {
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask { get { return UIInterfaceOrientationMask.portrait } }
    @IBOutlet weak var emailTextField: FormTextField!
    @IBOutlet weak var passwordTextField: FormTextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    class func generateHomescreenLoginTableViewController() -> HomescreenLoginTVC{
        let storyboard = UIStoryboard(name: StoryboardName, bundle: nil)
        let tvc = storyboard.instantiateInitialViewController() as? HomescreenLoginTVC
        return tvc!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureTextFields()
        #if DEBUG
            addEasySignOnFunction()
        #endif
    }
    
    private func configureView() {
        addCloseButton()
        setupBlankBackButton()
        loginButton.configure()
    }
    
    private func configureTextFields() {
        emailTextField.returnKeyType = .next
        emailTextField.delegate = self
        emailTextField.inputAccessoryView = nil
        passwordTextField.delegate = self
        passwordTextField.inputAccessoryView = nil
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        login()
    }
}

// MARK:- Logins
extension HomescreenLoginTVC {
    
    fileprivate func login() {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        guard let email = emailTextField.validatedString() else { return }
        guard let password = passwordTextField.validatedString() else { return }
        ASGlobalOverlay.showWorkingIndicator(withDescription: nil)
        
        AuthenticationRequestHandler.login(email: email, password: password) { result in
            switch result {
            case .success(let user):
                self.dismiss(animated: false, completion: nil) // prevents over retention, but I can't figure out why :(
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    appDelegate.setRootViewController(withUser: user)
                }
                ASGlobalOverlay.dismissWorkingIndicator()
            case .failure(let errorMessage):
                ASGlobalOverlay.showAlert(withTitle: "Error", message: errorMessage, dismissButtonTitle: "OK")
            }
        }
    }
}

// MARK:- View Management
extension HomescreenLoginTVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            passwordTextField.resignFirstResponder()
            login()
        }
        return true
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    override func dismissSelf() {
        super.dismissSelf()
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
}

// MARK:- Analytics
extension HomescreenLoginTVC {
    
    override func analyticsViewDidAppear() {
        Analytics.screenDidAppear(screen: .login)
    }
    
    override func analyticsViewDidDisappear() {
        Analytics.screenDidDissapear(screen: .login)
    }
}

// MARK:- DEBUG UTILTIES
#if DEBUG
    extension HomescreenLoginTVC {
        
        fileprivate func addEasySignOnFunction() {
            let easySignOn = UIButton(frame: CGRect(10, 300, view.frame.size.width - 20, 45))
            easySignOn.addTarget(self, action: #selector(showEasySignOnMenu), for: .touchUpInside)
            easySignOn.setTitle("DEBUG MODE. QA PLEASE FAIL.", for: .normal)
            easySignOn.backgroundColor = UIColor.brandUltraDarkGray
            view.addSubview(easySignOn)
        }
        
        @objc private func showEasySignOnMenu() {
            guard Environment.isDebugBuild else {
                abort()
            }
            let personal = ASUserOption(title: "amitsharma@mac.com", actionBlock: {
                self.emailTextField.text = "amitsharma@mac.com"
                self.passwordTextField.text = "train"
            })
            let test = ASUserOption(title: "test", actionBlock: {
                self.emailTextField.text = "amittest1@sixpackshortcuts.com"
                self.passwordTextField.text = "password"
            })
            let all = ASUserOption(title: "all access", actionBlock: {
                self.emailTextField.text = "spaqa5@sixpackshortcuts.com"
                self.passwordTextField.text = "65gtcuui"
            })
            let store = ASUserOption(title: "store", actionBlock: {
                self.emailTextField.text = "mscott@sixpackshortcuts.com"
                self.passwordTextField.text = "password"
            })
            ASGlobalOverlay.showSlideUpMenu(withPrompt: "Pick Credentials", userOptions: [personal!, test!, all!, store!])
        }
    }
#endif

