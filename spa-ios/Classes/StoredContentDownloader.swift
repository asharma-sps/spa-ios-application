//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import Alamofire

private typealias ExerciseBlockDataTupple = (data: Data, exerciseBlocks: [ExerciseBlock], workoutID: Int)

// MARK:- Properties & Public
class StoredContentDownloader {
    
    fileprivate let session: Session
    fileprivate let resourceHelper: StoredContentResourceHelper
    fileprivate let requestHandler = ExerciseRequestHandler()
    var cancelWhenPossible = false
    
    enum State {
        case preparing
        case downloadingVideos(total: Int, current: Int)
    }
    
    init(session: Session, productID: Int) {
        self.session = session
        self.resourceHelper = StoredContentResourceHelper(productID: productID)
    }
    
    func saveContentForOfflineUse(product: Product, progress: @escaping (StoredContentDownloader.State) -> (), completion: @escaping (Bool) -> ()) {
        progress(.preparing)
        downloadExerciseDataForProduct(product: product, progress: progress, completion: completion)
    }
}

// MARK:- Sequence Control
extension StoredContentDownloader {
    
    fileprivate func downloadExerciseDataForProduct(product: Product, progress: @escaping (StoredContentDownloader.State) -> (), completion: @escaping (Bool) -> ()) {
        guard resourceHelper.attemptToEnsureProductDirectoryExist() else {
            resourceHelper.deleteProductStoredContent()
            completion(false)
            return
        }
        
        downloadExerciseBlocks(product: product) { optionalExerciseBlockDataTupples in
            guard let exerciseBlockDataTupples = optionalExerciseBlockDataTupples, self.saveWorkoutData(exerciseBlockTupples: exerciseBlockDataTupples) else {
                self.resourceHelper.deleteProductStoredContent()
                completion(false)
                return
            }
            var exerciseBlocks = [ExerciseBlock]()
            for tupple in exerciseBlockDataTupples {
                for block in tupple.exerciseBlocks {
                    exerciseBlocks.append(block)
                }
            }
            self.downloadVideosForExerciseBlocks(blocks: exerciseBlocks, progress: progress, completion: completion)
        }
    }
}

// MARK:- Download Exercise Block Data
extension StoredContentDownloader {
    
    fileprivate func downloadExerciseBlocks(product: Product, completion: @escaping ([ExerciseBlockDataTupple]?) -> ()) {
        let workouts = extractWorkoutsWithExercises(product: product)
        recursivelyRequestExerciseData(workouts: workouts, exerciseData: [], completion: completion)
    }
    
    private func recursivelyRequestExerciseData(workouts: [Workout], exerciseData: [ExerciseBlockDataTupple], completion: @escaping ([ExerciseBlockDataTupple]?) -> ()) {
        if cancelWhenPossible { completion(nil); return }

        var workouts = workouts
        var exerciseData = exerciseData
        guard let workout = workouts.popLast() else { completion(exerciseData); return }
        
        requestHandler.requestExercisesData(user: session.user, workoutID: workout.id) { success, data in
            guard let unwrappedData = data, let exerciseBlocks = self.requestHandler.generateExerciseBlocks(data: unwrappedData) else { completion(nil); return }
            exerciseData.append((data: unwrappedData, exerciseBlocks: exerciseBlocks, workoutID: workout.id))
            self.recursivelyRequestExerciseData(workouts: workouts, exerciseData: exerciseData, completion: completion)
        }
    }
    
    private func extractWorkoutsWithExercises(product: Product) -> [Workout] {
        var workouts = [Workout]()
        for section in product.sections {
            for workout in section.workouts {
                if workout.status == .Workout {
                    workouts.append(workout)
                }
            }
        }
        return workouts
    }
}

// MARK:- Download Exercise Videos
extension StoredContentDownloader {
    
    fileprivate func downloadVideosForExerciseBlocks(blocks: [ExerciseBlock], progress: @escaping (StoredContentDownloader.State) -> (), completion: @escaping (Bool) -> ()) {
        let exerciseVideoResources = generateExerciseVideoURLs(exerciseBlocks: blocks)
        recursivelyDownloadExerciseBlockVideos(urls: exerciseVideoResources, totalCount: exerciseVideoResources.count, progress: progress, completion: completion)
    }
    
    private func generateExerciseVideoURLs(exerciseBlocks: [ExerciseBlock]) -> [URL] {
        var videoURLs = Set<URL>()
        for block in exerciseBlocks {
            for exercise in block.exercises {
                let url = URL(string: exercise.video.hd) ?? URL(string: "https://d36b0b7908f188a06eb4-0a7712e18cf18e72ced319502005802e.ssl.cf1.rackcdn.com/assets/ComingSoon.mp4")!
                videoURLs.insert(url)
            }
        }
        return Array(videoURLs)
    }
    
    private func recursivelyDownloadExerciseBlockVideos(urls: [URL], totalCount:Int, progress: @escaping (StoredContentDownloader.State) -> (), completion: @escaping (Bool) -> ()) {
        if cancelWhenPossible {
            resourceHelper.deleteProductStoredContent()
            completion(false);
            return
        }
        
        var urls = urls
        guard let url = urls.popLast() else {
            completion(true); return
        }
        progress(.downloadingVideos(total: totalCount, current: totalCount - urls.count))
        
        let savePath = resourceHelper.filePathForVideoResource(remoteURL: url)
        downloadVideo(resourceURL: url, destinationFilePath: savePath) { data in
            guard data != nil else {
                self.resourceHelper.deleteProductStoredContent()
                completion(false)
                return
            }
            self.recursivelyDownloadExerciseBlockVideos(urls: urls, totalCount: totalCount, progress: progress, completion: completion)
        }
    }
}

// MARK:- Exercise Block Data Saving
extension StoredContentDownloader {
    
    fileprivate func saveWorkoutData(exerciseBlockTupples: [ExerciseBlockDataTupple]) -> Bool {
        for tupple in exerciseBlockTupples {
            let filePath = resourceHelper.filePathForExerciseBlocksData(workoutID: tupple.workoutID)
            do {
                try tupple.data.write(to: filePath)
            } catch {
                errorPrint("Failed to save data (\(tupple.data)) to path \(filePath)", error)
                return false
            }
        }
        return true
    }
}

// MARK:- Video Download Helper
extension StoredContentDownloader {
    
    fileprivate func downloadVideo(resourceURL: URL, destinationFilePath: URL, completion: @escaping (Data?) -> ()) {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (destinationURL: destinationFilePath, options: [.removePreviousFile])
        }
        
        Alamofire.download(resourceURL, to: destination)
            .responseData { response in
                switch response.result {
                case .success(let data):
                    completion(data)
                case .failure(let error):
                    errorPrint("Failed to download video for offline use.", error)
                    completion(nil)
                }
        }
    }
}
