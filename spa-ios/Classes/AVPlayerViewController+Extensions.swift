//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import AVFoundation
import AVKit

extension AVPlayerViewController {
    
    func configureForLoopingVideo() {
        allowsPictureInPicturePlayback = false
        showsPlaybackControls = false
        view.isUserInteractionEnabled = false
        videoGravity = AVLayerVideoGravityResizeAspectFill
        if #available(iOS 10.0, *) { updatesNowPlayingInfoCenter = false }
    }
}
