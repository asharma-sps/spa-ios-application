//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import Alamofire

// MARK:- Error Type
enum RouterError: Error {
    case url
    case request
    case parameters
}

private let kBaseURL = "https://api.sixpackshortcuts.com/api/v5"

typealias RouterParameters = [String : Any]

// MARK:- Public Router
struct PublicRouter: URLRequestConvertible {
    
    private let route: PublicRoute
    
    init(_ route: PublicRoute) {
        self.route = route
    }
    
    func asURLRequest() throws -> URLRequest {
        switch route {
        case .login(let parameters):
            return try RequestMint.mint(path: "/login", method: .post, parameters: parameters)
        case .logout(let parameters):
            return try RequestMint.mint(path: "/logout", method: .post, parameters: parameters)
        case .register(let parameters):
            return try RequestMint.mint(path: "/register", method: .post, parameters: parameters)
        case .resetPassword(let resetEmail):
            return try RequestMint.mint(path: "/forgot-password", method: .post, parameters: ["email" : resetEmail])
        }
    }
}

enum PublicRoute {
    case login(parameters: RouterParameters)
    case logout(parameters: RouterParameters)
    case register(parameters: RouterParameters)
    case resetPassword(resetEmail: String)
}

// MARK:- Authenticated Router
struct Router: URLRequestConvertible {
    
    let route: Route
    let email: String
    let token: String
    
    init(_ route: Route,_ user: User) {
        self.route = route
        self.email = user.email
        self.token = "remember_token=\(user.token)"
    }
    
    func asURLRequest() throws -> URLRequest {
        switch route {
        case .getProducts:
            return try RequestMint.mint(path: "/items?\(token)&refresh=true", method: .get, parameters: nil)
        case .getBlocks(let workoutID):
            return try RequestMint.mint(path: "/content/\(workoutID)/routine?\(token)", method: .get, parameters: nil)
        case .postCheckout(let parameters):
            return try RequestMint.mint(path: "/payment?\(token)", method: .post, parameters: parameters)
        case .getProgress:
            return try RequestMint.mint(path: "/user/\(email)/progress?\(token)", method: .get, parameters: nil)
        case .postProgress:
            return try RequestMint.mint(path: "/user/\(email)/progress?\(token)", method: .post, parameters: nil)
        case .deleteProgress(let id):
            return try RequestMint.mint(path: "/user/\(email)/progress/\(id)/?\(token)", method: .delete, parameters: nil)
        case .getSettings:
            return try RequestMint.mint(path: "/user/\(email)/configuration?\(token)", method: .get, parameters: nil)
        case .postSetting(let parameters):
            return try RequestMint.mint(path: "/user/\(email)/configuration?\(token)", method: .post, parameters: parameters)
        case .postSupportMessage(let parameters):
            return try RequestMint.mint(path: "/support?\(token)", method: .post, parameters: parameters)
        case .getSchedulablePrograms:
            return try RequestMint.mint(path: "/user/\(email)/programs?\(token)", method: .get, parameters: nil)
        case .getScheduleEntries:
            return try RequestMint.mint(path: "/user/\(email)/schedule?\(token)", method: .get, parameters: nil)
        case .deleteSchedule:
            return try RequestMint.mint(path: "/user/\(email)/schedule?\(token)", method: .delete, parameters: nil)
        case .postSchedule(let parametersArray):
            var request = try! RequestMint.mint(path: "/user/\(email)/schedule?\(token)", method: .post, parameters: nil)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            guard let encodedData = try? JSONSerialization.data(withJSONObject: parametersArray, options:[]) else { throw RouterError.parameters }
            request.httpBody = encodedData
            return request
        }
    }
}

enum Route {
    case getProducts
    case getBlocks(workoutID: Int)
    case postCheckout(parameters: RouterParameters)
    case getProgress
    case postProgress
    case deleteProgress(id: String)
    case getSettings
    case postSetting(parameters: RouterParameters)
    case postSupportMessage(parameters: RouterParameters)
    case getSchedulablePrograms
    case getScheduleEntries
    case deleteSchedule
    case postSchedule(entries: [[String: Any]])
}

// MARK:- Request Creation
private struct RequestMint {
    
    static func mint(path: String, method: Alamofire.HTTPMethod, parameters: [String: Any]? ) throws -> URLRequest {
        guard let url = URL(string:"\(kBaseURL)\(path)") else { throw RouterError.url }
        guard let request = try? URLRequest(url: url, method: method) else { throw RouterError.request }
        if parameters != nil {
            guard let requestWithParameters = try? URLEncoding.methodDependent.encode(request, with: parameters) else { throw RouterError.parameters }
            print(requestWithParameters)
            return requestWithParameters
        }
        print("[REQUEST]: \(request)")
        return request
    }
}
