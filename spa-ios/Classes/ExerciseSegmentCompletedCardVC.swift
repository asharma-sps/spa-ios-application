//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class ExerciseSegmentCompleteCardVC: ExerciseBaseCardVC {
    
    let completeCardContentView = ExerciseSegmentCompletedView.generateExerciseSegmentCompletedView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupContentView()
    }
    
    private func setupContentView() {
        containerView.addSubview(completeCardContentView)
    }
    
    func configureLabels(currentSet: Int, totalSets: Int) {
        completeCardContentView.titleLabel.text = "Set Complete!"
        completeCardContentView.subtextLabel.text = "Swipe left to continue onto set \(currentSet + 1) of \(totalSets)"
    }
    
    func configureForCompletedWorkout() {
        completeCardContentView.titleLabel.text = "Workout Complete!"
        completeCardContentView.subtextLabel.text = "Great job! Tap the close button in the top left corner to close this view."
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        completeCardContentView.frame = CGRect(0.0, 0.0, containerView.frame.width, containerView.frame.height)
    }
}
