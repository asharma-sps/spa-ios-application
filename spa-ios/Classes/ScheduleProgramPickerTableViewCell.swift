//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class ScheduleProgramPickerTableViewCell: UITableViewCell {
    
    let scheduledIcon = UIImageView(image: UIImage(named: "spa_scheduled_icon"))
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        textLabel?.font = UIFont.brandMedium(size: 18)
        textLabel?.textColor = UIColor.brandDarkGray
        detailTextLabel?.font = UIFont.brandRegular(size: 14)
        detailTextLabel?.textColor = UIColor.brandLightGray
        scheduledIcon.contentMode = .scaleAspectFit
        addSubview(scheduledIcon)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        scheduledIcon.frame = CGRect(frame.width - 42.0, frame.height / 2 - 16, 32.0, 32.0)
    }
    
    func prepare(programTupple: (program: WorkoutProgram, scheduled: Bool)) {
        textLabel?.text = programTupple.program.name
        detailTextLabel?.text = "Approximately \(programTupple.program.outlines.count / 7) weeks"
        scheduledIcon.isHidden = !programTupple.scheduled
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
