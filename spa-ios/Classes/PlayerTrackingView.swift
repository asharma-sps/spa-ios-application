//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import AVFoundation

private let kPlayerTrackingViewHeight = CGFloat(2.0)
private let kDotBackingSquareDimension = CGFloat(40.0)
private let kDotDiameter = CGFloat(10.0)

// MARK:- Delegate
protocol PlayerTrackingViewDelegate: class {
    func trackingPercentageCompleteDidChange(percent: Double)
    func scrubbingStateDidChanged(isScrubbing: Bool)
}

// MARK:- Setup
class PlayerTrackingView: UIView {
    
    fileprivate let barBacking = UIView()
    fileprivate let barFill = UIView()
    fileprivate let barDot = DotView()
    weak var delegate: PlayerTrackingViewDelegate?
    var percent: Double = 0.0 {
        didSet {
            if !percent.isNormal { percent = 0.0 }
            setNeedsLayout()
        }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        setupBarBacking()
        setupBarFill()
        setupBarDot()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    private func setupBarBacking() {
        barBacking.layer.cornerRadius = kPlayerTrackingViewHeight / 2
        barBacking.backgroundColor = UIColor.init(white: 1.0, alpha: 0.5)
        barBacking.layer.masksToBounds = true
        addSubview(barBacking)
    }
    
    private func setupBarFill() {
        barFill.backgroundColor = UIColor.brandAccentBlue
        barBacking.addSubview(barFill)
    }
    
    private func setupBarDot() {
        barDot.layer.masksToBounds = true
        barDot.layer.cornerRadius = kDotDiameter / 2
        barDot.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognized)))
        addSubview(barDot)
    }
    
    @objc func panGestureRecognized(sender: UIPanGestureRecognizer) {
        
        if sender.state == .began {
            delegate?.scrubbingStateDidChanged(isScrubbing: true)
        } else if sender.state == .ended || sender.state == .cancelled {
            delegate?.scrubbingStateDidChanged(isScrubbing: false)
        }
        
        let relativePositionX = min(max(sender.location(in: self).x, 0), frame.width)
        percent = Double(relativePositionX / frame.width)
        delegate?.trackingPercentageCompleteDidChange(percent: percent)
    }
}

// MARK:- Layout
extension PlayerTrackingView {

    override func layoutSubviews() {
        super.layoutSubviews()
        let barY = CGFloat((frame.height - kPlayerTrackingViewHeight) / 2)
        barBacking.frame = CGRect(0.0, barY, frame.width, kPlayerTrackingViewHeight)
        barFill.frame = CGRect(0.0, 0.0, frame.width * CGFloat(percent), kPlayerTrackingViewHeight)
        
        let dotX = CGFloat((frame.width * CGFloat(percent) - (kDotBackingSquareDimension / 2)))
        let dotY = CGFloat((frame.height - kDotBackingSquareDimension) / 2)
        barDot.frame = CGRect(dotX, dotY, kDotBackingSquareDimension, kDotBackingSquareDimension)
    }
}

// MARK:- Dot Gesture Detection Area Expansion
fileprivate class DotView: UIView {
    
    private let dot = UIView()
    
    init() {
        super.init(frame: CGRect.zero)
        dot.backgroundColor = UIColor.white
        dot.layer.masksToBounds = true
        dot.layer.cornerRadius = kDotDiameter / 2
        addSubview(dot)
        backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate override func layoutSubviews() {
        super.layoutSubviews()
        dot.frame = CGRect((frame.width - kDotDiameter) / 2, (frame.height - kDotDiameter) / 2, kDotDiameter, kDotDiameter)
    }
}
