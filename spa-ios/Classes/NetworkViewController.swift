//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//
import UIKit

enum NetworkViewControllerState {
    case mainView
    case loadingView
    case failureView
}

class NetworkViewController: AnalyticBaseViewController {

    let mainView = UIView()
    let loadingView = BaseLoadingView()
    let failureView = BaseFailureView()
    var baseViewControllerState = NetworkViewControllerState.loadingView {
        didSet {
            switch baseViewControllerState {
            case .mainView:
                mainView.isHidden = false
                failureView.isHidden = true
                loadingView.isHidden = true
            case .loadingView:
                loadingView.isHidden = false
                failureView.isHidden = true
                mainView.isHidden = true
            case .failureView:
                failureView.isHidden = false
                mainView.isHidden = true
                loadingView.isHidden = true
            }
            baseViewControllerState == .loadingView ? loadingView.spinner.startAnimating() : loadingView.spinner.stopAnimating()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    func setupViews() {
        failureView.delegate = self
        let views = [loadingView, mainView, failureView]
        for view in views {
            view.backgroundColor = UIColor.groupTableViewBackground
            self.view.addSubview(view)
        }
        baseViewControllerState = .loadingView
    }
}

// MARK:- Failure View Management
extension NetworkViewController: BaseFailureViewDelegate{
    
    func retryLoadingData() {
        print("Override Me!")
    }
}

// MARK:- Layout
extension NetworkViewController {
    
    override func viewDidLayoutSubviews() {
        let frame = CGRect(0.0, 0.0, view.bounds.width, view.bounds.height)
        mainView.frame = frame
        loadingView.frame = frame
        failureView.frame = frame
    }
}
