//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import KeychainAccess
import Crashlytics
import Fabric
import FacebookCore
import FacebookLogin
import Branch

fileprivate let kAppHasLaunchedBeforeUserDefaultKey = "spa_not_first_launch"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    // static let shared = UIApplication.shared.delegate as! AppDelegate
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        setupCrashReporting()
        tempBranchSetup(launchOptions: launchOptions)
        ErrorReporter.set(state: "Crashlytics on. Routine will be invoked")
        runLaunchRoutine()
        ErrorReporter.set(state: "Crashlytics on. Routine completed")
        Analytics.didFinishLaunching(app: application) // must run after launch routine
        return true
    }
    
    private func tempBranchSetup(launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        let branch = Branch.getInstance()
        branch?.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: { params, error in
            print("grab-here-spa: IN")
            if let unwrappedError = error {
                ErrorReporter.report(error: unwrappedError, message: "Failed to setup branch.io")
                return
            }
            guard let parameters = params, let source = parameters["attr_source"] as? String else { return }
            SourceTracker.lastKnownSource = source
            print("grab-here-spa: \(source)")
            print("params: %@", params?.description ?? "no branch params")
        })
    }
}

// MARK:- Launch Routine
extension AppDelegate {
    
    fileprivate func runLaunchRoutine() {
        print("\n\n[STARTING LAUNCH ROUTINE] \(Environment.summary)\n\n")
//        setupCrashReporting() temp moved out of method for debugging.
        ErrorReporter.set(state: "Routine invoked")
        clearKeychainIfFirstLaunch()
        ErrorReporter.set(state: "Routine - 1")
        setupAnalytics()
        ErrorReporter.set(state: "Routine - 2")
        setupGoogleSignIn()
        ErrorReporter.set(state: "Routine - 3")
        customizeNavigationBarAppearance()
        ErrorReporter.set(state: "Routine - 4")
        configureAudioSessionCategory()
        ErrorReporter.set(state: "Routine - 5")
        setupImageCacheDirectory()
        ErrorReporter.set(state: "Routine - 6")
        excludeDocumentDirectoryFromBackup()
        ErrorReporter.set(state: "Routine - 7")
        assignInitialRootViewController()
        ErrorReporter.set(state: "Routine - 8")
        print("\n\n[FINISHED LAUNCH ROUTINE]\n\n")
    }
    
    fileprivate func setupCrashReporting() {
        if !Environment.isDebugBuild {
            Fabric.with([Crashlytics.self])
        }
    }
    
    private func clearKeychainIfFirstLaunch() {
        if UserDefaults.standard.object(forKey: kAppHasLaunchedBeforeUserDefaultKey) == nil {
            print("First launch, clearing user resources, including the keychain.")
            UserResourceManager.cleanupUserResources()
            UserDefaults.standard.setValue("app_has_launched_before", forKey: kAppHasLaunchedBeforeUserDefaultKey)
        }
    }
    
    private func setupAnalytics() {
        Analytics.initializeAnalytics()
        guard let user = AuthenticatedUserPersistanceManager.load() else { return }
        Analytics.setCurrentUser(user: user)
    }
    
    private func setupGoogleSignIn() {
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        GIDSignIn.sharedInstance().delegate = GoogleAuthDelegate.singleton
    }
    
    private func customizeNavigationBarAppearance() {
        UIApplication.shared.statusBarStyle = .lightContent
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = UIColor.brandAccentBlue
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white, NSFontAttributeName : UIFont.brandHeavy()]
    }
    
    private func configureAudioSessionCategory(){
        AudioHelper.configureAudioSessionCategory()
    }
    
    private func setupImageCacheDirectory() {
        NetworkImageView.ensureImageDiskCacheDirectoryExist()
    }
    
    private func excludeDocumentDirectoryFromBackup() { // shouldn't have to do this... ASTODO
        NetworkImageView.ensureImageDiskCacheDirectoryExist()
        var filePath = URL(fileURLWithPath: URL.documentDirectoryPath().absoluteString, isDirectory: false, relativeTo: nil)
        do {
            var resourceValues = URLResourceValues()
            resourceValues.isExcludedFromBackup = true
            try filePath.setResourceValues(resourceValues)
            print("Image disk cache has been excluded from iCloud backup successfully")
        } catch let error as NSError {
            print("ERROR: Failed te exclude \(filePath.lastPathComponent) from backup: \(error)");
        }
    }

    private func assignInitialRootViewController() {
        let user = AuthenticatedUserPersistanceManager.load()
        setRootViewController(withUser: user)
    }
}

// MARK:- Open URL
extension AppDelegate {
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        // Branch
        Branch.getInstance().handleDeepLink(url);
        
        // Facebook
        if SDKApplicationDelegate.shared.application(app, open: url, options: options) {
            return true
        }
        
        // Google
        let sourceApp = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
        let annotation = options[UIApplicationOpenURLOptionsKey.annotation] as? String
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApp, annotation: annotation)
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        Branch.getInstance().continue(userActivity)
        return true
    }
}

// MARK:- Localytics Alternative Session Management
extension AppDelegate {
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        Analytics.didBecomeActive()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        Analytics.willEnterForeground()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        Analytics.didEnterBackground()
    }
}

// MARK:- Root View Controller Management
extension AppDelegate {
    
    func setRootViewController(withUser optionalUser: User?) {
        if let user = optionalUser {
            AuthenticatedUserPersistanceManager.set(user: user)
            setRootViewController(vc: RootTabBarController(user: user))
        } else {
            UserResourceManager.cleanupUserResources()
            setRootViewController(vc: HomescreenVC())
        }
    }
    
    private func setRootViewController(vc: UIViewController) {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
    }
}
