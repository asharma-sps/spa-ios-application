//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class ExerciseInstructionsCardVC: ExerciseBaseCardVC {
    
    let swipeView = SwipeView()
    let startContentView = Bundle.main.loadNibNamed("ExerciseInstructionsView", owner: nil, options: nil)?[0] as? UIView ?? UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupStartContentView()
        setupSwipeView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        swipeView.startAnimating()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        swipeView.stopAnimating()
    }
    
    private func setupStartContentView() {
        containerView.addSubview(startContentView)
    }
    
    private func setupSwipeView() {
        containerView.addSubview(swipeView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        startContentView.frame = CGRect(0.0, 0.0, containerView.frame.width, containerView.frame.height)
        swipeView.frame = CGRect((containerView.frame.width - SwipeView.requiredSize.width) / 2,
                                 containerView.frame.height - 15 - SwipeView.requiredSize.height,
                                 SwipeView.requiredSize.width,
                                 SwipeView.requiredSize.height)
    }
}
