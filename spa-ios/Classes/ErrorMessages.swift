//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

struct ErrorMessages {
    static let general = "An error occurred. Please check your network settings and try again."
    static let inconsistancy = "An internal issue occurred. Please try again or contact support."
    static let connection = "We could not reach our servers. Please check your network settings and try again."
}
