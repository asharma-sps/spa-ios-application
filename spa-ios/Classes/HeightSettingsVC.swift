//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

// MARK:- Setup
class HeightSettingsVC: UIViewController {
    
    let textField = CaretlessTextField()
    let picker = HeightPickerView()
    let unitToggleButton = UIButton(type: .system)
    let session: Session
    var unit = SettingsRequestHandler.userHeight()?.unit ?? Height.Unit.IN {
        didSet {
            picker.unit = unit
            textField.text = picker.currentlySelectedHeight.description
        }
    }
    
    init(session: Session) {
        self.session = session
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupTextFieldAndPicker()
        setupUnitToggleButton()
    }
    
    private func configureView() {
        view.backgroundColor = UIColor.groupTableViewBackground
        title = "Height"
    }
    
    private func setupTextFieldAndPicker() {
        picker.selectionDelegate = self
        picker.unit = unit
        if let height = SettingsRequestHandler.userHeight() {
            picker.selectHeight(height: height)
        }
        
        textField.inputView = picker
        textField.text = SettingsRequestHandler.userHeight()?.description ?? "--"
        textField.backgroundColor = UIColor.white
        textField.borderStyle = .none
        textField.layer.cornerRadius = 3.0
        textField.layer.masksToBounds = true
        textField.textAlignment = .center
        textField.inputAssistantItem.leadingBarButtonGroups = []
        textField.inputAssistantItem.trailingBarButtonGroups = []
        textField.inputAccessoryView = UIButton.generateAccessoryInputButton(title: "Save", target: self, selector: #selector(saveButtonPressed))
        view.addSubview(textField)
    }
    
    private func setupUnitToggleButton() {
        unitToggleButton.addTarget(self, action: #selector(toggleUnits), for: .touchUpInside)
        setToggleUnitButtonTitleForCurrentUnit()
        view.addSubview(unitToggleButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        textField.becomeFirstResponder()
    }
}

// MARK:- HeightPickerViewDelegate
extension HeightSettingsVC: HeightPickerViewDelegate {
    
    func heightSelectionDidChange(height: Height) {
        textField.text = height.description
    }
}

// MARK:- Label & Button Management
extension HeightSettingsVC {
    
    @objc fileprivate func toggleUnits() {
        unit = unit == .IN ? .CM : .IN
        picker.reloadAllComponents()
        setToggleUnitButtonTitleForCurrentUnit()
    }

    func setToggleUnitButtonTitleForCurrentUnit() {
        let title = unit == .IN ? "Switch to centimeters" : "Switch to ft/inches"
        self.unitToggleButton.setTitle(title, for: .normal)
    }
}

// MARK:- Save
extension HeightSettingsVC {
    
    @objc fileprivate func saveButtonPressed() {
        ASGlobalOverlay.showWorkingIndicator(withDescription: nil)
        textField.resignFirstResponder()
        SettingsRequestHandler.setUserHeight(user: session.user, height: picker.currentlySelectedHeight) { (success) in
            if success {
                ASGlobalOverlay.dismissWorkingIndicator()
                _ = self.navigationController?.popViewController(animated: true)
            } else {
                let option = ASUserOption.init(title: "OK", actionBlock: {
                    self.textField.becomeFirstResponder()
                })
                ASGlobalOverlay.showAlert(withTitle: "Error", message: ErrorMessages.general , userOptions: [option as Any])
            }
        }
    }
}

// MARK:- Layout
extension HeightSettingsVC {

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let bounds = view.bounds
        textField.frame = CGRect(20.0, 60.0, bounds.width - 40.0, 45.0)
        unitToggleButton.frame = CGRect((bounds.width - 200.0) / 2, 120.0, 200.0, 50.0)
    }
}
