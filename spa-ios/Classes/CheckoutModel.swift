//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation


// MARK:- Models
struct OrderPacket {
    
    enum CardType: String {
        case Amex = "amex"
        case Discover = "discover"
        case Visa = "visa"
        case Mastercard = "mastercard"
        static func cardTypeForNumber(firstDigit: String?) -> CardType? {
            guard let unwrapped = firstDigit else { return nil }
            switch unwrapped {
            case "3": return Amex
            case "4": return Visa
            case "5": return Mastercard
            case "6": return Discover
            default: return nil
            }
        }
    }
    
    let productID: Int
    let phone: String
    let phoneCountry: String
    let creditCardString: String
    let cardType: CardType
    let expirationMonth: String
    let expirationYear: String
    let cvv: String
    let billing: Address
    let shipping: Address?
}

struct Address {
    let firstName: String
    let lastName: String
    let street: String
    let city: String
    let state: String
    let country: String
    let zipCode: String
    var description: String {
            return "\(street)\n\(city), \(state). \(zipCode)\n\(country)"
    }
}
