//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

fileprivate let kNumberLabelBoxWidth = CGFloat(50.0)

// MARK:- Setup
class ProgramPhaseTableViewCell: UITableViewCell {
    
    static let reuseID = "program_phase_cell_reuse_id"
    let numberLabel = UILabel()
    let phaseLabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        accessoryType = .disclosureIndicator
        selectionStyle = .none
        setupNumberLabel()
        setupPhaseLabel()
    }
    
    private func setupNumberLabel() {
        numberLabel.font = UIFont.brandBold(size: 28.0)
        numberLabel.textColor = UIColor.white
        numberLabel.backgroundColor = UIColor.brandAccentBlue
        numberLabel.layer.masksToBounds = true
        numberLabel.layer.cornerRadius = 4.0
        numberLabel.textAlignment = .center
        addSubview(numberLabel)
    }
    
    private func setupPhaseLabel() {
        phaseLabel.font = UIFont.brandMedium(size: 24.0)
        phaseLabel.textColor = UIColor.brandDarkGray
        addSubview(phaseLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK:- Layout
extension ProgramPhaseTableViewCell {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let space = CGFloat(20.0)
        numberLabel.frame = CGRect(space, (frame.height - kNumberLabelBoxWidth) / 2, kNumberLabelBoxWidth, kNumberLabelBoxWidth)
        phaseLabel.frame = CGRect(kNumberLabelBoxWidth + space * 2, 0.0, frame.width - kNumberLabelBoxWidth - space * 3, frame.height)
    }
}

// MARK:- Prepare
extension ProgramPhaseTableViewCell {
    
    func prepare(phase: Section) {
        phaseLabel.text = phase.name
        numberLabel.text = phase.contentLocked ? "🔒" : String(phase.position)
        numberLabel.backgroundColor = phase.contentLocked ? UIColor.brandUltraLightGray : UIColor.brandAccentBlue
    }
    
    func prepareAsResourcesCell() {
        phaseLabel.text = "Resources"
        numberLabel.text = "📄"
        numberLabel.backgroundColor = UIColor(red: 0.212, green: 0.638, blue: 0.400, alpha: 1.00)    }
}

