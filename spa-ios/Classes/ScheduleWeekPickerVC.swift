//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

fileprivate let kStoryboardName = "ScheduleWeekPickerVC"

// MARK:- Setup
class ScheduleWeekPickerVC: UIViewController, DatePickerInputViewProtocol {
    
    // MARK:- Outlets
    @IBOutlet weak var instructionsLabel: UILabel!
    @IBOutlet weak var textField: CaretlessTextField!
    
    // MARK:- Properties
    var program: WorkoutProgram?
    var pattern:ScheduleWeekPattern?
    var weekdayIndex = 0
    var startOfWeekOptionDates = [Date]()
    var selectedDateIndex = 0
    var startDatePicker: DatePickerInputView?
    var session: Session?
    
    // MARK:- Load From Storyboard
    class func generateScheduleDayPicker(session: Session, pattern: ScheduleWeekPattern, startingWeekday: Int, program: WorkoutProgram) -> ScheduleWeekPickerVC {
        let storyboard = UIStoryboard(name: kStoryboardName, bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as! ScheduleWeekPickerVC
        vc.session = session
        vc.program = program
        vc.weekdayIndex = startingWeekday
        vc.pattern = pattern
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureTextField()
        setupInstructionsLabel()
    }
    
    fileprivate func configureView() {
        setupBlankBackButton()
        title = program?.name
    }
    
    fileprivate func setupInstructionsLabel() {
        guard let pat = pattern else { return }
        instructionsLabel.text = generateInstructionString(pattern: pat)
    }
    
    fileprivate func configureTextField() {
        guard let (dates, dateStrings) = generateStartDates() else { return }
        startOfWeekOptionDates = dates
        startDatePicker = DatePickerInputView(dateStrings: dateStrings, selectionDelegate: self)
        textField.inputView = startDatePicker
        textField.inputAccessoryView = UIButton.generateAccessoryInputButton(title: "Continue", target: self, selector: #selector(continueButtonPressed))
        startDatePicker?.selectRow(5, inComponent: 0, animated: false) // selects the upcoming week (5 since we use 4 prior weeks)
        didSelectOptionAtIndex(index: 5) // updates label
        textField.becomeFirstResponder()
        textField.inputAssistantItem.leadingBarButtonGroups = []
        textField.inputAssistantItem.trailingBarButtonGroups = []
    }
    
    func generateStartDates() -> (dates: [Date], dateStrings: [String])? {
        
        guard let firstStartDateOption = ScheduleDateHelper.singleton.calendar.date(byAdding: DateComponents(day: -28), to: Date()) else { return nil } // 4 weeks back in case the user has already started.
        guard let lastStartDateOptions = ScheduleDateHelper.singleton.calendar.date(byAdding: DateComponents(day: 28), to: Date()) else { return nil }
        let startDaysOfWeekRange = ScheduleDateHelper.determineStartDatesForWeeksInRange(dateInStartWeek: firstStartDateOption, dateInEndWeek: lastStartDateOptions) // Normalized so that dates are the first Sunday of the week
        
        var dateStrings = [String]()
        for date in startDaysOfWeekRange {
           dateStrings.append(ScheduleDateHelper.userDateString(date: date))
        }

        return (dates: startDaysOfWeekRange, dateStrings: dateStrings)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return true
    }
    
    func didSelectOptionAtIndex(index: Int) {
        if index > startOfWeekOptionDates.count - 1 { return }
        let date = startOfWeekOptionDates[index]
        textField.text = ScheduleDateHelper.userDateString(date: date)
        selectedDateIndex = index
    }
    
    @objc func continueButtonPressed() {
        if selectedDateIndex > startOfWeekOptionDates.count - 1 { return }
        guard let startDate = ScheduleDateHelper.singleton.calendar.date(byAdding: DateComponents(day: weekdayIndex), to: startOfWeekOptionDates[selectedDateIndex]) else { return }
        guard let unwrappedProgram = program else { return }
        guard let unwrappedSession = session else { return }
        let vc = SchedulePickerConfirmationVC(session: unwrappedSession, startDate: startDate, program: unwrappedProgram)
        navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK:- Instructions Generation
extension ScheduleWeekPickerVC: UITextFieldDelegate {
    
    fileprivate func generateInstructionString(pattern: ScheduleWeekPattern) -> String {
        let daysString = dayOfWeekString(pattern: pattern)
        return "Awesome! You'll be working out on \(daysString)and resting on other days.\n\n Select a starting week below. If you've already started, you can select a date in the past."
    }
    
    // Ugh... I hate this method... clean it up... //ASTODO
    fileprivate func dayOfWeekString(pattern: ScheduleWeekPattern) -> String {
        if weekdayIndex > 6 { return "" }
        let days = ["Sundays, ", "Mondays, ", "Tuesdays, ", "Wednesdays, ", "Thursdays, ", "Fridays, ", "Saturdays, "]
        let adjustedDays = Array(days[weekdayIndex...6] + days[0...weekdayIndex]) // adjust the array to compensate for the users selected start date.
        var description = ""
        if pattern.first { description += adjustedDays[0] }
        if pattern.second { description += adjustedDays[1] }
        if pattern.third { description += adjustedDays[2] }
        if pattern.fourth { description += adjustedDays[3] }
        if pattern.fifth { description += adjustedDays[4] }
        if pattern.sixth { description += adjustedDays[5] }
        if pattern.seventh { description += adjustedDays[6] }
        return description
    }
}


