//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import Alamofire
import ASGlobalOverlay

// MARK:- Setup
class ProgramPhasesTVC: UITableViewController {
    
    let program: Product
    let session: Session
    
    // MARK:- Load From Storyboard
    init(program: Product, session: Session) {
        self.program = program
        self.session = session
        super.init(style: .grouped)
        self.navigationItem.title = "Phases"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBlankBackButton()
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.register(ProgramPhaseTableViewCell.self, forCellReuseIdentifier: ProgramPhaseTableViewCell.reuseID)
    }
}

// MARK:- Table View Delegate
extension ProgramPhasesTVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if program.resources != nil {
            return (section == 0) ? 1 : program.sections.count
        }
        return program.sections.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return (program.resources != nil) ? 2 : 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProgramPhaseTableViewCell.reuseID) as! ProgramPhaseTableViewCell
        if program.resources != nil && indexPath.section == 0 {
            cell.prepareAsResourcesCell()
        } else {
            let phase = program.sections[indexPath.row]
            cell.prepare(phase: phase)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0, let resources = program.resources {
            let resourceTVC = ResourcesTVC(resources: resources)
            navigationController?.pushViewController(resourceTVC, animated: true)
            return
        }
        
        let section = program.sections[indexPath.row]
        guard !section.contentLocked else {
            ASGlobalOverlay.showAlert(withTitle: "Content Locked", message: "This content is not yet available.", dismissButtonTitle: "OK")
            return
        }
        let workoutsTVC = ProgramWorkoutsTVC(section: section, titleText: .Videos, session: session)
        navigationController?.pushViewController(workoutsTVC, animated: true)
    }
}
