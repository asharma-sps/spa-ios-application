//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

private let kLineHeight = CGFloat(2.0)
private let kMarkerCircleDiameter = CGFloat(10.0)
private let kSnapPointCircleDiameter = CGFloat(4.0)

protocol ProgressTimelineViewDelegate {
    func selectedProgressTimelineIndexDidChange(index: Int)
}

// MARK:- Setup
class ProgressTimelineView: UIView {
    
    fileprivate let line = UIView()
    fileprivate let marker = UIView()
    fileprivate let panRecognizer = UIPanGestureRecognizer()
    fileprivate let tapRecognizer = UITapGestureRecognizer()
    fileprivate var delegate: ProgressTimelineViewDelegate?
    fileprivate var snapPointMarkers: [UIView] = []
    fileprivate var snapPointOffsets: [CGFloat]?
    fileprivate var selectedSnapPointIndex = 0
    
    init() {
        super.init(frame: CGRect.zero)
        self.backgroundColor = nil
        setupLine()
        setupMarker()
        setupPanRecognizer()
        setupTapRecognizer()
    }
    
    private func setupLine() {
        line.backgroundColor = UIColor(white: 0.7, alpha: 1.0)
        addSubview(line)
    }
    
    private func setupMarker() {
        marker.backgroundColor = UIColor.brandAccentBlue
        marker.layer.masksToBounds = true
        marker.layer.cornerRadius = kMarkerCircleDiameter / 2
        addSubview(marker)
    }
    
    private func setupPanRecognizer() {
        panRecognizer.addTarget(self, action: #selector(gestureRecognized))
        addGestureRecognizer(panRecognizer)
    }
    
    private func setupTapRecognizer() {
        tapRecognizer.addTarget(self, action: #selector(gestureRecognized))
        addGestureRecognizer(tapRecognizer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
}

// MARK:- Public 
extension ProgressTimelineView {
    
    /// Minimum of 3 snap points
    func enableSnapPoints(count: Int, delegate: ProgressTimelineViewDelegate) {
        if count < 3 { disableSnapPoints(); return }
        self.delegate = delegate
        internalEnableSnapPoints(count: count)
        setNeedsLayout()
    }
    
    func disableSnapPoints() {
        clearSnapPointMarkers()
        snapPointMarkers = []
        snapPointOffsets = nil
    }
    
    func setSelected(index: Int) {
        if snapPointOffsets == nil || index > snapPointOffsets!.count - 1 { return }
        selectedSnapPointIndex = index
        setNeedsLayout()
    }
}

// MARK:- Pan Recognizer
extension ProgressTimelineView {
    
    @objc fileprivate func gestureRecognized(_ sender: AnyObject) {
        guard let rec = sender as? UIGestureRecognizer else { return }
        if rec.numberOfTouches == 0 { return }
        let offset = rec.location(ofTouch: 0, in: self).x / frame.width
        guard let snapPointIndex = nearestSnapPointIndex(offset: offset) else {
            positionMarker(offset: offset)
            return
        }
        guard let snapPointOffset = snapPointOffsetForIndex(index: snapPointIndex) else {
            positionMarker(offset: offset)
            return
        }
        if snapPointIndex != selectedSnapPointIndex {
            positionMarker(offset: snapPointOffset)
            selectedSnapPointIndex = snapPointIndex
            delegate?.selectedProgressTimelineIndexDidChange(index: selectedSnapPointIndex)
        }
    }
}

// MARK:- Snap Point Management
extension ProgressTimelineView {
    
    func internalEnableSnapPoints(count: Int) {
        var offsets: [CGFloat] = []
        var lastPoint: Float = 0.0
        let increment = 1.0 / Float(count - 1)
        for _ in 0..<count {
            lastPoint = min(lastPoint, 1.0)
            offsets.append(CGFloat(lastPoint))
            lastPoint += increment
        }
        snapPointOffsets = offsets
        setupSnapPointMarkers(count: count)
    }
    
    private func setupSnapPointMarkers(count: Int) {
        var markers: [UIView] = []
        for _ in 0..<count {
            let dot = UIView()
            dot.backgroundColor = UIColor(white: 0.7, alpha: 1.0)
            dot.layer.masksToBounds = true
            dot.layer.cornerRadius = kSnapPointCircleDiameter / 2
            addSubview(dot)
            markers.append(dot)
        }
        clearSnapPointMarkers()
        snapPointMarkers = markers
        bringSubview(toFront: marker)
        self.setNeedsLayout()
    }
    
    fileprivate func clearSnapPointMarkers() {
        for snapMarker in snapPointMarkers {
            snapMarker.removeFromSuperview()
        }
    }
    
    fileprivate func nearestSnapPointIndex(offset: CGFloat) -> Int? {
        if snapPointOffsets == nil { return nil }
        var currentPointIndex = 0
        var nearestPointIndex = 0
        var smallestDifference = CGFloat.greatestFiniteMagnitude
        for point in snapPointOffsets! {
            let difference = fabs(point - offset)
            if difference < smallestDifference {
                nearestPointIndex = currentPointIndex
                smallestDifference = difference
            }
            currentPointIndex += 1
        }
        return nearestPointIndex
    }
    
    fileprivate func snapPointOffsetForIndex(index: Int) -> CGFloat? {
        if snapPointOffsets == nil || index > snapPointOffsets!.count - 1 { return nil }
        return snapPointOffsets![index]
    }
}

// MARK:- Layout
extension ProgressTimelineView {

    override func layoutSubviews() {
        let lineOriginY = (frame.height - kLineHeight) / 2
        let dotOriginY = (frame.height - kSnapPointCircleDiameter) / 2
        let lineWidth = frame.width - kSnapPointCircleDiameter
        let dotSpacing = lineWidth / CGFloat(snapPointMarkers.count - 1)
        line.frame = CGRect(kSnapPointCircleDiameter / 2, lineOriginY, lineWidth, kLineHeight)
        var offset: CGFloat = 0.0
        for dot in snapPointMarkers {
            dot.frame = CGRect(offset, dotOriginY, kSnapPointCircleDiameter, kSnapPointCircleDiameter)
            offset += dotSpacing
        }
        guard let markerOffset = snapPointOffsetForIndex(index: selectedSnapPointIndex) else { return }
        positionMarker(offset: markerOffset)
    }
    
    fileprivate func positionMarker(offset: CGFloat) {
        if offset < 0 || offset > 1.0 { return }
        let scrollableWidth = frame.width - kMarkerCircleDiameter
        let markerOriginX = scrollableWidth * offset
        let markerOriginY = (frame.height - kMarkerCircleDiameter) / 2
        marker.frame = CGRect(markerOriginX, markerOriginY, kMarkerCircleDiameter, kMarkerCircleDiameter)
    }
}
