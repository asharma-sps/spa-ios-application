//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

extension UITableViewController {
    
    func deselectAnySelectedCell() {
        tableView.deselectAnySelectedCell()
    }
}
