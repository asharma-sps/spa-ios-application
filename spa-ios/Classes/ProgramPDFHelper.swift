//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import Alamofire

class ProgramPDFHelper {
    
    class func downloadGuide(savePath: URL, resourceURL: URL, completion: @escaping (Bool) -> Void) {
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (savePath, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(resourceURL, to: destination).response { response in
            if response.error == nil, let _ = response.destinationURL?.path {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    class func pathForGuideFile(name: String) -> URL {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let pathURL = URL(fileURLWithPath: path)
        return pathURL.appendingPathComponent(name)
    }
}
