//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

protocol DatePickerInputViewProtocol {
    func didSelectOptionAtIndex(index: Int)
}

class DatePickerInputView: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let dateStrings: [String]
    let selectionDelegate: DatePickerInputViewProtocol
    
    init(dateStrings: [String], selectionDelegate: DatePickerInputViewProtocol) {
        self.dateStrings = dateStrings
        self.selectionDelegate = selectionDelegate
        super.init(frame: CGRect.zero)
        delegate = self
        dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dateStrings.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dateStrings[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectionDelegate.didSelectOptionAtIndex(index: row)
    }
}
