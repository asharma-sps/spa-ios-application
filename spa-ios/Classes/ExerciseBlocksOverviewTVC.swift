//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

// MARK:- Setup
class ExerciseBlocksOverviewTVC: NetworkViewController { // ASTODO
    
    let tableView = UITableView(frame: CGRect.zero, style: .grouped)
    let requestHandler = ExerciseRequestHandler()
    let session: Session
    let workout: Workout
    var exerciseBlocks = [ExerciseBlock]()
    var multiBlockMode: Bool { return exerciseBlocks.count > 1 }
    
    init(session: Session, workout: Workout) {
        self.workout = workout
        self.session = session
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupTableView()
        requestExerciseBlock()
    }
    
    private func configureView() {
        title = workout.title
        setupBlankBackButton()
    }
    
    private func setupTableView() {
        tableView.register(ExerciseTableViewCell.self, forCellReuseIdentifier: ExerciseTableViewCell.reuseID)
        tableView.dataSource = self
        tableView.delegate = self
        mainView.addSubview(tableView)
    }
    
    private func requestExerciseBlock() {
        baseViewControllerState = .loadingView
        
        if let exerciseBlocks = loadExerciseBlockFromDisk(), exerciseBlocks.count > 0 {
            self.exerciseBlocks = exerciseBlocks
            self.tableView.reloadData()
            self.baseViewControllerState = .mainView
            return
        }
        
        requestHandler.requestExercises(user: session.user, workoutID: workout.id) { (_, optionalExerciseBlocks) in
            if let exerciseBlocks = optionalExerciseBlocks, !exerciseBlocks.isEmpty {
                self.exerciseBlocks = exerciseBlocks
                self.tableView.reloadData()
                self.baseViewControllerState = .mainView
            } else {
                self.exerciseBlocks = []
                self.baseViewControllerState = .failureView
            }
        }
    }
    
    private func loadExerciseBlockFromDisk() -> [ExerciseBlock]? {
        let resourceHelper = StoredContentResourceHelper(productID: workout.productID)
        guard let data = FileManager.default.contents(atPath: resourceHelper.filePathForExerciseBlocksData(workoutID: workout.id).path) else { return nil }
        return requestHandler.generateExerciseBlocks(data: data)
    }
    
    override func retryLoadingData() {
        requestExerciseBlock()
    }
}

// MARK:- Table View Delegate / Data Source
extension ExerciseBlocksOverviewTVC: UITableViewDelegate, UITableViewDataSource {
    
    // data presentation is different for a single exercise block vs multiple exercise block
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if exerciseBlocks.count == 0 { return 0 }
        return (multiBlockMode) ? exerciseBlocks.count : 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if multiBlockMode {
            return exerciseBlocks[section].exercises.count + 1
        } else {
            return (section == 0) ? 1 : exerciseBlocks[0].exercises.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ExerciseTableViewCell.reuseID) as? ExerciseTableViewCell ?? ExerciseTableViewCell(style: .subtitle, reuseIdentifier: ExerciseTableViewCell.reuseID)
        if multiBlockMode {
            if indexPath.row == walkthroughCellRowIndex(forSection: indexPath.section) {
                cell.prepareAsPlayAllCell(forExerciseGroup: true)
            } else {
                cell.prepare(exercise: exerciseBlocks[indexPath.section].exercises[indexPath.row])
            }
        } else {
            if (indexPath.section == 0) {
                cell.prepareAsPlayAllCell(forExerciseGroup: false)
            } else {
                cell.prepare(exercise: exerciseBlocks[0].exercises[indexPath.row])
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if multiBlockMode {
            return (indexPath.row == walkthroughCellRowIndex(forSection: indexPath.section)) ? 80.0 : 60.0
        } else {
            return (indexPath.section == 0) ? 80.0 : 60.0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if multiBlockMode {
            return "Exercise group \(section + 1), perform \(exerciseBlocks[section].sets) times"
        } else {
            guard section == 1 else { return nil }
            return "perform this set of exercises \(exerciseBlocks[0].sets) times"
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if multiBlockMode {
            
            if indexPath.row == walkthroughCellRowIndex(forSection: indexPath.section) {
                presentWalkthrough(exerciseBlock: exerciseBlocks[indexPath.section])
            } else {
                presentExerciseDetail(exercise: exerciseBlocks[indexPath.section].exercises[indexPath.row])
            }
        } else {
            if indexPath.section == 0 {
                presentWalkthrough(exerciseBlock: exerciseBlocks[0])
            } else {
                presentExerciseDetail(exercise: exerciseBlocks[0].exercises[indexPath.row])
            }
        }
    }
    
    private func walkthroughCellRowIndex(forSection section: Int) -> Int {
        return exerciseBlocks[section].exercises.count 
    }
}

// MARK:- Presentation Helpers
extension ExerciseBlocksOverviewTVC {
    
    fileprivate func presentWalkthrough(exerciseBlock: ExerciseBlock) {
        let walkthroughVC = ExerciseWalkthroughVC(exerciseBlock: exerciseBlock, productID: workout.productID)
        let walkthroughNC = ManagedNavigationController(rootViewController: walkthroughVC)
        present(walkthroughNC, animated: true, completion: nil)
    }
    
    fileprivate func presentExerciseDetail(exercise: Exercise) {
        let exerciseVC = ExerciseDetailVC(exercise: exercise, productID: workout.productID, usedForExerciseCard: false)
        navigationController?.pushViewController(exerciseVC, animated: true)
    }
}

// MARK:- Layout
extension ExerciseBlocksOverviewTVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(0.0, 0.0, mainView.frame.width, mainView.frame.height)
    }
}

// MARK:- Analytics
extension ExerciseBlocksOverviewTVC {
    
    override func analyticsViewDidAppear() {
        Analytics.screenDidAppear(screen: .exerciseBlocksOverview)
    }
    
    override func analyticsViewDidDisappear() {
        Analytics.screenDidDissapear(screen: .exerciseBlocksOverview, attributes: [AnalyticAttribute.workoutID:    String(workout.id),
                                                                                   AnalyticAttribute.productID:    String(workout.productID),
                                                                                   AnalyticAttribute.workoutType:  workout.type])
    }
}
