//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class PlayerFailureWarningView: UIView {
    
    private let descriptionLabel = UILabel()
    var productID: Int = -1 {
        didSet {
            updateDescriptionLabel()
        }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        backgroundColor = UIColor.brandDarkGray
        descriptionLabel.textColor = UIColor.white
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .center
        updateDescriptionLabel()
        addSubview(descriptionLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func updateDescriptionLabel() {
        let resourceHelper = StoredContentResourceHelper(productID: productID)
        
        var text = "We are unable to play this video. Please check your network settings."
        if resourceHelper.isProductDownloaded() {
            text += " \n\nSince you have downloaded this program for offline viewing, you may still view the exercises in this workout by tapping \"Workout\" below."
        }
        
        descriptionLabel.text = text
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        descriptionLabel.frame = CGRect(0.0, 0.0, frame.width, frame.height)
    }
}
