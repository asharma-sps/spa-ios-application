//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

enum WeightUnit: String {
    case Pounds = "lbs"
    case Kilograms = "kg"
}

struct ProgressEntry {
    let id: Int
    let weight: String
    let date: Date
    let imageURL: String
    var unit: WeightUnit? {
        if weight.contains("lbs") || weight.contains("Lbs") { return .Pounds } // accounts for legacy data formats
        if weight.contains("kg") || weight.contains("Kg") { return .Kilograms } // accounts for legacy data formats
        return nil
    }
    
    func seperateWeightStringComponents() -> (whole: Int, decimal: Int)? {
        if !validateWeightString(weightString: weight) { return nil }
        guard let wholeNumberRange = weight.range(of: ".") else { return nil }
        let wholeNumberString = weight.substring(to: wholeNumberRange.upperBound)
        let decimalNumberString = weight.characters[wholeNumberRange.upperBound]
        guard let whole = Int(wholeNumberString) else { return nil }
        guard let decimal = Int(String(decimalNumberString)) else { return nil }
        return (whole, decimal)
    }
    
    private func validateWeightString(weightString: String) -> Bool {
        if weightString.isEmpty { return false }
        guard let regex = try? NSRegularExpression(pattern: "^\\d\\d\\d?.\\d (lbs|kg)$", options: .anchorsMatchLines) else { return false }
        guard let _ = regex.firstMatch(in: weightString, options: .anchored, range: NSMakeRange(0, weightString.characters.count)) else { return false }
        return true
    }
}
