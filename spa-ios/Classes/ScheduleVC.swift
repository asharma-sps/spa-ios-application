//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

let kScheduleUpdatedNotificationName = "schedule_updated_notification"

class ScheduleVC: NetworkViewController {
    
    fileprivate let session: Session
    fileprivate let pageControl = UIPageControl()
    var scroller = ScheduleWeekCardScrollerPVC()
    var onboardingView: ScheduleOnboardingView?
    var programs: [WorkoutProgram]?
    var entries = [ScheduleEntry]()
    
    init(session: Session) {
        self.session = session
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupScheduleScrollerPVC()
        setupPageControl()
        setupNotificationObserving()
        requestScheduleEntriesData()
    }
    
    fileprivate func setupView() {
        navigationItem.title = "Schedule"
        setupBlankBackButton()
    }
    
    fileprivate func setupScheduleScrollerPVC() {
        scroller.setSelectionDelegate(delegate: self)
        mainView.addSubview(self.scroller.view)
        addChildViewController(self.scroller)
        scroller.didMove(toParentViewController: self)
    }
    
    fileprivate func setupPageControl() {
        pageControl.numberOfPages = 0
        pageControl.pageIndicatorTintColor = UIColor.brandLightGray
        pageControl.currentPageIndicatorTintColor = UIColor.brandDarkGray
        pageControl.isUserInteractionEnabled = false
        mainView.addSubview(pageControl)
    }
    
    fileprivate func setupNotificationObserving() {
        NotificationCenter.default.addObserver(self, selector: #selector(requestScheduleEntriesData), name: NSNotification.Name(rawValue: kScheduleUpdatedNotificationName), object: nil)
    }
    
    @objc fileprivate func requestScheduleEntriesData() {
        baseViewControllerState = .loadingView
        self.configureScheduleBarButtons(visible: false)
        ScheduleRequestHandler.requestSchedulableProgramsAndScheduleEntries(user: session.user) { optionalResultTupple in
            
            guard let (programs, entries) = optionalResultTupple else {
                self.baseViewControllerState = .failureView
                self.programs = nil
                return
            }
            
            self.programs = programs
            self.entries = entries
                        
            if self.entries.isEmpty {
                self.setupScheduleOnboarding()
                self.baseViewControllerState = .mainView
                self.mainView.setNeedsLayout()
                Analytics.setProfileAttribute(key: .currentlyHasProgramScheduled, value: false)
                return
            } else {
                self.breakdownScheduleOnboarding()
                Analytics.setProfileAttribute(key: .currentlyHasProgramScheduled, value: true)
            }
            
            let dateSorted = entries.sorted(by: { $0.date.compare($1.date) == .orderedAscending})
            guard let first = dateSorted.first, let last = dateSorted.last else { return }
            let weekStartDates = ScheduleDateHelper.determineStartDatesForWeeksInRange(dateInStartWeek: first.date, dateInEndWeek: last.date)
            
            self.pageControl.numberOfPages = weekStartDates.count
            let entriesPool = ScheduleEntriesPool()
            entriesPool.addEntries(entries: entries)
            self.scroller.setWeekStartDates(weekStartDates: weekStartDates, pool: entriesPool, weekTableViewDelegate: self)
            self.pageControl.currentPage = 0
            self.baseViewControllerState = .mainView
            self.configureScheduleBarButtons(visible: true)
        }
    }
    
    fileprivate func configureScheduleBarButtons(visible: Bool) {
        navigationItem.rightBarButtonItem = visible ? UIBarButtonItem(title: "Manage", style: .plain, target: self, action: #selector(showPlannerVC)) : nil
    }
}

// MARK:- Layout
extension ScheduleVC {

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if onboardingView != nil {
            onboardingView?.frame = CGRect(0.0, 0.0, mainView.frame.width, mainView.frame.height)
            mainView.bringSubview(toFront: onboardingView!)
        } else {
            scroller.view.frame = CGRect(0.0, 0.0, mainView.frame.width, mainView.frame.height - kWeekCardViewPadding)
            let pageControlOriginY = scroller.view.frame.maxY - kWeekCardViewPadding // the WeekCards views are centered in their VC w/ transparent padding on all sides == kWeekCardViewPadding
            pageControl.frame = CGRect(0.0, pageControlOriginY, mainView.frame.width, mainView.frame.height - pageControlOriginY)
        }
    }
}

// MARK:- Helpers
extension ScheduleVC {
    
    @objc func showPlannerVC() {
        let plannerNC = ManagedNavigationController(rootViewController: ScheduleProgramPickerVC(session: session, entries: entries))
        present(plannerNC, animated: true, completion: nil)
    }
    
    override func retryLoadingData() {
        requestScheduleEntriesData()
    }
}

// MARK:- WeekTableViewDelegate
extension ScheduleVC: WeekTableViewDelegate {
    
    func pushViewControllerForEntry(entry: ScheduleEntry) {
        var possibleProduct: Product?
        productLoop: for i in session.productsPool.products {
            if i.id == entry.programID {
                possibleProduct = i
                break productLoop
            }
        }
        guard let product = possibleProduct else { return }
        for i in product.sections {
            for x in i.workouts {
                if x.id == entry.workoutID {
                    let player = ProductContentVideoPlayerVC(session: session, workout: x)
                    player.hidesBottomBarWhenPushed = true
                    navigationController?.pushViewController(player, animated: true)
                }
            }
        }
    }
}

// ScheduleWeekCardScrollerDelegate
extension ScheduleVC: ScheduleWeekCardScrollerDelegate {
 
    func selectedScheduleWeekCardEntryDidChange(index: Int) {
        self.pageControl.currentPage = index
    }
}

// MARK:- Onboarding Management
extension ScheduleVC: ScheduleOnboardingViewDelegate {
    
    func setupScheduleButtonPressed() {
        showPlannerVC()
    }
    
    fileprivate func setupScheduleOnboarding() {
        onboardingView?.removeFromSuperview() // safety cleanup
        onboardingView = ScheduleOnboardingView.generateScheduleOnboardingViewFromNib(delegate: self)
        mainView.addSubview(onboardingView!)
    }
    
    fileprivate func breakdownScheduleOnboarding() {
        onboardingView?.removeFromSuperview()
        onboardingView = nil
    }
}

// MARK:- Analytics
extension ScheduleVC {
    
    override func analyticsViewDidAppear() {
        Analytics.screenDidAppear(screen: .schedule)
    }
    
    override func analyticsViewDidDisappear() {
        Analytics.screenDidDissapear(screen: .schedule)
    }
}

