//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import AVFoundation
import AVKit

private let kPlayerActionBarViewHeight = CGFloat(70.0)

// MARK:- Setup
class BaseVideoPlayerVC: AnalyticBaseViewController {
    
    let videoPlayerViewController: VideoPlayerVC
    let descriptionTextView = UITextView()
    let buttonBarView: ButtonBarView
    
    init(buttonTitle: String, productID: Int?) {
        videoPlayerViewController = VideoPlayerVC(productID: productID)
        buttonBarView = ButtonBarView(buttonTitle: buttonTitle)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupVideoPlayerViewController()
        setupDescriptionTextView()
        setupButtonBarView()
    }
    
    private func configureView() {
        view.backgroundColor = UIColor.groupTableViewBackground
        navigationController?.interactivePopGestureRecognizer?.delaysTouchesBegan = false // covers the play button the video player
        setupBlankBackButton()
    }
    
    private func setupVideoPlayerViewController() {
        addChildViewController(videoPlayerViewController)
        view.addSubview(videoPlayerViewController.view)
        videoPlayerViewController.didMove(toParentViewController: self)
        videoPlayerViewController.delegate = self
    }
    
    private func setupDescriptionTextView() {
        descriptionTextView.font = UIFont.brandMedium()
        descriptionTextView.backgroundColor = UIColor.white
        descriptionTextView.textColor = UIColor.brandDarkGray
        descriptionTextView.isEditable = false
        descriptionTextView.textContainerInset = UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0)
        view.addSubview(descriptionTextView)
    }
    
    private func setupButtonBarView() {
        buttonBarView.delegate = self
        view.addSubview(buttonBarView)
    }
}

// MARK:- Subclass Helpers
extension BaseVideoPlayerVC {
    
    internal func play(videoURL: String) {
        guard let url = URL(string: videoURL) else { return }
        let player = AVPlayer(url: url)
        videoPlayerViewController.setPlayer(player: player)
        player.play()
    }
    
    internal func setButtonBarVisibility(visible: Bool) {
        buttonBarView.isHidden = !visible
        view.setNeedsLayout()
    }
    
    internal func setDescriptionText(HTMLString: String) {
        guard let data = HTMLString.data(using: String.Encoding.unicode) else { return }
        if let attributedString = try? NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf16.rawValue], documentAttributes: nil) { // ASTODO... isolate, add title/subtitle... :(
            descriptionTextView.text = attributedString.string
        } else {
            descriptionTextView.text = "No Description"
        }
    }
}

// MARK:- PlayerControlsViewDelegate
extension BaseVideoPlayerVC: VideoPlayerViewControllerDelegate {
    
    func presentFullscreenViewController(player: AVPlayer) {
        player.pause()
        let fullscreenVC = FullscreenContentPlayer(player: player)
        present(fullscreenVC, animated: true) {
            fullscreenVC.play()
        }
    }
}

// MARK:- Layout
extension BaseVideoPlayerVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let playerHeight = VideoPlayerVC.requiredViewHeight(width: view.frame.width)
        videoPlayerViewController.view.frame = CGRect(0.0, 0.0, view.frame.width, playerHeight)
        let workoutButtonBarHeight = buttonBarView.isHidden ? CGFloat(0.0) : kPlayerActionBarViewHeight
        descriptionTextView.frame = CGRect(0.0, playerHeight, view.frame.width, view.frame.height - playerHeight - workoutButtonBarHeight)
        if !buttonBarView.isHidden {
            buttonBarView.frame = CGRect(0.0, view.frame.height - kPlayerActionBarViewHeight, view.frame.width, kPlayerActionBarViewHeight)
        }
    }
}

// MARK:- PlayerActionBarViewDelegate
extension BaseVideoPlayerVC: ButtonBarViewDelegate {
    
    func buttonPressed() {
        print("override me! Line: \(#line)")
    }
}
