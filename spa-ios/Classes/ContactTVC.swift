//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

private let StoryboardName = "ContactTVC"

class ContactTVC: UITableViewController {
    
    @IBOutlet weak var unitedStatesCell: UITableViewCell!
    @IBOutlet weak var internationalCell: UITableViewCell!
    @IBOutlet weak var emailCell: UITableViewCell!
    var session: Session?
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        deselectAnySelectedCell()
        let cell = tableView.cellForRow(at: indexPath)
        if cell == unitedStatesCell { call(number: "1-800-655-8576")}
        else if cell == internationalCell { call(number: "1-855-520-7596")}
        else if cell == emailCell {
            let messageVC = MessageVC.generateMessageViewController()
            messageVC.session = session
            let messageNC = ManagedNavigationController(rootViewController: messageVC)
            self.present(messageNC, animated: true, completion: nil)
        }
    }
    
    private func call(number: String) {
        guard let callURL = URL(string:"tel:\(number)") else { return }
        UIApplication.shared.openURL(callURL)
    }
    
    // MARK:- Load From Storyboard
    class func generateContactTableViewController() -> ContactTVC {
        let storyboard = UIStoryboard(name: StoryboardName, bundle: nil)
        let contactTVC = storyboard.instantiateInitialViewController() as? ContactTVC!
        return contactTVC!
    }
}
