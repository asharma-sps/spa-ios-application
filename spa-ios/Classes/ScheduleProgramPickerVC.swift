//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import ASGlobalOverlay

fileprivate let kProgramCellReuseID = "program_cell"

// MARK:- Setup
class ScheduleProgramPickerVC: NetworkViewController {
    
    fileprivate let session: Session
    fileprivate let entries: [ScheduleEntry]
    fileprivate let tableView = UITableView(frame: CGRect.zero, style: .grouped)
    fileprivate var programs = [(program: WorkoutProgram, scheduled: Bool)]()
    
    init(session: Session, entries: [ScheduleEntry]) {
        self.session = session
        self.entries = entries
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupTableView()
        setupClearButton()
        requestSchedulablePrograms()
    }
    
    fileprivate func configureView() {
        title = "Schedule Setup"
        setupBlankBackButton()
        addCloseButton()
    }
    
    fileprivate func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(ScheduleProgramPickerTableViewCell.self, forCellReuseIdentifier: kProgramCellReuseID)
        mainView.addSubview(tableView)
    }
    
    fileprivate func setupClearButton() {
        if entries.count == 0 { return }
        let button = UIBarButtonItem(title: "Clear", style: .plain, target: self, action: #selector(clearAllEntries))
        navigationItem.rightBarButtonItem = button
    }
    
    @objc fileprivate func requestSchedulablePrograms() {
        baseViewControllerState = .loadingView
        ScheduleRequestHandler.requestSchedulablePrograms(user: session.user) { optionalPrograms in
            guard let programs = optionalPrograms, !programs.isEmpty else { // There should always be at least 1 program (Fit 15 is always included)
                print("ERROR: ScheduleProgramPickerVC failed to request schedulable programs for user.")
                self.baseViewControllerState = .failureView
                return
            }
            
            // filters out nutritional programs
            var schedulablePrograms = [WorkoutProgram]()
            for program in programs {
                if program.subtype == WorkoutSubtype.program && !program.outlines.isEmpty {
                    schedulablePrograms.append(program)
                }
            }
            
            self.programs = self.determineIfProgramIsScheduled(programs: schedulablePrograms)
            self.baseViewControllerState = .mainView
            self.tableView.reloadData()
        }
    }
    
    @objc fileprivate func clearAllEntries() {
        let clear = ASUserOption.destructiveUserOption(withTitle: "Clear") {
            ASGlobalOverlay.showWorkingIndicator(withDescription: nil)
            ScheduleRequestHandler.clearScheduleEntries(user: self.session.user) { (success) in
                if !success {
                    ASGlobalOverlay.showAlert(withTitle: "Error", message: "We were unable to clear your schedule. Please try again.", dismissButtonTitle: "OK")
                } else {
                    self.clearScheduledSymbol()
                    self.navigationItem.rightBarButtonItem = nil
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kScheduleUpdatedNotificationName), object: nil)
                    ASGlobalOverlay.dismissWorkingIndicator()
                }
            }
        }
        let cancel = ASUserOption.cancel(withTitle: "Cancel", actionBlock: nil)
        ASGlobalOverlay.showAlert(withTitle: "Clear Schedule", message: "Would you like to clear your workout schedule?", userOptions: [clear as Any, cancel as Any])
    }
    
    fileprivate func determineIfProgramIsScheduled(programs: [WorkoutProgram]) -> [(program: WorkoutProgram, scheduled: Bool)] {
        let scheduledProgramID = entries.first?.programID ?? -1
        var workoutTupples = [(program: WorkoutProgram, scheduled: Bool)]()
        for program in programs {
            if program.id == scheduledProgramID {
                workoutTupples.append((program: program, scheduled: true))
            }
            else {
                workoutTupples.append((program: program, scheduled: false))
            }
        }
        return workoutTupples
    }
    
    private func clearScheduledSymbol() {
        programs = programs.map() { (program: $0.program, scheduled: false)}
        tableView.reloadData()
    }
}


// MARK:- Layout
extension ScheduleProgramPickerVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(0.0, 0.0, mainView.frame.width, mainView.frame.height)
    }
}

// MARK:- Table View Delegate
extension ScheduleProgramPickerVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kProgramCellReuseID, for: indexPath) as! ScheduleProgramPickerTableViewCell
        cell.prepare(programTupple: programs[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return programs.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Select a program to schedule:"
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "You may only have one program scheduled at a time."
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if entries.isEmpty {
            startSchedulingForProgramAtIndexPath(index: indexPath.row)
        } else {
            let cancel = ASUserOption.cancel(withTitle: "Cancel", actionBlock: nil)
            let clear = ASUserOption(title: "Continue", actionBlock: {
                ASGlobalOverlay.showWorkingIndicator(withDescription: nil)
                ScheduleRequestHandler.clearScheduleEntries(user: self.session.user) { (success) in
                    if success {
                        self.startSchedulingForProgramAtIndexPath(index: indexPath.row)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kScheduleUpdatedNotificationName), object: nil)
                        self.navigationItem.rightBarButtonItem = nil
                        ASGlobalOverlay.dismissWorkingIndicator()
                    } else {
                        ASGlobalOverlay.showAlert(withTitle: "Error", message: ErrorMessages.general, dismissButtonTitle: "OK")
                    }
                }
            })
            ASGlobalOverlay.showAlert(withTitle: "Reset Schedule", message: "Scheduling a program will replace your current schedule.", userOptions: [clear as Any, cancel as Any])
        }
    }
    
    fileprivate func startSchedulingForProgramAtIndexPath(index: Int) {
        
        if programs.count < index { return }
        let program = programs[index].program
        if program.outlines.count < 7 { return }
        let pattern = ScheduleWeekPattern(first: program.outlines[0].id != nil,
                                          second: program.outlines[1].id != nil,
                                          third: program.outlines[2].id != nil,
                                          fourth: program.outlines[3].id != nil,
                                          fifth: program.outlines[4].id != nil,
                                          sixth: program.outlines[5].id != nil,
                                          seventh: program.outlines[6].id != nil)
        // print(pattern) // DEBUG
        let vc = ScheduleDayPickerVC.generateScheduleDayPicker(session: session, pattern: pattern, program: program)
        navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK:- Layout
extension ScheduleProgramPickerVC {
    
    override func retryLoadingData() {
        requestSchedulablePrograms()
    }
}
