//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2016 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

private let kTableViewCellReuseIdentifier = "resource_cell"

class ResourcesTVC: UITableViewController {
    
    let resources: [Resource]
    
    init(resources: [Resource]) {
        self.resources = resources
        super.init(style: .grouped)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Resources"
        setupBlankBackButton()
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: kTableViewCellReuseIdentifier)
    }
}

extension ResourcesTVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resources.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let resource = resources[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: kTableViewCellReuseIdentifier)!
        cell.textLabel?.font = UIFont.brandMedium(size: 24)
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.textColor = UIColor.brandDarkGray
        cell.textLabel?.text = resource.title
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let resource = resources[indexPath.row]
        let viewer = PDFViewerVC(url: resource.resource, title: resource.title)
        navigationController?.pushViewController(viewer, animated: true)
    }
}
