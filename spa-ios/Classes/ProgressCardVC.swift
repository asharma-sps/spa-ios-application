//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

private let kContainerSidePadding = CGFloat(20.0)
private let kDateLabelHeight = CGFloat(20.0)
private let kWeightLabelHeight = CGFloat(20.0)
private let kGeneralLabelPadding = CGFloat(8.0)
private let kLabelAreaHeight = kDateLabelHeight + kWeightLabelHeight + kGeneralLabelPadding * 3

// MARK:- Setup
class ProgressCardVC: UIViewController {
    
    var entry: ProgressEntry
    fileprivate let container = UIView()
    fileprivate let picture = NetworkImageView()
    fileprivate let dateLabel = UILabel()
    fileprivate let weightLabel = UILabel()
    
    init(entry: ProgressEntry) {
        self.entry = entry
        super.init(nibName: nil, bundle: nil)
        prepare(progress: entry)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        setupContainer()
        setupPicture()
        setupLabels()
        prepare(progress: entry)
    }
    
    private func setupContainer() {
        container.backgroundColor = UIColor.white
        container.layer.masksToBounds = true
        container.layer.cornerRadius = 3.0
        container.layer.borderWidth = Environment.thinnestLine
        container.layer.borderColor = UIColor.brandLightGray.cgColor
        view.addSubview(container)
    }
    
    private func setupPicture() {
        picture.backgroundColor = UIColor.brandDarkGray
        picture.contentMode = .scaleAspectFill
        picture.clipsToBounds = true
        container.addSubview(picture)
    }
    
    private func setupLabels() {
        dateLabel.font = UIFont.brandMedium()
        dateLabel.textColor = UIColor.brandDarkGray
        dateLabel.textAlignment = .center
        container.addSubview(dateLabel)
        weightLabel.font = UIFont.brandMedium()
        weightLabel.textColor = UIColor.brandLightGray
        weightLabel.textAlignment = .center
        container.addSubview(weightLabel)
    }
}

// MARK:- Prepare
extension ProgressCardVC {
    
    func prepare(progress:ProgressEntry) {
        entry = progress
        weightLabel.text = String(progress.weight)
        dateLabel.text = ScheduleDateHelper.userDateString(date: progress.date)
        guard let imageURL = URL(string: progress.imageURL) else { return }
        picture.setImageFromURL(imageURL: imageURL)
    }
}

// MARK:- Layout
extension ProgressCardVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let size = view.frame.size
        let contentWidth = size.width - kContainerSidePadding * 2
        container.frame = CGRect(kContainerSidePadding, 0.0, contentWidth, size.height)
        picture.frame = CGRect(0.0,
                               0.0,
                               contentWidth,
                               size.height - kLabelAreaHeight)
        dateLabel.frame = CGRect(kGeneralLabelPadding,
                                 picture.frame.height + kGeneralLabelPadding,
                                 contentWidth,
                                 kDateLabelHeight)
        weightLabel.frame = CGRect(kGeneralLabelPadding,
                                   dateLabel.frame.origin.y + dateLabel.frame.height + kGeneralLabelPadding,
                                   contentWidth,
                                   kWeightLabelHeight)
    }
}
