//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

private let kTimelineHeight = CGFloat(35.0)
private let kAddButtonHeight = CGFloat(50.0)
private let kGeneralPadding = CGFloat(20.0)

// MARK:- Setup
class ProgressVC: NetworkViewController {
    
    fileprivate let session: Session
    fileprivate let progressCardScroller = ProgressCardScrollerPVC()
    fileprivate let timeline = ProgressTimelineView()
    fileprivate let addButton = UIButton.generateButton(title: "Add Progress")
    fileprivate let progressService = ProgressRequestHandler()
    fileprivate var onboardingView: ProgressOnboardingView?
    fileprivate var entries: [ProgressEntry]?

    init(session: Session) {
        self.session = session
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Progress"
        setupProgressCardScroller()
        setupTimeline()
        setupAddButton()
        requestProgressEntriesData(completion: nil)
    }
    
    private func setupProgressCardScroller() {
        progressCardScroller.setSelectionDelegate(delegate: self)
        addChildViewController(progressCardScroller)
        self.mainView.addSubview(progressCardScroller.view)
        progressCardScroller.didMove(toParentViewController: self)
    }
    
    private func setupTimeline() {
        self.mainView.addSubview(timeline)
    }
    
    private func setupAddButton() {
        addButton.addTarget(self, action: #selector(addButtonPressed), for: .touchUpInside)
        self.mainView.addSubview(addButton)
    }
}

// MARK:- Delegates
extension ProgressVC: ProgressCardScrollerDelegate, ProgressTimelineViewDelegate, ProgressAddEntryViewControllerDelegate {
    
    func selectedProgressCardEntryDidChange(index: Int){
        timeline.setSelected(index: index)
    }
    
    func selectedProgressTimelineIndexDidChange(index: Int) {
        progressCardScroller.setSelectedProgressEntry(index: index)
    }
    
    func refreshProgressEntriesData(completion: @escaping RefreshCompletion) {
        requestProgressEntriesData(completion: completion)
    }
}

// MARK:- Add Button
extension ProgressVC {
    
    @objc fileprivate func addButtonPressed() {
        let lastEntry = entries?.last
        guard let lastUnit = lastEntry?.unit else {
            let pounds = ASUserOption(title: "Pounds") { self.pushProgressAddEntryVC(entry: lastEntry, unit: .Pounds) }
            let kilograms = ASUserOption(title: "Kilograms") { self.pushProgressAddEntryVC(entry: lastEntry, unit: .Kilograms) }
            let cancel = ASUserOption.cancel(withTitle: "Cancel", actionBlock: nil)
            ASGlobalOverlay.showSlideUpMenu(withPrompt: "Please select your prefered weight unit", userOptions: [pounds as Any, kilograms as Any, cancel as Any])
            return
        }
        pushProgressAddEntryVC(entry: lastEntry, unit: lastUnit)
    }
    
    private func pushProgressAddEntryVC(entry: ProgressEntry?, unit: WeightUnit) {
        let addProgressVC = ProgressAddEntryVC.generateProgressAddEntryViewController(session: session, delegate: self, lastEntry: entry, unit: unit)
        let addProgressNC = ManagedNavigationController(rootViewController: addProgressVC)
        self.present(addProgressNC, animated: true, completion: nil)
    }
}

// MARK:- Layout
extension ProgressVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        runViewConfigurationRoutine()
    }
    
    fileprivate func runViewConfigurationRoutine() {
        showOrHideTimelineDependingOnEntriesCount()
        setupOrTeardownOnboardingDependingOnEntriesCount()
        layoutVisibleViewsAppropriately()
        updateDeleteButtonState()
    }
    
    private func showOrHideTimelineDependingOnEntriesCount() {
        if let unwrappedEntries = entries {
            timeline.isHidden = unwrappedEntries.count < 3 // it's confusing to show the timeline if there are less than 3 entires.
        } else {
            timeline.isHidden = true
        }
    }
    
    private func setupOrTeardownOnboardingDependingOnEntriesCount() {
        if entries == nil || entries?.count ?? 0 > 0 {
            onboardingView?.removeFromSuperview()
            onboardingView = nil
            return
        }
        if onboardingView == nil {
            onboardingView = ProgressOnboardingView.generateProgressOnboardingViewFromNib(delegate: self)
            mainView.addSubview(onboardingView!)
            mainView.bringSubview(toFront: onboardingView!)
        }
    }
    
    fileprivate func updateDeleteButtonState() {
        if entries == nil || entries?.count == 0 || baseViewControllerState != .mainView {
            navigationItem.rightBarButtonItem = nil
        } else {
            let deleteButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteButtonPressed))
            navigationItem.rightBarButtonItem = deleteButton
        }
    }
    
    private func layoutVisibleViewsAppropriately() {
        if let unwrappedOnboardingView = onboardingView {
            unwrappedOnboardingView.frame = CGRect(0.0, 0.0, mainView.frame.width, mainView.frame.height)
            return
        }
        
        let contentAreaWidth = mainView.frame.width - kGeneralPadding * 2
        let spaceForTimeline = !timeline.isHidden ? kTimelineHeight + kGeneralPadding : 0.0
        let progressCardScrollerHeight = mainView.frame.height - kAddButtonHeight - spaceForTimeline - kGeneralPadding * 3
        
        progressCardScroller.view.frame = CGRect(0.0,
                                                     kGeneralPadding,
                                                     mainView.frame.width,
                                                     progressCardScrollerHeight)
        timeline.frame = CGRect(kGeneralPadding,
                                    progressCardScroller.view.frame.maxY + kGeneralPadding,
                                    contentAreaWidth,
                                    kTimelineHeight)
        let addButtonOriginY = !timeline.isHidden ? timeline.frame.maxY  + kGeneralPadding : progressCardScroller.view.frame.maxY + kGeneralPadding
        addButton.frame = CGRect(kGeneralPadding,
                                     addButtonOriginY,
                                     contentAreaWidth,
                                     kAddButtonHeight)
    }
}

// MARK:- ProgressOnboardingViewDelegate
extension ProgressVC: ProgressOnboardingViewDelegate {
    
    func startTrackingButtonPressed() {
        addButtonPressed()
    }
}

// MARK:- Delete Entry
extension ProgressVC {
    
    @objc fileprivate func deleteButtonPressed() {
        let delete = ASUserOption.destructiveUserOption(withTitle: "Delete") { self.deleteCurrentEntry() }
        let cancel = ASUserOption.cancel(withTitle: "Cancel", actionBlock: nil)
        ASGlobalOverlay.showSlideUpMenu(withPrompt: "Are you sure you want to delete this progress entry?", userOptions: [delete as Any, cancel as Any])
    }
    
    private func deleteCurrentEntry() {
        guard let index = progressCardScroller.currentlySelectedIndex() else { return }
        guard let entry = entries?[index] else { return }
        
        ASGlobalOverlay.showWorkingIndicator(withDescription: nil)
        progressService.deleteProgressEntry(user: session.user, id: String(entry.id)) { (success) in
            if success {
                self.entries?.remove(at: index)
                guard let unwrappedEntries = self.entries else { return }
                self.progressCardScroller.setProgressEntries(entries: unwrappedEntries)
                self.timeline.enableSnapPoints(count: unwrappedEntries.count, delegate: self)
                let newIndex = max(index - 1, 0)
                self.progressCardScroller.setSelectedProgressEntry(index: newIndex)
                self.timeline.setSelected(index: newIndex)
                self.runViewConfigurationRoutine()
                ASGlobalOverlay.dismissWorkingIndicator()
            } else {
                ASGlobalOverlay.showAlert(withTitle: "Error", message: "We could not delete this progress entry. Please check your network settings and try again.", dismissButtonTitle: "OK")
            }
        }
    }
}

// MARK:- Request Handling
extension ProgressVC {
    
    override func retryLoadingData() {
        requestProgressEntriesData(completion: nil)
    }
    
    fileprivate func requestProgressEntriesData(completion: RefreshCompletion?) {
        baseViewControllerState = .loadingView
        updateDeleteButtonState()
        progressService.requestProgressEntries(user: session.user) { (success, entries) in
            if !success {
                self.baseViewControllerState = .failureView
                return
            }
            self.entries = entries
            Analytics.setProfileAttribute(key: .progressEntriesCount, value: entries.count)
            if entries.count == 0 {
                self.runViewConfigurationRoutine()
                self.baseViewControllerState = .mainView
            } else {
                self.progressCardScroller.setProgressEntries(entries: entries)
                self.timeline.enableSnapPoints(count: entries.count, delegate: self)
                completion?()
                let index = entries.count - 1
                self.progressCardScroller.setSelectedProgressEntry(index: index)
                self.timeline.setSelected(index: index)
                self.runViewConfigurationRoutine()
                self.baseViewControllerState = .mainView
                self.updateDeleteButtonState() // ran during configuration routine, but needs baseViewControllerState == .MainView
            }
        }
    }
}

// MARK:- Analytics
extension ProgressVC {
    
    override func analyticsViewDidAppear() {
        Analytics.screenDidAppear(screen: .progress)
    }
    
    override func analyticsViewDidDisappear() {
        Analytics.screenDidDissapear(screen: .progress)
    }
}
