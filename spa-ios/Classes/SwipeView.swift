//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

private let kSwipeDistance = CGFloat(300)
private let kCircleDiameter = CGFloat(50.0)

// MARK:- Setup
class SwipeView: UIView {
    
    static let requiredSize = CGSize(kSwipeDistance, 60.0)
    
    fileprivate let circle = UIView()
    fileprivate let label = UILabel()
    fileprivate var animating = false // quick-n-dirty runaway loop prevention
    
    init() {
        super.init(frame: CGRect.zero)
        setupCircle()
        setupLabel()
        stopAnimating() // sets default state
    }
    
    private func configureView() {
        self.isUserInteractionEnabled = false
    }
    
    private func setupCircle() {
        circle.layer.masksToBounds = true
        circle.layer.cornerRadius = kCircleDiameter / 2
        circle.backgroundColor = UIColor.brandLightGray
        circle.isUserInteractionEnabled = false
        addSubview(circle)
    }
    
    private func setupLabel() {
        label.text = "Swipe To Continue"
        label.textColor = UIColor.brandLightGray
        label.textAlignment = .center
        addSubview(label)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK:- Animation Control
extension SwipeView {
    
    func startAnimating() {
        stopAnimating()
        animating = true
        resetCircleAndRepeatAnimatationUnlessCancelled()
    }
    
    func stopAnimating() {
        animating = false
        circle.layer.removeAllAnimations()
        circle.isHidden = true
    }
    
    private func resetCircleAndRepeatAnimatationUnlessCancelled() {
        circle.frame = CGRect(kSwipeDistance - kCircleDiameter, 0.0, kCircleDiameter, kCircleDiameter)
        circle.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        circle.alpha = 1.0
        circle.isHidden = false
        if !animating { return }
        performCircleAnimationPartOneThenTwo()
    }
    
    private func performCircleAnimationPartOneThenTwo() {
        UIView.animate(withDuration: 0.4,
                       animations: {
                        self.circle.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        },
                       completion: { completed in
                        if completed && self.animating {
                            self.performCircleAnimationPartTwo()
                        }
        })
    }
    
    private func performCircleAnimationPartTwo() {
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.circle.frame = CGRect(0.0, 0.0, kCircleDiameter, kCircleDiameter)
                        self.circle.alpha = 0.0
        },
                       completion: { completed in
                        if completed && self.animating {
                            self.resetCircleAndRepeatAnimatationUnlessCancelled()
                        }
        })
    }
}

// MARK:- Layout
extension SwipeView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        label.frame = CGRect(0.0, 0.0, frame.width, frame.height)
    }
}
