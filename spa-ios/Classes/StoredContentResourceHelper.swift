//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

private let kStoredContentDirectoryName = "stored_content"
private let kProductDirectoryPrefix = "product"
private let kExerciseBlockDataPrefix = "exercise_data_for_workout"

// MARK:- Public
struct StoredContentResourceHelper {
    
    fileprivate let storedContentDirectory = URL.documentDirectoryPath().appendingPathComponent(kStoredContentDirectoryName)
    fileprivate let productDirectoryPath: URL

    init(productID: Int) {
        productDirectoryPath = storedContentDirectory.appendingPathComponent("\(kProductDirectoryPrefix)_\(productID)")
    }
    
    func filePathForExerciseBlocksData(workoutID: Int) -> URL {
        let path = productDirectoryPath.appendingPathComponent("\(kExerciseBlockDataPrefix)_\(workoutID)")
        return URL(fileURLWithPath: path.path)
    }
    
    func attemptToEnsureProductDirectoryExist() -> Bool {
        return PersistanceHelper.createDirectoryIfNonExistant(path: productDirectoryPath)
    }
    
    func isProductDownloaded() -> Bool {
        return FileManager.default.fileExists(atPath: productDirectoryPath.path)
    }
    
    func filePathForVideoResource(remoteURL: URL) -> URL {
        var fileName = remoteURL.lastPathComponent
        fileName = fileName.replacingOccurrences(of: " ", with: "_")
        fileName = fileName.replacingOccurrences(of: "%20", with: "_")
        let path = productDirectoryPath.appendingPathComponent(fileName)
        return URL(fileURLWithPath: path.path)
    }
    
    func deleteProductStoredContent() {
        PersistanceHelper.delete(path: productDirectoryPath)
    }
    
    func fileSizeDescription() -> String? {
        guard let size = FileManager.default.roughSizeOfDirectory(filePathURL: productDirectoryPath) else { return nil }
        let sizeFloat = Double(size) / 1024 / 1024
        let sizeString = String(format: "%.1f", sizeFloat)
        return "Downloaded (\(sizeString) MB)"
    }
}

// MARK:- Class-level Public
extension StoredContentResourceHelper {
    
    static func deleteAllStoredContent() {
        let helper = StoredContentResourceHelper(productID: 0) // we don't actually need it to manage a real product, just the root directory
        PersistanceHelper.delete(path: helper.storedContentDirectory)
    }
}
