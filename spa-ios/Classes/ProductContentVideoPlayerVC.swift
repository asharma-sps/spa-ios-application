//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

// MARK:- Setup
class ProductContentVideoPlayerVC: BaseVideoPlayerVC {
    
    let session: Session
    let workout: Workout
    
    init(session: Session, workout: Workout) {
        self.workout = workout
        self.session = session
        let buttonTitle = (workout.status == .Workout) ? "Workout" : "View Recipe & List"
        super.init(buttonTitle: buttonTitle, productID: workout.productID)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupDescriptionLabel()
        setupPlayer()
        configureButtonBar()
    }
    
    private func setupView() {
        title = workout.title
    }
    
    private func setupDescriptionLabel() {
        setDescriptionText(HTMLString: workout.description)
    }
    
    private func configureButtonBar() {
        let containsResource = workout.PDFResources.count > 0
        let isWorkout = (workout.status == .Workout)
        setButtonBarVisibility(visible: (containsResource || isWorkout))
    }
    
    override func buttonPressed() {
        if (workout.status == .Workout) {
            let exercisesTVC = ExerciseBlocksOverviewTVC(session: session, workout: workout)
            navigationController?.pushViewController(exercisesTVC, animated: true)
        } else if workout.PDFResources.count == 1 {
            let viewer = PDFViewerVC(url: workout.PDFResources[0].resource, title: title)
            navigationController?.pushViewController(viewer, animated: true)
        } else if workout.PDFResources.count > 1 {
            let viewer = ResourcesTVC(resources: workout.PDFResources)
            navigationController?.pushViewController(viewer, animated: true)
        }
    }
}

// MARK:- Video Playback Management
extension ProductContentVideoPlayerVC {
    
    fileprivate func setupPlayer() {
        let url = workout.video.preferedResolution
        guard !url.isEmpty else { return }
        play(videoURL: url)
    }
}

// MARK:- Analytics
extension ProductContentVideoPlayerVC {
    
    override func analyticsViewDidAppear() {
        Analytics.screenDidAppear(screen: .contentPlayer)
        Analytics.incrementProfileAttributeValue(key: .totalExerciseOverviewViews)
    }
    
    override func analyticsViewDidDisappear() {
        Analytics.screenDidDissapear(screen: .contentPlayer, attributes: [AnalyticAttribute.productID:     String(workout.productID),
                                                                          AnalyticAttribute.workoutID:     String(workout.id),
                                                                          AnalyticAttribute.workoutType:   workout.type])
    }
}
