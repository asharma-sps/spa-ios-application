//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

private let kBackingBarHeight = CGFloat(20.0)
private let kSpinnerLabelHeight = CGFloat(40.0)

class StoredContentProgressView: UIView {
    
    static let suggestedHeight = kBackingBarHeight + kSpinnerLabelHeight
    private let progressBar = ProgressBar()
    private let spinnerLabel = SpinnerLabel()
    
    init() {
        super.init(frame: CGRect.zero)
        addSubview(progressBar)
        addSubview(spinnerLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(text: String, spinning: Bool, progress: Float) {
        spinnerLabel.text = text
        spinnerLabel.showingSpinner = spinning
        progressBar.amount = progress
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        progressBar.frame = CGRect(0.0, 0.0, frame.width, kBackingBarHeight)
        spinnerLabel.frame = CGRect(0.0, kBackingBarHeight, frame.width, kSpinnerLabelHeight)
    }
}

private class ProgressBar: UIView {
    
    private let backing = UIView()
    private let fill = UIView()
    var amount: Float = 0.0 {
        didSet {
            setNeedsLayout()
        }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        backing.backgroundColor = UIColor.white
        backing.layer.masksToBounds = true
        backing.layer.cornerRadius = kBackingBarHeight / 2
        addSubview(backing)
        fill.backgroundColor = UIColor.brandAccentBlue
        backing.addSubview(fill)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate override func layoutSubviews() {
        super.layoutSubviews()
        backing.frame = CGRect(0.0, 0.0, frame.width, kBackingBarHeight)
        fill.frame = CGRect(0.0, 0.0, frame.width * CGFloat(amount), kBackingBarHeight)
    }
}

private class SpinnerLabel: UIView {
    
    private let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    private let label = UILabel()
    var text = "" {
        didSet {
            label.text = text
            setNeedsLayout()
        }
    }
    var showingSpinner = false {
        didSet {
            showingSpinner ? spinner.startAnimating() : spinner.stopAnimating()
        }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        label.font = UIFont.brandRegular()
        addSubview(label)
        spinner.hidesWhenStopped = true
        addSubview(spinner)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate override func layoutSubviews() {
        super.layoutSubviews()
        let spinnerWidth = CGFloat(40.0)
        let labelWidth = min(label.sizeThatFits(CGSize(CGFloat.greatestFiniteMagnitude, CGFloat.greatestFiniteMagnitude)).width, frame.width - spinnerWidth * 2)
        let labelOriginX = (frame.width - labelWidth) / 2
        label.frame = CGRect(labelOriginX, 0.0, labelWidth, frame.height)
        spinner.frame = CGRect(labelOriginX - spinnerWidth, 0.0, spinnerWidth, frame.height)
    }
}
