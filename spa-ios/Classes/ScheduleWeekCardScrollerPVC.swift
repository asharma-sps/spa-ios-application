//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

protocol ScheduleWeekCardScrollerDelegate {
    func selectedScheduleWeekCardEntryDidChange(index: Int)
}

// MARK:- Setup
class ScheduleWeekCardScrollerPVC: UIPageViewController {

    fileprivate var selectionDelegate: ScheduleWeekCardScrollerDelegate?
    fileprivate var weekCardViewControllers: [UIViewController] = []
    fileprivate var currentIndex = 0
    fileprivate var nextIndex = 0

    init() {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
    }
}

// MARK:- Public
extension ScheduleWeekCardScrollerPVC {
    
    func setWeekStartDates(weekStartDates: [Date], pool: ScheduleEntriesPool, weekTableViewDelegate: WeekTableViewDelegate) {
        weekCardViewControllers = generateWeekCardViewControllers(weekStartDates: weekStartDates, pool: pool, weekTableViewDelegate: weekTableViewDelegate)
        if !weekCardViewControllers.isEmpty { displayCardAtIndex(index: 0) }
    }
    
    func setSelectionDelegate(delegate: ScheduleWeekCardScrollerDelegate) {
        self.selectionDelegate = delegate
    }
    
    func setSelectedCard(index: Int) { // Unused... maybe scrap this?
        if weekCardViewControllers.count - 1 <= index { return }
        let vc = weekCardViewControllers[index]
        setViewControllers([vc], direction: .forward, animated: false, completion: nil)
        
    }
}

// MARK:- Internal
extension ScheduleWeekCardScrollerPVC {
    
    fileprivate func displayCardAtIndex(index: Int) {
        self.setViewControllers([weekCardViewControllers[index]], direction: .forward, animated: false, completion: nil)
    }
    
    fileprivate func generateWeekCardViewControllers(weekStartDates: [Date], pool: ScheduleEntriesPool, weekTableViewDelegate: WeekTableViewDelegate) -> [WeekCardVC] {
        var cards: [WeekCardVC] = []
        for date in weekStartDates {
            let card = WeekCardVC(weekStartDate: date, pool: pool, weekTableViewDelegate: weekTableViewDelegate)
            cards.append(card)
        }
        return cards
    }
}

// MARK:- UIPageViewController Delegates
extension ScheduleWeekCardScrollerPVC: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = indexOfWeekCardViewController(card: viewController as! WeekCardVC) else { return nil }
        if currentIndex + 1 > weekCardViewControllers.count - 1 { return nil }
        let nextCard = weekCardViewControllers[currentIndex + 1]
        return nextCard
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = indexOfWeekCardViewController(card: viewController as! WeekCardVC) else { return nil }
        if currentIndex - 1 < 0 || weekCardViewControllers.isEmpty { return nil }
        let priorCard = weekCardViewControllers[currentIndex - 1]
        return priorCard
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            guard let controllers = pageViewController.viewControllers else { return }
            if controllers.isEmpty { return }
            guard let index = indexOfWeekCardViewController(card: controllers[0] as! WeekCardVC) else { return }
            selectionDelegate?.selectedScheduleWeekCardEntryDidChange(index: index)
        }
    }
    
    func indexOfWeekCardViewController(card: WeekCardVC) -> Int? {
        guard let index = weekCardViewControllers.index(of: card) else { return nil }
        return index
    }
}
