//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

private let kTabletCellSpacing = CGFloat(15.0)

// MARK:- Setup
class ProductsVC: NetworkViewController {
    
    override var shouldAutorotate: Bool { return false }
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask { return UIInterfaceOrientationMask.portrait }
    fileprivate let session: Session
    fileprivate let requestHandler = ProductsRequestHandler()
    fileprivate let collectionView = UICollectionView(frame:CGRect.zero, collectionViewLayout:UICollectionViewFlowLayout())
    fileprivate var preventPush = false // Prevents "double-pushing" view controllers if user double taps while waiting.
    fileprivate var line = UIView()

    init(session: Session) {
        self.session = session
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupCollectionView()
        setupLine()
        registerForProductsPoolChanges()
        requestProducts()
    }
    
    private func setupView() {
        navigationItem.title = "Programs"
        setupBlankBackButton()
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = Environment.isTablet ? UIColor.brandUltraDarkGray : UIColor.groupTableViewBackground
        collectionView.register(ProductsCollectionViewCell.self, forCellWithReuseIdentifier: ProductsCollectionViewCell.reuseID)
        let flow = generateCollectionViewFlow(screenSize: view.frame.size)
        collectionView.setCollectionViewLayout(flow, animated: false)
        mainView.addSubview(collectionView)
        if Environment.isTablet {
            collectionView.contentInset = UIEdgeInsets(top: kTabletCellSpacing, left: kTabletCellSpacing, bottom: kTabletCellSpacing, right: kTabletCellSpacing)
        }
    }
    
    private func generateCollectionViewFlow(screenSize: CGSize) -> UICollectionViewFlowLayout {
        if Environment.isTablet {
            let flow = UICollectionViewFlowLayout()
            let sideLength = (screenSize.width - kTabletCellSpacing * 4) / 3
            let cellSize = CGSize(sideLength, sideLength)
            flow.estimatedItemSize = cellSize
            flow.itemSize = cellSize
            flow.sectionInset = UIEdgeInsets.zero
            flow.minimumLineSpacing = kTabletCellSpacing
            flow.minimumInteritemSpacing = kTabletCellSpacing
            return flow
        }
        
        let flow = UICollectionViewFlowLayout()
        let cellSize = CGSize(width: screenSize.width / 2, height: screenSize.width / 2)
        flow.estimatedItemSize = cellSize
        flow.itemSize = cellSize
        flow.sectionInset = UIEdgeInsets.zero
        flow.minimumLineSpacing = 0.0
        flow.minimumInteritemSpacing = 0.0
        return flow
    }
    
    private func setupLine() {
        if Environment.isTablet {
            line.backgroundColor = UIColor.brandUltraLightGray
            mainView.addSubview(line)
        }
    }
    
    private func registerForProductsPoolChanges() {
        session.productsPool.addObserver(self)
    }
    
    fileprivate func requestProducts() {
        baseViewControllerState = .loadingView
        session.productsPool.reloadProducts(user: session.user)
    }
}

// MARK:- Layout
extension ProductsVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = CGRect(0.0, 0.0, mainView.frame.width, mainView.frame.height)
        line.frame = CGRect(0, mainView.frame.height - Environment.thinnestLine, mainView.frame.width, Environment.thinnestLine)
    }
}

// MARK:- UICollectionViewDataSource
extension ProductsVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return session.productsPool.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductsCollectionViewCell.reuseID, for: indexPath as IndexPath) as? ProductsCollectionViewCell else {
            return UICollectionViewCell()
        }
        let product = session.productsPool.products[indexPath.row]
        cell.setupWithProduct(product: product, rounded: Environment.isTablet)
        return cell
    }
}

// MARK:- Product Loading
extension ProductsVC: ProductsObserver {
    
    func productsDidReload() {
        refreshCollectionView()
    }
    
    private func refreshCollectionView() {
        if session.productsPool.products.count > 0 {
            self.collectionView.reloadData()
            self.baseViewControllerState = .mainView
        } else {
            self.collectionView.reloadData()
            self.baseViewControllerState = .failureView
        }
    }
}

// MARK:- UICollectionViewDelegate
extension ProductsVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if preventPush { print("Double-push prevented"); return }
        let product = session.productsPool.products[indexPath.item]
        
        if product.id == 12 { // program 12 is a special diet program. Not available for sale in-app, but will appear if client purchased elsewhere.
            guard let PDFURL = product.resources?.first?.resource else { return }
            let viewer = PDFViewerVC(url: PDFURL, title: "Done For You Diet")
            viewer.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(viewer, animated: true)
        }
        else if product.type != .application {
            if product.purchasedState == .paid || product.purchasedState == .free {
                let phasesTVC = ProgramPhasesTVC(program: product, session: session)
                phasesTVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(phasesTVC, animated: true)
            }
            else {
                let demoPlayerVC = ProductVideoSalesLetterPlayerVC(program: product, session: session)
                demoPlayerVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(demoPlayerVC, animated: true)
            }
            preventPush = true
        }
    }
}

// MARK:- Helpers
extension ProductsVC {
    
    override func retryLoadingData() {
        requestProducts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        preventPush = false
    }
}

// MARK:- Analytics
extension ProductsVC {
    
    override func analyticsViewDidAppear() {
        Analytics.screenDidAppear(screen: .product)
    }
    
    override func analyticsViewDidDisappear() {
        Analytics.screenDidDissapear(screen: .product)
    }
}
