//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class ExerciseTableViewCell: UITableViewCell {
    
    static let reuseID = "exercise_cell_reuse_id"
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        accessoryType = .disclosureIndicator
        layer.masksToBounds = true
        configureLabels()
    }
    
    private func configureLabels() {
        textLabel?.font = UIFont.brandMedium(size: 18.0)
        detailTextLabel?.font = UIFont.brandRegular(size: 14.0)
        detailTextLabel?.textColor = UIColor.brandGray
        detailTextLabel?.numberOfLines = 1
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func prepare(exercise: Exercise) {
        textLabel?.textColor = UIColor.brandDarkGray
        textLabel?.text = exercise.name
        detailTextLabel?.text = exercise.typeDescription
        detailTextLabel?.numberOfLines = 1
    }
    
    func prepareAsPlayAllCell(forExerciseGroup: Bool) {
        textLabel?.textColor = UIColor.brandAccentBlue
        textLabel?.text = forExerciseGroup ? "Start Exercise Group Walkthrough" : "Start Workout Walkthrough"
        detailTextLabel?.text = forExerciseGroup ? "Walkthrough this group of exercises." : "Walkthrough this workout, exercise by exercise."
        detailTextLabel?.numberOfLines = 2
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
}
