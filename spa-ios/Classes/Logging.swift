//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

func errorPrint(_ message: String, _ error: Error) {
    if error.localizedDescription == "The Internet connection appears to be offline." {
        print("\n[ERROR]: \(message). The app appears to be offline... [END ERROR]\n")
    } else {
        print("\n[ERROR]: \(message)\n\(error)\n[END ERROR]\n")
    }
}

func responseErrorPrint(_ message: String, _ error: Error, _ responseData: Data?) {
    if error.localizedDescription == "The Internet connection appears to be offline." {
        print("\n[ERROR]: \(message). The app appears to be offline... [END ERROR]\n")
        return
    }
    if let unwrappedResponse = responseData {
        let responseString = String(data: unwrappedResponse, encoding: .utf8) ?? "(no response data found)"
        print("\n[ERROR]: \(message)\n\(error)\n\(responseString)\n[END ERROR]\n")
        return
    }
    print("\n[ERROR]: \(message)\n\(error)\n[END ERROR]\n")
}

func warningPrint(_ message: String) {
    print("[WARNING]: \(message)")
}
