//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

private let StoryboardName = "HomescreenCreateAccountTVC"

class HomescreenCreateAccountTVC: AnalyticBaseTableViewController, CheckoutCountryCodePickerViewDelegate {
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask { get { return UIInterfaceOrientationMask.portrait } }
    @IBOutlet weak var firstNameTextField: FormTextField!
    @IBOutlet weak var lastNameTextField: FormTextField!
    @IBOutlet weak var emailTextField: FormTextField!
    @IBOutlet weak var passwordTextField: FormTextField!
    @IBOutlet weak var phoneCodeTextField: FormPickerTextField!
    @IBOutlet weak var phoneTextField: FormTextField!
    @IBOutlet weak var createAccountButton: UIButton!
    var orderedTextFields = [FormTextField]()
    
    // MARK:- CheckoutCountryCodePickerViewDelegate
    var phoneCountryCode: String? {
        didSet {
            guard let code = phoneCountryCode else { phoneCodeTextField.text = nil; return }
            phoneCodeTextField.text = "+\(code)"
        }
    }
    
    class func generateHomescreenCreateAccountTableViewController() -> HomescreenCreateAccountTVC {
        let storyboard = UIStoryboard(name: StoryboardName, bundle: nil)
        let tvc = storyboard.instantiateInitialViewController() as? HomescreenCreateAccountTVC
        return tvc!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureTextFields()
    }
    
    private func configureView() {
        setupBlankBackButton()
        addCloseButton()
        createAccountButton.configure()
        createAccountButton.addTarget(self, action: #selector(createAccount), for: .touchUpInside)
        tableView.showsVerticalScrollIndicator = false
    }
    
    private func configureTextFields() {
        orderedTextFields = [firstNameTextField, lastNameTextField, emailTextField, passwordTextField, phoneCodeTextField, phoneTextField]
        for textField in orderedTextFields {
            textField.nextButtonDelegate = self
        }
        phoneTextField.setAccessoryTitle(accessoryTitle: .create)
        phoneCodeTextField.inputView = CheckoutCountryCodePV(delegate: self)
    }
    
    @objc fileprivate func createAccount() {
        dismissAnyFields()
        if !createAccountIfFieldsAreValid() {
            ASGlobalOverlay.showAlert(withTitle: "Missing Fields", message: "Please ensure all fields are completed.", dismissButtonTitle: "OK")
        }
    }
    
    private func createAccountIfFieldsAreValid() -> Bool {
        guard let first = firstNameTextField.validatedString() else { return false }
        guard let last = lastNameTextField.validatedString() else { return false }
        guard let email = emailTextField.validatedString() else { return false }
        guard let password = passwordTextField.validatedString() else { return false }
        guard let phoneNumber = phoneTextField.validatedString() else { return false }
        guard let phoneCode = phoneCountryCode, !phoneCode.isEmpty else { return false }
        
        ASGlobalOverlay.showWorkingIndicator(withDescription: nil)
        AuthenticationRequestHandler.registerUser(first: first, last: last, password: password, email: email, phoneCode: phoneCode, phoneNumber: phoneNumber) { result in
            switch result {
            case .success(let user):
                self.dismiss(animated: false, completion: nil) // prevents over retention, but I can't figure out why :(
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    appDelegate.setRootViewController(withUser: user)
                }
                ASGlobalOverlay.dismissWorkingIndicator()
            case .failure(let errorMessage):
                ASGlobalOverlay.showAlert(withTitle: "Error", message: errorMessage, dismissButtonTitle: "OK")
            }
        }
        return true
    }
}

extension HomescreenCreateAccountTVC: FormTextFieldDelegate {
    
    func nextButtonPressed(sender: FormTextField) {
        guard let index = orderedTextFields.index(of: sender) else { return }
        if index > orderedTextFields.count { return } // index + 1 cancels out orderedFields.count -1
        if index + 1 == orderedTextFields.count { createAccount(); sender.resignFirstResponder() }
        else { orderedTextFields[index + 1].becomeFirstResponder() }
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        dismissAnyFields()
    }
    
    fileprivate func dismissAnyFields() {
        for textField in orderedTextFields {
            textField.resignFirstResponder()
        }
    }
    
    override func dismissSelf() {
        super.dismissSelf()
        dismissAnyFields()
    }
}

// MARK:- Analytics
extension HomescreenCreateAccountTVC {
    
    override func analyticsViewDidAppear() {
        Analytics.screenDidAppear(screen: .createAccount)
    }
    
    override func analyticsViewDidDisappear() {
        Analytics.screenDidDissapear(screen: .createAccount)
    }
}
