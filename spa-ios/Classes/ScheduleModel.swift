//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation

struct ScheduleEntry {
    let id: Int
    let date: Date
    let dateString: String
    let workoutID: Int
    var workoutTitle: String?
    var programName: String?
    var programID: Int?
    var programLogo: String?
    
    init(id: Int, date: Date, dateString: String, workoutID: Int) {
        self.id = id
        self.date = date
        self.dateString = dateString
        self.workoutID = workoutID
        self.workoutTitle = nil
        self.programName = nil
        self.programID = nil
        self.programLogo = nil
    }
}

struct WorkoutProgram {
    let id: Int
    let name: String
    let subtype: WorkoutSubtype
    let logo: String?
    let outlines: [WorkoutOutline]
    var isScheduled: Bool
    init(id: Int, name: String, subtype: WorkoutSubtype, logo: String?, outlines: [WorkoutOutline]) {
        self.id = id
        self.name = name
        self.subtype = subtype
        self.logo = logo
        self.outlines = outlines
        self.isScheduled = false
    }
}

enum WorkoutSubtype: String {
    case program = "program"
    case nutritional = "nutritional"
    case supplement = "supplement"
    case compilation = "compilation"
    case application = "application"
}

struct WorkoutOutline {
    let title: String?
    let id: Int?
}

struct ScheduleWeekPattern {
    let first: Bool
    let second: Bool
    let third: Bool
    let fourth: Bool
    let fifth: Bool
    let sixth: Bool
    let seventh: Bool
    func valueForIndex(index: Int) -> Bool {
        switch index {
        case 0: return first
        case 1: return second
        case 2: return third
        case 3: return fourth
        case 4: return fifth
        case 5: return sixth
        case 6: return seventh
        default: return false
        }
    }
}
