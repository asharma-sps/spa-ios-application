//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

let kWeekCardViewPadding = CGFloat(20.0)
fileprivate let kTopLabelHeight = CGFloat(20)
fileprivate let kBottomLabelHeight = CGFloat(30)
fileprivate let kLabelTopBottomPadding = CGFloat(15)
fileprivate let kLabelSidePadding = CGFloat(15.0)
fileprivate let kHeaderHeight = kTopLabelHeight + kBottomLabelHeight + kLabelTopBottomPadding * 2

class WeekCardVC: UIViewController {
    
    let containerView = UIView()
    let topLabel = UILabel()
    let bottomLabel = UILabel()
    let weekTableView: WeekTV
    let weekStartDate: Date
    let pool: ScheduleEntriesPool

    var weekTVEntries = [[ScheduleEntry]]()
    
    init(weekStartDate: Date,  pool: ScheduleEntriesPool, weekTableViewDelegate: WeekTableViewDelegate) {
        self.weekStartDate = weekStartDate
        self.pool = pool
        weekTableView = WeekTV(startDate: weekStartDate, pool: pool, delegate: weekTableViewDelegate)
        super.init(nibName: nil, bundle: nil)
        setupContainerView()
        setupWeekTableView()
        setupTopLabel()
        setupBottomLabel()
    }
    
    fileprivate func setupContainerView() {
        containerView.backgroundColor = UIColor.brandAccentBlue
        containerView.layer.masksToBounds = true
        containerView.layer.cornerRadius = 3.0
        view.addSubview(containerView)
    }
    
    fileprivate func setupWeekTableView() {
        containerView.addSubview(weekTableView)
    }
    
    fileprivate func setupTopLabel() {
        topLabel.font = UIFont.brandRegular(size: 16.0)
        topLabel.textColor = UIColor.white
        topLabel.text = "WEEK STARTING"
        containerView.addSubview(topLabel)
    }
    
    fileprivate func setupBottomLabel() {
        bottomLabel.font = UIFont.brandMedium(size: 25)
        bottomLabel.textColor = UIColor.white
        bottomLabel.text = ScheduleDateHelper.userDateString(date: weekStartDate)
        containerView.addSubview(bottomLabel)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerView.frame = CGRect(kWeekCardViewPadding, kWeekCardViewPadding, view.frame.width - kWeekCardViewPadding * 2, view.frame.height - kWeekCardViewPadding * 2)
        let labelWidth = containerView.frame.width - kLabelSidePadding * 2
        topLabel.frame = CGRect(kLabelSidePadding, kLabelTopBottomPadding, labelWidth, kTopLabelHeight)
        bottomLabel.frame = CGRect(kLabelSidePadding, topLabel.frame.maxY, labelWidth, kBottomLabelHeight)
        weekTableView.frame = CGRect(0.0, kHeaderHeight, containerView.frame.width, containerView.frame.height - kHeaderHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
