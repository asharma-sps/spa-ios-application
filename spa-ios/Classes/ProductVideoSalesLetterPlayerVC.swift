//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

// MARK:- Setup
class ProductVideoSalesLetterPlayerVC: BaseVideoPlayerVC {
    
    let session: Session
    let product: Product
    
    init(program: Product, session: Session) {
        self.product = program
        self.session = session
        super.init(buttonTitle: "Buy", productID: program.id)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupDemoButton()
        setupDescriptionLabel()
        setupPlayer()
    }
    
    private func setupView() {
        title = product.name
    }
    
    private func setupDemoButton() {
        guard let demoSection = extractDemoSection(program: product) else { return }
        if demoSection.workouts.isEmpty { return }
        let clearButton = UIBarButtonItem(title: "Demo", style: .plain, target: self, action: #selector(showDemoVideos))
        navigationItem.rightBarButtonItem = clearButton
    }
    
    private func setupDescriptionLabel() {
        guard let promoWorkout = extractPromoWorkout(program: product) else { return }
        setDescriptionText(HTMLString: promoWorkout.description)
    }
    
    @objc private func showDemoVideos() {
        guard let demoSection = extractDemoSection(program: product) else { return }
        if demoSection.workouts.isEmpty { return }
        let workoutsTVC = ProgramWorkoutsTVC(section: demoSection, titleText: .DemoVideos, session: session)
        navigationController?.pushViewController(workoutsTVC, animated: true)
    }
    
    private func setupPlayer() {
        guard let promoWorkout = extractPromoWorkout(program: product) else { return }
        let url = promoWorkout.video.preferedResolution
        guard !url.isEmpty else { return }
        play(videoURL: url)
    }
}

// MARK:- Content Extractions
extension ProductVideoSalesLetterPlayerVC {
    
    fileprivate func extractPromoWorkout(program: Product) -> Workout? {
        guard let promoSection = program.sections.first else { return nil }
        guard let promoWorkout = promoSection.workouts.first else { return nil } // It appears that the promo workout is simple place in the first section as the first workout.
        return promoWorkout
    }
    
    fileprivate func extractDemoSection(program: Product) -> Section? {
        if program.sections.count < 2 { return nil }
        return program.sections[1]
    }
}

// MARK:- Proceed To Checkout
extension ProductVideoSalesLetterPlayerVC {
    
    override func buttonPressed() {
        let checkoutTVC = CheckoutTVC.generateCheckoutTableViewController(product: product, session: session)
        navigationController?.pushViewController(checkoutTVC, animated: true)
    }
}

// MARK:- Analytics
extension ProductVideoSalesLetterPlayerVC {
    
    override func analyticsViewDidAppear() {
        Analytics.screenDidAppear(screen: .vslPlayer)
    }
    
    override func analyticsViewDidDisappear() {
        Analytics.screenDidDissapear(screen: .vslPlayer, attributes: [AnalyticAttribute.product:   product.name,
                                                                      AnalyticAttribute.medium:    product.medium.rawValue])

    }
}
