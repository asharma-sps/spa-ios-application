//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//
import Foundation
import ASGlobalOverlay

private let kProgressOnboardingView = "ProgressOnboardingView"

protocol ProgressOnboardingViewDelegate {
    func startTrackingButtonPressed()
}

class ProgressOnboardingView: UIView {
    
    var delegate: ProgressOnboardingViewDelegate?
    
    class func generateProgressOnboardingViewFromNib(delegate: ProgressOnboardingViewDelegate) -> ProgressOnboardingView {
        guard let view = Bundle.main.loadNibNamed(kProgressOnboardingView, owner: self, options: nil)!.first as? ProgressOnboardingView else {
            return ProgressOnboardingView()
        }
        view.delegate = delegate
        return view
    }
    
    @IBAction func startTrackingButtonPressed(_ sender: AnyObject) {
        delegate?.startTrackingButtonPressed()
    }
}
