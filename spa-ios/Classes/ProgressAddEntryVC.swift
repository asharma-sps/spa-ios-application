//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import ASGlobalOverlay

private let StoryboardName = "ProgressAddEntryVC"
private let kWeightTextFieldPlaceholder = "(tap here to set weight)"
typealias RefreshCompletion = () -> Void

private enum ControllerState {
    case photoAdded
    case editingWeight
    case other
}

protocol ProgressAddEntryViewControllerDelegate {
    func refreshProgressEntriesData(completion: @escaping RefreshCompletion)
}

// MARK:- Setup
class ProgressAddEntryVC: UIViewController, ImageSelectionManagerDelegate, WeightPickerViewDelegate {
    
    // Orientation Management
    override var shouldAutorotate: Bool { return false }
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask { return UIInterfaceOrientationMask.portrait }
    
    // IBOutlets
    @IBOutlet weak var weightTextField: CaretlessTextField!
    @IBOutlet weak var addPhotoButton: UIButton!
    @IBOutlet weak var photoContainerView: UIView!
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var changePhotoButton: UIButton!
    @IBOutlet weak var saveEntryButton: UIButton!
    
    // Passed Arguments
    var session: Session?
    fileprivate var lastEntry: ProgressEntry?
    fileprivate var unit: WeightUnit?
    fileprivate var delegate: ProgressAddEntryViewControllerDelegate?
    
    // Other Properties
    fileprivate let progressService = ProgressRequestHandler()
    fileprivate let previewImageView = UIImageView()
    fileprivate let imageSelectionManager = ImageSelectionManager()
    fileprivate var entryPhoto: UIImage?
    fileprivate var weightPicker: WeightPickerView?
    fileprivate var selectedWeightString: String? {
        didSet {
            weightTextField.text = selectedWeightString
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addCloseButton()
        setupSaveEntryButton()
        setupWeightTextField()
        setupPreviewImageView()
        configureView(state: .other, animated: false)
    }
    
    private func setupSaveEntryButton() {
        saveEntryButton.backgroundColor = UIColor.brandAccentBlue
    }
    
    private func setupWeightTextField() {
        let done = UIButton.generateAccessoryInputButton(title: "Done", target: self, selector: #selector(donePressed))
        weightPicker = WeightPickerView(unit: unit ?? .Pounds, delegate: self)
        weightTextField.inputAccessoryView = done
        weightTextField.inputView = weightPicker
        weightTextField.delegate = self
        weightTextField.text = kWeightTextFieldPlaceholder
        weightTextField.inputAssistantItem.leadingBarButtonGroups = []
        weightTextField.inputAssistantItem.trailingBarButtonGroups = []
    }
    
    private func setupPreviewImageView() {
        previewImageView.clipsToBounds = true
        previewImageView.layer.cornerRadius = 3.0
        previewImageView.contentMode = .scaleAspectFill
        photoContainerView.addSubview(previewImageView)
    }
    
    // MARK:- ImageSelectionManagementDelegate
    func presentImagePickerController(imagePicker: UIImagePickerController) {
        self.present(imagePicker, animated: true, completion:nil)
    }
    
    // MARK:- WeightPickerViewDelegate
    func weightSelectionChange(description: String) {
        selectedWeightString = description
    }
}

// MARK:- State Controller {
extension ProgressAddEntryVC {
    
    fileprivate func configureView(state: ControllerState, animated: Bool) {
        let duration = animated ? 0.4 : 0.0
        UIView.animate(withDuration: duration) {
            switch state {
            case .other:
                self.photoLabel.alpha = 1.0
                self.addPhotoButton.alpha = 1.0
                self.photoContainerView.alpha = 1.0
                self.previewImageView.alpha = 1.0
                self.changePhotoButton.alpha = 0.0
            case .photoAdded:
                self.photoLabel.alpha = 1.0
                self.addPhotoButton.alpha = 0.0
                self.photoContainerView.alpha = 1.0
                self.previewImageView.alpha = 1.0
                self.changePhotoButton.alpha = 1.0
            case .editingWeight:
                self.photoLabel.alpha = 0.0
                self.addPhotoButton.alpha = 0.0
                self.photoContainerView.alpha = 0.0
                self.previewImageView.alpha = 0.0
                self.changePhotoButton.alpha = 0.0
            }
            self.saveEntryButton.alpha = self.selectedWeightString != nil ? 1.0 : 0.0
        }
    }
}

// MARK:- IBActions
extension ProgressAddEntryVC {

    @IBAction func changePhotoButtonPressed(_ sender: AnyObject) {
        promptUserToSelectPhoto()
    }
    
    @IBAction func addPhotoButtonPressed(_ sender: AnyObject) {
        promptUserToSelectPhoto()
    }
    
    @IBAction func saveEntryButtonPressed(_ sender: AnyObject) {
        weightTextField.resignFirstResponder()
        validateProgressEntryAndSave()
    }
}

// MARK:- Photo Selection
extension ProgressAddEntryVC {

    fileprivate func promptUserToSelectPhoto() {
        imageSelectionManager.promptUserToSelectPhoto(delegate: self, prompt: nil) { [weak self] (userCancelled, success, image) in
            if userCancelled { return }
            if !success || image == nil { ASGlobalOverlay.showAlert(withTitle: "Error", message: "An error occurred! Try another photo.", dismissButtonTitle: "OK")}
            if self != nil {
                self?.entryPhoto = image
                self?.previewImageView.image = image
                self?.view.setNeedsLayout()
                self?.configureView(state: .photoAdded, animated: false)
            } else {
                ASGlobalOverlay.showAlert(withTitle: "Error", message: "An error occurred! Try another photo.", dismissButtonTitle: "OK")
            }
        }
    }
}

// MARK:- Save Progress Entry
extension ProgressAddEntryVC {
    
    fileprivate func validateProgressEntryAndSave() {
        guard let weightText = selectedWeightString else {
            ASGlobalOverlay.showAlert(withTitle: "Missing Information", message: "You must specify your current weight to add a progress entry.", dismissButtonTitle: "OK");
            return
        }
        guard let photo = entryPhoto else {
            ASGlobalOverlay.showAlert(withTitle: "Missing Photo", message: "You must add a progress photo to save a progress entry.", dismissButtonTitle: "OK");
            return
        }
        let normalizedPhoto = photo.normalizedOrientation()
        saveProgressEntry(weight: weightText, image: normalizedPhoto)
    }
    
    private func saveProgressEntry(weight: String, image: UIImage) {
        guard let user = session?.user else {
            print("PRECONDITON FAILURE- session was not injected into ProgressEntryVC.")
            return
        }
        ASGlobalOverlay.showWorkingIndicator(withDescription: "Saving")
        progressService.postProgressEntry(user: user, image: image, weight: weight) { (success) in
            if success {
                guard let delegate = self.delegate else {
                    ASGlobalOverlay.showAlert(withTitle: "Progress Saved!", message: nil, forTimePeriod: 2.0)
                    self.dismissSelf()
                    return
                }
                delegate.refreshProgressEntriesData(completion: {
                    ASGlobalOverlay.showAlert(withTitle: "Progress Saved!", message: nil, forTimePeriod: 2.0)
                    self.dismissSelf()
                })
            } else {
                ASGlobalOverlay.showAlert(withTitle: "Error", message: "An error occurred. Do 5 pushups then try again.", dismissButtonTitle: "OK")
            }
        }
    }
}

// MARK:- Layout
extension ProgressAddEntryVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // assumes portrait image
        if let image = previewImageView.image {
            let ratio = image.size.height / image.size.width
            let maxHeight = photoContainerView.frame.height - 20.0
            let imageSize = CGSize(maxHeight / ratio, maxHeight)
            let previewImageOriginX = (photoContainerView.frame.width - imageSize.width) / 2
            previewImageView.frame = CGRect(previewImageOriginX, 10.0, imageSize.width, imageSize.height)
        }
    }
}

// MARK:- Load From Storyboard
extension ProgressAddEntryVC {
    
    class func generateProgressAddEntryViewController(session: Session, delegate: ProgressAddEntryViewControllerDelegate, lastEntry: ProgressEntry?, unit: WeightUnit) -> ProgressAddEntryVC {
        let storyboard = UIStoryboard(name: StoryboardName, bundle: nil)
        let progressAddEntryVC = storyboard.instantiateInitialViewController() as? ProgressAddEntryVC!
        progressAddEntryVC!.delegate = delegate
        progressAddEntryVC!.session = session
        progressAddEntryVC!.lastEntry = lastEntry
        return progressAddEntryVC!
    }
}

// MARK:- UITextFieldDelegate
extension ProgressAddEntryVC: UITextFieldDelegate {
    
    @objc fileprivate func donePressed() {
        weightPicker?.updateDelegate()
        weightTextField.resignFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if entryPhoto == nil { configureView(state: .other, animated: true) }
        else { configureView(state: .photoAdded, animated: true) }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        configureView(state: .editingWeight, animated: true)
        if weightTextField.text == kWeightTextFieldPlaceholder {
            if let entry = lastEntry {
                weightPicker?.selectPickerRowsForProgressEntry(entry: entry)
                weightTextField.text = weightPicker?.selectedWeight()
            }
        }
    }
}

// Mark:- Helpers
extension ProgressAddEntryVC {
    
    @objc override func dismissSelf() {
        self.weightTextField.resignFirstResponder()
        super.dismissSelf()
    }
}
