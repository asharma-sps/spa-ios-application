//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//


import Foundation

import UIKit

// MARK:- Memory Cache Singleton
class ImageCache: NSCache<AnyObject, AnyObject> {
    static let singleton = ImageCache()
}

// MARK:- Network Image View
class NetworkImageView: UIImageView {
    
    fileprivate var currentImageURL = String()

    func setImageFromURL(imageURL: URL) {
        
        // checks if any action if needed
        if imageURL.absoluteString == currentImageURL { return }
        image = nil
        currentImageURL = imageURL.absoluteString
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            // checks for image in memory cache
            if let memCachedImage = ImageCache.singleton.object(forKey: imageURL.absoluteString as NSString) as? UIImage {
                DispatchQueue.main.async {
                    if self.currentImageURL == imageURL.absoluteString { self.image = memCachedImage }
                    self.internalDebugPrint("image found in mem cache")
                }
                return
            }
            
            // extracts image name from URL. If the operation fails, the image is fetched from the network.
            guard let imageName = self.imageNameForURL(url: imageURL) else {
                guard let downloadedImageData = try? Data(contentsOf: imageURL) else {
                    self.internalDebugPrint("image could not be fetched from network!")
                    return
                }
                guard let downloadedImage = UIImage(data: downloadedImageData) else {
                    self.internalDebugPrint("could not convert downloaded image data into an image")
                    return
                }
                ImageCache.singleton.setObject(downloadedImage, forKey: imageURL.absoluteString as NSString)
                DispatchQueue.main.async {
                    if self.currentImageURL == imageURL.absoluteString { self.image = downloadedImage }
                    self.internalDebugPrint("image fetched from network, but couldn't be saved to the disk!")
                }
                return
            }
            
            // checks for image in disk cache
            if let diskCachedImageData = self.loadImageFromCache(name: imageName){
                ImageCache.singleton.setObject(diskCachedImageData, forKey: imageURL.absoluteString as NSString)
                DispatchQueue.main.async {
                    if self.currentImageURL == imageURL.absoluteString { self.image = diskCachedImageData }
                    self.internalDebugPrint("image found in disk cache")
                }
                return
            }
            
            // fetches image from network
            if let imageData = try? Data(contentsOf: imageURL) {
                guard let image = UIImage(data: imageData) else { return }
                self.saveImageToCache(name: imageName, image: image)
                ImageCache.singleton.setObject(image, forKey: imageURL.absoluteString as NSString)
                DispatchQueue.main.async {
                    if self.currentImageURL == imageURL.absoluteString { self.image = image }
                    self.internalDebugPrint("image fetched from network!")
                }
            }
        }
    }
    
    func clearImage() {
        image = nil
        currentImageURL = ""
    }
}

// MARK:- Disk Cache Helpers
extension NetworkImageView {

    fileprivate func saveImageToCache(name: String, image: UIImage) {
        guard let imageData = UIImagePNGRepresentation(image) else { return }
        let imageFilePath = diskImageCacheFilePath().appendingPathComponent(name)
        do {
            try imageData.write(to: imageFilePath, options: .atomicWrite)
            self.internalDebugPrint("image was cached to disk: \(imageFilePath)")
        } catch let error {
            print("ERROR: Failed to save image to disk cache. \(error)")
        }
    }
    
    fileprivate func loadImageFromCache(name: String) -> UIImage? {
        let imagePath = diskImageCachePath().appendingPathComponent(name)
        let imageData = UIImage(contentsOfFile: imagePath.absoluteString)
        return imageData
    }
    
    fileprivate func imageNameForURL(url: URL) -> String? {
        let name = url.lastPathComponent
        guard name != "" else { return nil }
        return name
    }
    
    fileprivate func internalDebugPrint(_ text: String) { // For easy debug logging on/off
        // print(text)
    }
}

// MARK:- Cache Path Helpers
extension NetworkImageView {
    
    class func ensureImageDiskCacheDirectoryExist() {
        let niv = NetworkImageView()
        _ = niv.internalEnsureDiskImageCacheDirectoryExist()
    }
    
    class func purgeImageDiskCacheDirectory() {
        let niv = NetworkImageView()
        PersistanceHelper.delete(path: niv.diskImageCacheFilePath())
    }
    
    private func internalEnsureDiskImageCacheDirectoryExist() -> URL? {
        let path = diskImageCachePath()
        guard PersistanceHelper.createDirectoryIfNonExistant(path: path) else { return nil }
        return path
    }
    
    fileprivate func diskImageCachePath() -> URL {
        return URL.documentDirectoryPath().appendingPathComponent("stored_images")
    }
    
    fileprivate func diskImageCacheFilePath() -> URL {
        return URL(fileURLWithPath: diskImageCachePath().absoluteString, isDirectory: true, relativeTo: nil)
    }
}
