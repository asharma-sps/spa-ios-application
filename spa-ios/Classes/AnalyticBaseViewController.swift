//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

class AnalyticBaseViewController: UIViewController {
    
    private static weak var mostRecentlyAppered: UIViewController?
    private var isVisible = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(viewDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(viewDidEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    @objc private func viewDidBecomeActive() {
        if self == AnalyticBaseViewController.mostRecentlyAppered && isVisible {
            print("[ACTIVE]: The view that appeared most recently is: \(self)")
            analyticsViewDidAppear()
        }
    }
    
    @objc private func viewDidEnterBackground() {
        if self == AnalyticBaseViewController.mostRecentlyAppered && isVisible {
            print("[BACKGROUND]: The view that appeared most recently is: \(self)")
            analyticsViewDidDisappear()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isVisible = true
        AnalyticBaseViewController.mostRecentlyAppered = self
        analyticsViewDidAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        isVisible = false
        analyticsViewDidDisappear()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
}

extension AnalyticBaseViewController {

    func analyticsViewDidAppear() {
        warningPrint("analyticViewDidAppear() was not overriden by AnalyticBaseViewController subclass")
    }
    
    func analyticsViewDidDisappear() {
        warningPrint("analyticsViewDidDisappear() was not overriden by AnalyticBaseViewController subclass")
    }
}
