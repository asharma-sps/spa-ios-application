//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import AVFoundation
import AVKit

class ExerciseDetailVC: AnalyticBaseViewController {
    
    fileprivate let exampleVideoPlayer = AVPlayerViewController()
    fileprivate let descriptionTextView = UITextView()
    fileprivate let exercise: Exercise
    fileprivate let exerciseInfoBanner: ExerciseInfoBannerView
    fileprivate let productID: Int
    fileprivate let usedForExerciseCard: Bool
    fileprivate var timerView: TimerView?
    
    init(exercise: Exercise, productID: Int, usedForExerciseCard: Bool) {
        self.productID = productID
        self.exercise = exercise
        self.usedForExerciseCard = usedForExerciseCard
        self.exerciseInfoBanner = ExerciseInfoBannerView(exercise: exercise)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupExampleVideoPlayer()
        setupExerciseInfoBanner()
        setupDescriptionTextView()
    }
    
    private func configureView() {
        title = exercise.name
        view.backgroundColor = UIColor.white
    }
    
    private func setupExampleVideoPlayer() {
        exampleVideoPlayer.configureForLoopingVideo()
        addChildViewController(exampleVideoPlayer)
        view.addSubview(exampleVideoPlayer.view)
        exampleVideoPlayer.didMove(toParentViewController: self)
    }
    
    private func setupExerciseInfoBanner() {
        exerciseInfoBanner.delegate = self
        view.addSubview(exerciseInfoBanner)
    }
    
    private func setupDescriptionTextView() {
        descriptionTextView.text = exercise.description
        descriptionTextView.font = UIFont.brandRegular(size: 16.0)
        descriptionTextView.textColor = UIColor.brandDarkGray
        descriptionTextView.alwaysBounceVertical = true
        descriptionTextView.backgroundColor = UIColor.white
        descriptionTextView.isEditable = false
        descriptionTextView.textContainerInset = UIEdgeInsets(top: 15.0, left: 15.0, bottom: 15.0, right: 15.0)
        view.addSubview(descriptionTextView)
    }
    
     // I think we're leaking memory somewhere here... Does this help? look into this... #ASTODO
    deinit {
        NotificationCenter.default.removeObserver(self)
        exampleVideoPlayer.player = nil
    }
}

// MARK:- ExerciseInfoBannerViewDelegate & Timer Management
extension ExerciseDetailVC: ExerciseInfoBannerViewDelegate {
    
    func timerModeButtonToggled(selected: Bool) {
        toggleTimerMode(selected: selected)
    }
    
    fileprivate func toggleTimerMode(selected: Bool) {
        if selected {
            descriptionTextView.isHidden = true
            let unwrappedTimerView = TimerView(seconds: exercise.reps, completionText: "Times Up!")
            timerView = unwrappedTimerView
            view.addSubview(unwrappedTimerView)
            timerView?.start()
        } else {
            timerView?.stop()
            timerView?.removeFromSuperview()
            timerView = nil
            descriptionTextView.isHidden = false
        }
    }
}

// MARK:- Player Management
extension ExerciseDetailVC {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startLoopingExampleVideo()
    }
    
    private func startLoopingExampleVideo() {
        if let player = exampleVideoPlayer.player {
            player.play()
            return
        }
        
        stopObservingVideoEndedNotification()
        guard let videoURL = determineVideoURL() else { return }
        let player = AVPlayer(url: videoURL)
        player.allowsExternalPlayback = false
        player.actionAtItemEnd = .none
        player.isMuted = true
        exampleVideoPlayer.player = player
        player.play()
        startObservingVideoEndedNotification()
    }
    
    func determineVideoURL() -> URL? {
        if let url = localVideoPath() { return url }
        return URL(string: exercise.video.preferedResolution)
    }
    
    private func localVideoPath() -> URL? {
        let resourceHelper = StoredContentResourceHelper(productID: productID)
        guard let url = URL(string: exercise.video.urlCachedFrom) else { return nil }
        let path = resourceHelper.filePathForVideoResource(remoteURL: url)
        if FileManager.default.fileExists(atPath: path.path) {
            return path
        }
        return nil
    }
    
    @objc func restartVideo() {
        exampleVideoPlayer.player?.seek(to: kCMTimeZero)
        exampleVideoPlayer.player?.play()
    }
    
    private func stopObservingVideoEndedNotification() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    private func startObservingVideoEndedNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(restartVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: exampleVideoPlayer.player?.currentItem)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        exampleVideoPlayer.player?.pause()
        toggleTimerMode(selected: false)
        exerciseInfoBanner.reset()
    }
}

// MARK:- Layout
extension ExerciseDetailVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let examplePlayerHeight = view.frame.width * CGFloat(0.562) // 16:9
        exampleVideoPlayer.view.frame = CGRect(0.0, 0.0, view.frame.width, examplePlayerHeight)
        exerciseInfoBanner.frame = CGRect(0.0, examplePlayerHeight, view.frame.width, ExerciseInfoBannerView.suggestedHeight)
        let descriptionTextViewOriginY = examplePlayerHeight + ExerciseInfoBannerView.suggestedHeight
        descriptionTextView.frame = CGRect(0.0, examplePlayerHeight + ExerciseInfoBannerView.suggestedHeight, view.frame.width, view.frame.height - descriptionTextViewOriginY)
        let timerHeight = view.frame.height - descriptionTextViewOriginY - 40
        timerView?.frame = CGRect(20, descriptionTextViewOriginY + 20, view.frame.width - 40, timerHeight)
    }
}

// MARK:- Analytics
extension ExerciseDetailVC {
    
    // if "usedForExerciseCard == true", the instance is being used as a child view controller, and the relevant tracking data is reported by the parent vc
    
    override func analyticsViewDidAppear() {
        if usedForExerciseCard { return }
        Analytics.screenDidAppear(screen: .exerciseDetail)
        Analytics.incrementProfileAttributeValue(key: .totalExerciseDetailViews)
        exampleVideoPlayer.player?.play() // Not actually related to analytics, just piggybacking off the fact that this method is invoked when the app becomes active and the view is visible.
    }
    
    override func analyticsViewDidDisappear() {
        if usedForExerciseCard { return }
        Analytics.screenDidDissapear(screen: .exerciseDetail, attributes: [AnalyticAttribute.exerciseID: exercise.id])
    }
}
