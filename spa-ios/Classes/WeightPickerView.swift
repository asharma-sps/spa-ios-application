//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

protocol WeightPickerViewDelegate {
    func weightSelectionChange(description: String)
}

class WeightPickerView: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let unit: WeightUnit
    let values: (firstMax: Int, firstMin: Int, secondMax: Int, secondMin: Int)
    let selectionDelegate: WeightPickerViewDelegate
    
    init(unit: WeightUnit, delegate: WeightPickerViewDelegate) {
        self.unit = unit
        self.selectionDelegate = delegate
        values = unit == .Pounds ? (firstMax: 499, firstMin: 60, secondMax: 9, secondMin: 0) : (firstMax: 219, firstMin: 30, secondMax: 9, secondMin: 0)
        super.init(frame: CGRect.zero)
        self.delegate = self
        self.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateDelegate() {
        selectionDelegate.weightSelectionChange(description: selectedWeight())
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 { return values.firstMax - values.firstMin + 1}
        return values.secondMax - values.secondMin + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 { return String(row + values.firstMin) }
        return String(row + values.secondMin)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectionDelegate.weightSelectionChange(description: selectedWeight())
    }
    
    func selectedWeight() -> String {
        guard let first = self.pickerView(self, titleForRow: selectedRow(inComponent: 0), forComponent: 0) else { return "" }
        guard let second = self.pickerView(self, titleForRow: selectedRow(inComponent: 1), forComponent: 1) else { return "" }
        return first + "." + second + " " + unit.rawValue
    }
    
    func selectPickerRowsForProgressEntry(entry: ProgressEntry) {
        guard let components = entry.seperateWeightStringComponents() else { return }
        selectRow(components.whole - values.firstMin, inComponent: 0, animated: false)
        selectRow(components.decimal - values.secondMin, inComponent: 1, animated: false)
    }
}
