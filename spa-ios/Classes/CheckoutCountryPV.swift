//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

protocol CheckoutCountryPickerViewDelegate: class {
    var country: String? { get set }
}

class CheckoutCountryPV: UIPickerView {
    
    let countries = CheckoutCountriesHelper.generateCountries()
    weak var selectionDelegate: CheckoutCountryPickerViewDelegate?
    
    init(delegate: CheckoutCountryPickerViewDelegate) {
        self.selectionDelegate = delegate
        super.init(frame: CGRect.zero)
        self.delegate = self
        self.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CheckoutCountryPV: UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 { return "Select a Country" }
        return countries[row - 1].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectionDelegate?.country = row == 0 ? nil : countries[row - 1].name
    }
}
