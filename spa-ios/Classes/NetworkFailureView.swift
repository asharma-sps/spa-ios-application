//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

protocol BaseFailureViewDelegate: class {
    func retryLoadingData()
}

// MARK:- Setup
class BaseFailureView: UIView {
    
    fileprivate let containerView = UIView()
    fileprivate let titleLabel = UILabel()
    fileprivate let descriptionLabel = UILabel()
    fileprivate let retryButton = UIButton(type: .system)
    weak var delegate: BaseFailureViewDelegate?
    
    init() {
        super.init(frame: CGRect.zero)
        setupContainerView()
        setupTitleLabel()
        setupDescriptionLabel()
        setupRetryButton()
        configure(title: nil, description: nil, buttonTitle: nil)
    }
    
    private func setupContainerView() {
        containerView.backgroundColor = UIColor.white
        containerView.layer.cornerRadius = 4.0
        containerView.layer.masksToBounds = true
        containerView.layer.borderWidth = Environment.thinnestLine
        containerView.layer.borderColor = UIColor.brandLightGray.cgColor
        addSubview(containerView)
    }
    
    private func setupTitleLabel() {
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.brandBold(size: 20.0)
        titleLabel.textColor = UIColor.brandDarkGray
        containerView.addSubview(titleLabel)
    }
    
    private func setupDescriptionLabel() {
        descriptionLabel.textColor = UIColor.brandLightGray
        descriptionLabel.numberOfLines = 8
        descriptionLabel.textAlignment = .center
        descriptionLabel.font = UIFont.brandMedium()
        containerView.addSubview(descriptionLabel)
    }
    
    private func setupRetryButton() {
        retryButton.addTarget(self, action: #selector(retryButtonPressed), for: .touchUpInside)
        retryButton.backgroundColor = UIColor.brandGray
        retryButton.setTitleColor(UIColor.white, for: .normal)
        retryButton.titleLabel?.font = UIFont.brandMedium()
        retryButton.layer.cornerRadius = 3.0
        retryButton.layer.masksToBounds = true
        containerView.addSubview(retryButton)
    }
    
    @objc private func retryButtonPressed() {
        delegate?.retryLoadingData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(title: String?, description: String?, buttonTitle: String?) {
        let unwrappedTitle = title ?? "Error"
        let unwrappedDescription = description ?? "An error occurred. Check your network settings and try again."
        let unwrappedButtonTitle = buttonTitle ?? "Retry"
        titleLabel.text = unwrappedTitle
        descriptionLabel.text = unwrappedDescription
        retryButton.setTitle(unwrappedButtonTitle, for: .normal)
    }
}

private let kContainerViewWidth = CGFloat(300.0)
private let kSharedPadding = CGFloat(15.0)
private let kTitleLabelHeight = CGFloat(30.0)
private let kRetryButtonHeight = CGFloat(45.0)

// MARK:- Layout
extension BaseFailureView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let contentWidth = kContainerViewWidth - kSharedPadding * 2
        let descriptionLabelHeight = descriptionLabel.sizeThatFits(CGSize(width: contentWidth, height: CGFloat.greatestFiniteMagnitude)).height
        let containerHeight = kSharedPadding + kTitleLabelHeight + kSharedPadding + descriptionLabelHeight + kSharedPadding + kRetryButtonHeight + kSharedPadding
        containerView.frame = CGRect((frame.width - kContainerViewWidth) / 2, (frame.height - containerHeight) / 2, kContainerViewWidth, containerHeight)
        titleLabel.frame = CGRect(kSharedPadding, kSharedPadding, contentWidth, kTitleLabelHeight)
        descriptionLabel.frame = CGRect(kSharedPadding, titleLabel.frame.maxY + kSharedPadding, contentWidth, descriptionLabelHeight)
        retryButton.frame = CGRect(kSharedPadding, descriptionLabel.frame.maxY + kSharedPadding, contentWidth, kRetryButtonHeight)
    }
}
