//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit

protocol ButtonBarViewDelegate: class {
    func buttonPressed()
}

class ButtonBarView: UIView {
    
    private let line = UIView()
    private let button = UIButton.generateButton(title: "")
    weak var delegate: ButtonBarViewDelegate?
    
    init(buttonTitle: String) {
        button.setTitle(buttonTitle, for: .normal)
        super.init(frame: CGRect.zero)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = UIColor.white
        line.backgroundColor = UIColor.brandGray
        addSubview(line)
        button.addTarget(self, action: #selector(internalButtonPressed), for: .touchUpInside)
        addSubview(button)
    }
    
    @objc private func internalButtonPressed() {
        delegate?.buttonPressed()
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        line.frame = CGRect(0.0, 0.0, frame.width, Environment.thinnestLine)
        button.frame = CGRect(30.0 , 10.0, frame.width - 60.0, frame.height - 20.0)
    }
}
