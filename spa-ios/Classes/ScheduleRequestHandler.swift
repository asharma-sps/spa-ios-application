//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import Foundation
import Alamofire

// Mark:- GET
struct ScheduleRequestHandler {
    
    static func requestSchedulableProgramsAndScheduleEntries(user: User, completion: @escaping ((programs: [WorkoutProgram], entries: [ScheduleEntry])?) -> ()) {
        requestSchedulableProgramsDataForCurrentUser(user: user) { optionalData in
            guard let (programs, programsData) = self.unpackPrograms(data: optionalData) else {
                self.loadProgramsAndEntriesFromCache(completion: completion)
                return
            }
            
            self.requestScheduleEntriesDataForCurrentUser(user: user) { optionalData in
                guard let (entries, entriesData) = self.unpackScheduleEntries(data: optionalData) else {
                    self.loadProgramsAndEntriesFromCache(completion: completion)
                    return
                }
                if entries.count > 0 {
                    ScheduleDataCacheHelper.set(programsData: programsData, entriesData: entriesData)
                }
                completion(self.crossAssociateData(programs: programs, entries: entries))
            }
        }
    }
    
    static func requestSchedulablePrograms(user: User, completion: @escaping ([WorkoutProgram]?) -> ()) {
        requestSchedulableProgramsDataForCurrentUser(user: user) { optionalData in
            completion(self.unpackPrograms(data: optionalData)?.programs)
        }
    }
    
    fileprivate static func requestSchedulableProgramsDataForCurrentUser(user: User, completion: @escaping (Data?) -> ()) {
        let request = Router(.getSchedulablePrograms, user)
        Alamofire.request(request)
            .validate()
            .responseData { response in
                switch response.result {
                case .success(let data):
                    completion(data)
                case .failure(let error):
                    errorPrint("Failed to fetch schedulable programs.", error)
                    completion(nil)
                }
            }
    }
    
    fileprivate static func requestScheduleEntriesDataForCurrentUser(user: User, completion: @escaping (Data?) -> ()) {
        let request = Router(.getScheduleEntries, user)
        Alamofire.request(request)
            .validate()
            .responseData { response in
                    switch response.result {
                    case .success(let data):
                        completion(data)
                    case .failure(let error):
                        errorPrint("Failed to fetch schedule entries.", error)
                        completion(nil)
                }
        }
    }
}

// MARK:- Unpacking Helpers
extension ScheduleRequestHandler {
    
    fileprivate static func unpackScheduleEntries(data: Data?) -> (entries: [ScheduleEntry], data: Data)? {
        guard let unwrappedData = data else { return nil }
        guard let dictionaries = jsonDictionariesFromData(data: unwrappedData) else { return nil }
        return (entries: deserializeScheduleEntries(scheduleDicts: dictionaries), data: unwrappedData)
    }
    
    fileprivate static func unpackPrograms(data: Data?) -> (programs: [WorkoutProgram], data: Data)? {
        guard let unwrappedData = data else { return nil }
        guard let dictionaries = jsonDictionariesFromData(data: unwrappedData) else { return nil }
        return (programs: deserializePrograms(programDicts: dictionaries), data: unwrappedData)
    }
    
    private static func jsonDictionariesFromData(data: Data) -> [[String: AnyObject]]? {
        guard let uncheckedJSON = try? JSONSerialization.jsonObject(with: data, options: []) else { return nil }
        guard let json = uncheckedJSON as? [[String: AnyObject]] else { return nil }
        return json
    }
}

// MARK:- Cache Helpers
extension ScheduleRequestHandler {
    
    fileprivate static func loadProgramsAndEntriesFromCache(completion: @escaping ((programs: [WorkoutProgram], entries: [ScheduleEntry])?) -> ()) {
        guard let (programData, entriesData) = ScheduleDataCacheHelper.load() else {
            completion(nil)
            return
        }
        guard let (programs, _) = unpackPrograms(data: programData), let (entries, _) = unpackScheduleEntries(data: entriesData) else { return }
        completion(self.crossAssociateData(programs: programs, entries: entries))
    }
}

// MARK:- Deserialization
extension ScheduleRequestHandler {
    
    fileprivate static func deserializePrograms(programDicts: [[String: AnyObject]]) -> [WorkoutProgram] {
        var programs = [WorkoutProgram]()
        for dict in programDicts {
            guard let id = dict["id"] as? Int else { continue }
            guard let name = dict["name"] as? String else { continue }
            guard let subtypeString = dict["subtype"] as? String, let subtype = WorkoutSubtype(rawValue: subtypeString) else { continue }
            guard let outlineDicts = dict["outlines"] as? [[String: AnyObject]] else { continue }
            let outlines = deserializeWorkoutOutlines(outlineDicts: outlineDicts)
            let logo = dict["logo"]?["sm"] as? String? ?? nil
            let program = WorkoutProgram(id: id, name: name, subtype: subtype, logo: logo, outlines: outlines)
            programs.append(program)
        }
        return programs
    }

    fileprivate static func deserializeScheduleEntries(scheduleDicts: [[String: AnyObject]]) -> [ScheduleEntry] {
        var calendar = NSCalendar.current
        calendar.firstWeekday = 1
        calendar.minimumDaysInFirstWeek = 7

        var scheduleEntries = [ScheduleEntry]()
        for dict in scheduleDicts {
            guard let id = dict["id"] as? Int else { continue }
            guard let workoutID = dict["content_id"] as? Int else { continue }
            guard let dateString = dict["date"] as? String else { continue }
            guard let date = dateForDateString(dateString: dateString) else { continue }
            let scheduleEntry = ScheduleEntry(id: id, date: date, dateString: dateString, workoutID: workoutID)
            scheduleEntries.append(scheduleEntry)
        }
        return scheduleEntries
    }

    fileprivate static func dateForDateString(dateString: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let date = formatter.date(from: dateString) { return date }
        return nil
    }
    
    fileprivate static func deserializeWorkoutOutlines(outlineDicts:[[String: AnyObject]]) -> [WorkoutOutline] {
        var outlines = [WorkoutOutline]()
        for dict in outlineDicts {
            let id = dict["id"] as? Int
            let title = dict["title"] as? String
            let outline = WorkoutOutline(title: title, id: id)
            outlines.append(outline)
        }
        return outlines
    }
}

// MARK:- Data Association
extension ScheduleRequestHandler {
    
    /*
     * This method interates though a [WorkoutProgram] and a [ScheduleEntry] to associate data not provided by the API.
     * Specifically, the API does not provide a program information or a workout title when requesting schedule entries.
     * This data can be found in the results of a request for programs by associated the workout_id with the content_id of an scheduleEntry
     */
    
    fileprivate static func crossAssociateData(programs: [WorkoutProgram], entries: [ScheduleEntry]) -> (programs: [WorkoutProgram], entries: [ScheduleEntry]) {
        var completedScheduleEntries = [ScheduleEntry]()
        for scheduleEntry in entries {
           var entry = scheduleEntry
            programLoop: for var program in programs {
                for outline in program.outlines {
                    if entry.workoutID == outline.id {
                        entry.programID = program.id
                        entry.programName = program.name
                        entry.programLogo = program.logo
                        entry.workoutTitle = outline.title
                        program.isScheduled = true
                        break programLoop
                    }
                }
            }
            completedScheduleEntries.append(entry)
        }
        
        var completedPrograms = [WorkoutProgram]()
        for program in programs {
            var completedProgram = program
            for scheduleEntry in completedScheduleEntries {
                if program.id == scheduleEntry.programID {
                    completedProgram.isScheduled = true
                    break
                }
            }
            completedPrograms.append(completedProgram)
        }
        return(programs: completedPrograms, entries: completedScheduleEntries)
    }
}

// MARK:- Saving / Deleting Schedules
extension ScheduleRequestHandler {
    
    static func setScheduleEntries(user: User, entriesDicts: [[String: Any]], completion: @escaping (Bool) -> ()) {
        let request = Router(.postSchedule(entries: entriesDicts), user)
        Alamofire.request(request)
            .validate()
            .responseJSON() { response in
                switch response.result {
                case .success:
                    completion(true)
                case .failure(let error):
                    errorPrint("Failed to save schedule entries.", error)
                    completion(false)
                }
        }
    }
    
    static func clearScheduleEntries(user: User, completion: @escaping (Bool) -> ()) {
        let request = Router(.deleteSchedule, user)
        Alamofire.request(request)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    ScheduleDataCacheHelper.clear()
                    completion(true)
                case .failure(let error):
                    errorPrint("Failed to delete all schedule entries", error)
                    completion(false)
                }
        }
    }
}
