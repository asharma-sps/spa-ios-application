//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import AVKit
import AVFoundation
import ASGlobalOverlay
import FacebookCore
import FacebookLogin

private let kSquareLogoContainerFrameWidth = CGFloat(250.0)
private let kLogoImageViewPadding = CGFloat(25)
private let kButtonHeight = CGFloat(60.0)
private let kThinButtonHeight = CGFloat(25.0)
private let kButtonPadding = CGFloat(10.0)

// MARK:- Setup
class HomescreenVC: AnalyticBaseViewController, GIDSignInUIDelegate {
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask { return UIInterfaceOrientationMask.portrait }
    let backgroundVideoPlayer = AVPlayerViewController()
    let logoContainerView = UIView()
    let logoImageView = UIImageView(image: UIImage(named: ""))
    let loginButton = UIButton.generateButton(title: "Login")
    let createAccountButton = UIButton.generateButton(title: "Create Account")
    let socialSignInButton = UIButton(type: .system)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBackgroundViewPlayer()
        setupLogoImageView()
        setupLoginButton()
        setupCreateAccountButton()
        setupGoogleSignInButton()
    }
    
    private func setupBackgroundViewPlayer() {
        backgroundVideoPlayer.configureForLoopingVideo()
        addChildViewController(backgroundVideoPlayer)
        view.addSubview(backgroundVideoPlayer.view)
        backgroundVideoPlayer.didMove(toParentViewController: self)
    }
    
    private func setupLogoImageView() {
        logoContainerView.backgroundColor = UIColor.brandDarkBlue.withAlphaComponent(0.95)
        logoContainerView.layer.cornerRadius = 5.0
        logoContainerView.layer.masksToBounds = true
        logoImageView.image = UIImage(named: "SPS_LOGO_WHITE")
        logoImageView.contentMode = .scaleAspectFit
        logoContainerView.addSubview(logoImageView)
        view.addSubview(logoContainerView)
    }
    
    private func setupLoginButton() {
        loginButton.backgroundColor = UIColor.brandLightBlue
        loginButton.addTarget(self, action: #selector(loginButtonPressed), for: .touchUpInside)
        view.addSubview(loginButton)
    }
    
    private func setupCreateAccountButton() {
        createAccountButton.backgroundColor = UIColor.brandDarkBlue
        createAccountButton.setTitleColor(UIColor.white, for: .normal)
        createAccountButton.addTarget(self, action: #selector(createAccountButtonPressed), for: .touchUpInside)
        view.addSubview(createAccountButton)
    }
    
    private func setupGoogleSignInButton() {
        GIDSignIn.sharedInstance().uiDelegate = self
        socialSignInButton.setTitle("or login with Google or Facebook", for: .normal)
        socialSignInButton.addTarget(self, action: #selector(socialSignInButtonPressed), for: .touchUpInside)
        socialSignInButton.setTitleColor(UIColor.white, for: .normal)
        view.addSubview(socialSignInButton)
    }
    
    deinit {
        backgroundVideoPlayer.player = nil
        NotificationCenter.default.removeObserver(self)
    }
    
    private func generateButton(title: String, image: UIImage, selector: Selector) -> UIButton {
        let button = UIButton.generateButton(title: title)
        button.addTarget(self, action: selector, for: .touchUpInside)
        button.titleLabel?.textAlignment = .left
        button.setImage(image, for: .normal)
        return button
    }
}

// MARK:- Button Targets
extension HomescreenVC {
    
    @objc fileprivate func loginButtonPressed() {
        let loginTVC = HomescreenLoginTVC.generateHomescreenLoginTableViewController()
        let loginNC = ManagedNavigationController(rootViewController: loginTVC)
        present(loginNC, animated: true, completion: nil)
    }
    
    @objc fileprivate func createAccountButtonPressed() {
        let createTVC = HomescreenCreateAccountTVC.generateHomescreenCreateAccountTableViewController()
        let createNC = ManagedNavigationController(rootViewController: createTVC)
        present(createNC, animated: true, completion: nil)
    }
    
    @objc fileprivate func socialSignInButtonPressed() {
        let google = ASUserOption(title: "Google") {
            GIDSignIn.sharedInstance().signOut()
            GIDSignIn.sharedInstance().signIn()
        }
        let facebook = ASUserOption(title: "Facebook") {
            self.showUserFacebookAuthDialog()
        }
        let cancel = ASUserOption.cancel(withTitle: "Cancel", actionBlock: nil)
        ASGlobalOverlay.showSlideUpMenu(withPrompt: "Select a service to sign in.", userOptions: [google as Any, facebook as Any, cancel as Any])
    }
}

// MARK:- Facebook Auth Result Handling
extension HomescreenVC {
    
    fileprivate func showUserFacebookAuthDialog() {
        FacebookAuthHelper.signOutFacebookUser()
        FacebookAuthHelper.showUserFacebookAuthDialog(viewController: self) { result in
            switch result {
            case .cancelled:
                break
            case .failure:
                ASGlobalOverlay.showAlert(withTitle: "Error", message: "Please check your network settings and try again.", dismissButtonTitle: "OK")
            case .success:
                self.signUserInUsingFacebook()
            }
        }
    }
    
    private func signUserInUsingFacebook() {
        ASGlobalOverlay.showWorkingIndicator(withDescription: nil)
        FacebookAuthHelper.authenticateUsingCurrentFacebookUser { optionalUser in
            if let user = optionalUser {
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    appDelegate.setRootViewController(withUser: user)
                }
                ASGlobalOverlay.dismissWorkingIndicator()
            } else {
                let message = "You may have a poor network connection, or the email associated with your Facebook may already be in use with another account type (i.e. Google or Email login). Please contact support if you continue having isssues."
                ASGlobalOverlay.showAlert(withTitle: "Error", message: message, dismissButtonTitle: "OK")
            }
        }
    }
}

// MARK:- Layout
extension HomescreenVC {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        backgroundVideoPlayer.view.frame = CGRect(0.0, 0.0, view.frame.width, view.frame.height)
        
        logoContainerView.frame = CGRect((view.frame.width - kSquareLogoContainerFrameWidth) / 2,
                                     view.frame.height * 0.15,
                                     kSquareLogoContainerFrameWidth,
                                     kSquareLogoContainerFrameWidth)
        
        logoImageView.frame = CGRect(kLogoImageViewPadding, kLogoImageViewPadding, kSquareLogoContainerFrameWidth - kLogoImageViewPadding * 2, kSquareLogoContainerFrameWidth - kLogoImageViewPadding * 2)
        
        let buttonWidth = view.frame.width - kButtonPadding * 2
        
        socialSignInButton.frame = CGRect(kButtonPadding, view.frame.height - kButtonPadding * 2 - kThinButtonHeight, buttonWidth, kThinButtonHeight)
        
        createAccountButton.frame = CGRect(kButtonPadding,
                                           socialSignInButton.frame.origin.y - kButtonPadding - kButtonHeight,
                                           buttonWidth,
                                           kButtonHeight)
        
        loginButton.frame = CGRect(kButtonPadding,
                                   createAccountButton.frame.origin.y - kButtonPadding - kButtonHeight,
                                   buttonWidth,
                                   kButtonHeight)
    }
}

// MARK:- Lifecycle Management
extension HomescreenVC {
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        backgroundVideoPlayer.player?.pause()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let videoPath = Bundle.main.path(forResource: "sps_homescreen_video", ofType:"mp4") else { return }
        let videoURL = URL(fileURLWithPath: videoPath)
        let player = AVPlayer(url: videoURL)
        player.allowsExternalPlayback = false
        player.actionAtItemEnd = .none
        backgroundVideoPlayer.player = player
        NotificationCenter.default.addObserver(self, selector: #selector(restartVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        player.play()
    }
    
    @objc func restartVideo() {
        backgroundVideoPlayer.player?.seek(to: kCMTimeZero)
        backgroundVideoPlayer.player?.play()
    }
}

// MARK:- Analytics
extension HomescreenVC {
    
    override func analyticsViewDidAppear() {
        Analytics.screenDidAppear(screen: .homescreen)
        backgroundVideoPlayer.player?.play() // Not actually related to analytics, just piggybacking off the fact that this method is invoked when the app becomes active and the view is visible.
    }
    
    override func analyticsViewDidDisappear() {
        Analytics.screenDidDissapear(screen: .homescreen)
    }
}
