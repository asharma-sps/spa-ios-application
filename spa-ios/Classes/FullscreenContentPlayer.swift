//
//  *************************************************
//  Created by Amit Sharma 2017
//  Copyright © 2017 SixPackAbs. All rights reserved.
//  *************************************************
//

import UIKit
import AVFoundation
import AVKit

class FullscreenContentPlayer: AVPlayerViewController {
    
    override var shouldAutorotate: Bool { return false }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask { return .landscape }

    init(player: AVPlayer) {
        super.init(nibName: nil, bundle: nil)
        self.player = player
    }
    
    func play() {
        player?.play()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
